/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

/*!
 * @mainpage Documentazione codice sorgente GoMoku3D
 *
 * @section descrizione Cos'è Gomoku3D
 * GoMoku3D è una variante tridimensionale del classico gioco GoMoku.
 * Lo scopo del gioco è di creare una sequenza di pedine dello stesso colore.
 *
 *
 * @section installazione Come si installa
 * Il gioco GoMoku3D è fornito nelle versioni per....
 *
 *
 * @section ringraziamenti Ringraziamenti
 * - Tullio per la bella esperienza.
 * - I sumiti per i panini.
 * - Pizza Shop e la Forcellini (perché lo dice la Martina).
 * - Tutte le mamme del mondo.
 * - La matemagica.
 */

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QAbstractSocket>
#include <QList>
#include <Inventor/Qt/SoQt.h>

#include "MainWindow.h"
#include "GUISettings.h"
#include "Point.h"
#include "Move.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QApplication::setOrganizationName("ITWorks");
    QApplication::setApplicationName("GoMoku3D");

    GUISettings settings;

    /* load translations */
    QTranslator qtTranslator;
    if (qtTranslator.load("qt_" + QLocale::system().name())) {
        app.installTranslator(&qtTranslator);
    }
    QTranslator myTranslator;
    if (myTranslator.load("translations/gomoku3d_" + settings.language())) {
        app.installTranslator(&myTranslator);
    }

    /* register metatypes */
    qRegisterMetaType<Point>();
    qRegisterMetaType<Move>();
    qRegisterMetaType< QList<int> >();
    qRegisterMetaType< QList<Move> >();
    qRegisterMetaType<QAbstractSocket::SocketError>();

    MainWindow mainwin;

    /* initialize SoQt library */
    SoQt::init(&mainwin);

    /* restore MainWindow's geometry*/
    mainwin.restoreGeometry(settings.geometry());

    mainwin.show();
    return app.exec();
}

Q_DECLARE_METATYPE(QList<int>)
Q_DECLARE_METATYPE(QList<Move>)
Q_DECLARE_METATYPE(QAbstractSocket::SocketError)
