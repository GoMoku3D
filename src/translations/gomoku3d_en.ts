<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1">
<context>
    <name>About</name>
    <message>
        <location filename="../gui/ui/About.ui" line="97"/>
        <source>GoMoku3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/About.ui" line="104"/>
        <source>Version 1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/About.ui" line="111"/>
        <source>A 3D version of the popular GoMoku board game.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/About.ui" line="118"/>
        <source>Developed by:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/About.ui" line="132"/>
        <source>ITWorks!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/About.ui" line="139"/>
        <source>Contact: itworks@googlegroups.com</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/About.ui" line="22"/>
        <source>About GoMoku3D</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatWidget</name>
    <message>
        <location filename="../gui/ChatWidget.cpp" line="32"/>
        <source>Chat.
Here you can send/receive messages to/from all players in this game.
To send a new message, write the text, then press ENTER.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClientSocket</name>
    <message>
        <location filename="../network/ClientSocket.cpp" line="76"/>
        <source>connecting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="83"/>
        <source>invalid content in &lt;joinACK&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="95"/>
        <source>invalid content in &lt;joinNAK&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="101"/>
        <source>You cannot join while a match
is already in progress.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="104"/>
        <source>The name you have chosen is already
being used by someone else.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="107"/>
        <source>The server you are connecting to
is using an incompatible protocol version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="110"/>
        <source>An unknown error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="134"/>
        <source>invalid &lt;difficultyOne&gt; value in &lt;settings&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="145"/>
        <source>invalid &lt;difficultyTwo&gt; value in &lt;settings&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="156"/>
        <source>invalid &lt;numberOfPlayers&gt; value in &lt;settings&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="167"/>
        <source>invalid &lt;timerDuration&gt; value in &lt;settings&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="231"/>
        <source>invalid &lt;player&gt; value in &lt;move&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="274"/>
        <source>invalid &lt;x&gt; value in &lt;point&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="291"/>
        <source>invalid &lt;y&gt; value in &lt;point&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/ClientSocket.cpp" line="308"/>
        <source>invalid &lt;z&gt; value in &lt;point&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HistoryModel</name>
    <message>
        <location filename="../gui/HistoryModel.cpp" line="65"/>
        <source>Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/HistoryModel.cpp" line="68"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../gui/MainWindow.cpp" line="72"/>
        <source>Players&apos; Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="73"/>
        <source>History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="74"/>
        <source>Timer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="75"/>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="597"/>
        <source>Game Ended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="181"/>
        <source>Timer expired:
%1 lost the game.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="210"/>
        <source>Abandon Game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="211"/>
        <source>If you continue, the current
game will be lost.

Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="586"/>
        <source>And the winner is ...
%1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="597"/>
        <source>Game ended in a draw:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="775"/>
        <source>Forbidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="775"/>
        <source>You can&apos;t go back when it&apos;s not your turn.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="779"/>
        <source>Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="779"/>
        <source>Do you really want to bring the game in a past situation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="986"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="824"/>
        <source>%1
Quitting current game.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="831"/>
        <source>Save Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="904"/>
        <source>Open Saved Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="986"/>
        <source>Saved game file is corrupted.
Cannot load game.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="990"/>
        <source>Load Settings?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="13"/>
        <source>GoMoku3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="46"/>
        <source>Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="50"/>
        <source>New Online Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="67"/>
        <source>Moves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="78"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="91"/>
        <source>New Stand-Alone Game...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="94"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="99"/>
        <source>Load Game...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="102"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="110"/>
        <source>Save Game...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="113"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="118"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="121"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="126"/>
        <source>Help Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="139"/>
        <source>Undo Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="142"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="150"/>
        <source>Show Hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="158"/>
        <source>Show Last Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="163"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="168"/>
        <source>Connect to Server...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="171"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="176"/>
        <source>Host Game...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="179"/>
        <source>Ctrl+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="184"/>
        <source>Preferences...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="192"/>
        <source>Force AI Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="195"/>
        <source>Ctrl+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="911"/>
        <source>Cannot open selected file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="926"/>
        <source>File is corrupted.
Cannot load game.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="991"/>
        <source>Load color settings from the saved game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="62"/>
        <source>What&apos;s This?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/MainWindow.ui" line="131"/>
        <source>About GoMoku3D</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OnlineDialog</name>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="134"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="57"/>
        <source>You cannot have an empty name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="127"/>
        <source>Human</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="177"/>
        <source>AI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="180"/>
        <source>Remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="286"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="261"/>
        <source>Cannot connect,
&apos;%1&apos; is not a valid port number.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="264"/>
        <source>Cannot connect,
port number must be greater than 1024.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="286"/>
        <source>Network error.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="298"/>
        <source>not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="22"/>
        <source>Online Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="53"/>
        <source>Server Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="59"/>
        <source>IP address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="76"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="93"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="112"/>
        <source>Status of connection with the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="143"/>
        <source>Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="159"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Bitstream Vera Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Lucida Grande&apos;; font-size:13pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; color:#8b0000;&quot;&gt;not connected&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="218"/>
        <source>Number of players:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="333"/>
        <source>?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="254"/>
        <source>Difficulty 1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="290"/>
        <source>Difficulty 2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="326"/>
        <source>Timer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="340"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="363"/>
        <source>Players&apos; Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="402"/>
        <source>Player 1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="519"/>
        <source>Player 2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="636"/>
        <source>Player 3:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="769"/>
        <source>My name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="851"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="874"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="887"/>
        <source>Join the game as a spectator. 
 In this mode, you can only view the current game and others player&apos;s move.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="891"/>
        <source>Join as Spectator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="901"/>
        <source>Join the game as a player. 
 In this mode you can play.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="905"/>
        <source>Join as Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="49"/>
        <source>Set here the IP address and the port of the server hosting the game.
Press the button to connect to the server and get game status.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="200"/>
        <source>Settings of the game hosted by the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/OnlineDialog.ui" line="359"/>
        <source>Properties of players that already joined the game.
Set your name and color, then join the game as a player or a spectator.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="79"/>
        <source>Players&apos; colors are too similar
to background color or empty cube color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/OnlineDialog.cpp" line="82"/>
        <source>Players&apos; colors are too similar.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayersWidget</name>
    <message>
        <location filename="../gui/PlayersWidget.cpp" line="69"/>
        <source>Player 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/PlayersWidget.cpp" line="70"/>
        <source>Player 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/PlayersWidget.cpp" line="71"/>
        <source>Player 2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../gui/Preferences.cpp" line="60"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/Preferences.cpp" line="60"/>
        <source>Background color and cube color are too similar.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenderWidget</name>
    <message>
        <location filename="../gui/3d/RenderWidget.cpp" line="60"/>
        <source>Here is displayed the game matrix. Possible interactions are:
- left mouse button click and drag: rotate the game matrix;
- left mouse button: select a cube;
- mouse wheel: zoom in and out;
- ENTER key: confirm a move for the current player;
- SPACE key: enable the explode function;
- UP, DOWN, LEFT, RIGHT arrows: change selected cube;
- W, S keys: change selected cube.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/3d/RenderWidget.cpp" line="137"/>
        <source>invalid point</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerSettingsDialog</name>
    <message>
        <location filename="../gui/ServerSettingsDialog.cpp" line="169"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ServerSettingsDialog.cpp" line="132"/>
        <source>Invalid port number.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ServerSettingsDialog.cpp" line="143"/>
        <source>Human and AI players cannot have empty name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ServerSettingsDialog.cpp" line="147"/>
        <source>You cannot have an empty name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ServerSettingsDialog.cpp" line="358"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="16"/>
        <source>Server Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="43"/>
        <source>Game settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="55"/>
        <source>Set here the number of players.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="58"/>
        <source>Number of players</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="142"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="147"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="91"/>
        <source>Difficulty 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="98"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="103"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="108"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="130"/>
        <source>Difficulty 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="137"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="172"/>
        <source>Players settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="208"/>
        <source>Player 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="544"/>
        <source>Human</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="554"/>
        <source>Remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="564"/>
        <source>AI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="330"/>
        <source>Player 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="461"/>
        <source>Player 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="588"/>
        <source>Timer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="606"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="635"/>
        <source>Maximum time per move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="648"/>
        <source> seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="678"/>
        <source>Server settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="689"/>
        <source>Spectator mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="730"/>
        <source>Your name here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="742"/>
        <source>Dedicated server mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="761"/>
        <source>Server port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="813"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="87"/>
        <source>Set here the first difficulty level.
It represents the length of a winning sequence.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="125"/>
        <source>Set here the second difficulty level.
It&apos;s used to determine the size of the game matrix with the following formula:
latus = difficulty 1 * difficulty 2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="167"/>
        <source>Set here the players&apos; properties.
You can choose here the players&apos; type.
For each human and artificial player you must set name and color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="673"/>
        <source>Spectator mode allows you to join the game as a spectator. You need to set your name.
Dedicated server mode allows you to start hosting a game without partecipating to it nor as a spectator nor as a player.
Server port is the port used to accept connections from other clients. This port needs to be opened and forwarded by your router and firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/ServerSettingsDialog.ui" line="583"/>
        <source>Here you can enable or disable the additional &quot;timer&quot; game rule.
It&apos;s the maximum time a human player can spend to choose his next move.
If a player exceeds this time, he loses the game.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ServerSettingsDialog.cpp" line="166"/>
        <source>Players&apos; colors are too similar
to background color or empty cube color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ServerSettingsDialog.cpp" line="169"/>
        <source>Players&apos; colors are too similar.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ServerSettingsDialog.cpp" line="267"/>
        <source>Waiting for players to join ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerSocket</name>
    <message>
        <location filename="../network/ServerSocket.cpp" line="81"/>
        <source>refusing to send &lt;playerJoined&gt; with invalid player type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StandAloneDialog</name>
    <message>
        <location filename="../gui/StandAloneDialog.cpp" line="109"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/StandAloneDialog.cpp" line="88"/>
        <source>Players cannot have empty name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="25"/>
        <source>Stand-Alone Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="52"/>
        <source>Game settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="64"/>
        <source>Set here the number of players.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="67"/>
        <source>Number of players</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="151"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="156"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="100"/>
        <source>Difficulty 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="107"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="112"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="117"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="139"/>
        <source>Difficulty 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="146"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="183"/>
        <source>Players settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="207"/>
        <source>Player 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="480"/>
        <source>Type the player&apos;s name here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="550"/>
        <source>Human</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="560"/>
        <source>AI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="328"/>
        <source>Player 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="458"/>
        <source>Player 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="578"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="96"/>
        <source>Set here the first difficulty level.
It represents the length of a winning sequence.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="134"/>
        <source>Set here the second difficulty level.
It&apos;s used to determine the size of the game matrix with the following formula:
latus = difficulty 1 * difficulty 2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/StandAloneDialog.ui" line="176"/>
        <source>Set here the players&apos; properties.
For each player set: 
- name 
- color 
- type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/StandAloneDialog.cpp" line="106"/>
        <source>Players&apos; colors are too similar
to background color or empty cube color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/StandAloneDialog.cpp" line="109"/>
        <source>Players&apos; colors are too similar.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StreamSocket</name>
    <message>
        <location filename="../network/StreamSocket.cpp" line="69"/>
        <source>resolving hostname...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="73"/>
        <source>negotiating stream...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="75"/>
        <source>connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="81"/>
        <source>not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="213"/>
        <source>unknown protocol message %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="278"/>
        <source>invalid value for attribute &apos;id&apos; in &lt;playerJoined&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="318"/>
        <source>invalid content in &lt;playerLeft&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="359"/>
        <source>invalid &lt;player&gt; value in &lt;move&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="379"/>
        <source>invalid &lt;x&gt; value in &lt;point&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="390"/>
        <source>invalid &lt;y&gt; value in &lt;point&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="401"/>
        <source>invalid &lt;z&gt; value in &lt;point&gt; : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="128"/>
        <source>refusing to send &lt;chatMessage&gt; with empty sender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../network/StreamSocket.cpp" line="177"/>
        <source>Communication with the remote host timed out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimerWidget</name>
    <message>
        <location filename="../gui/TimerWidget.cpp" line="31"/>
        <source>Timer for your turn.
Don&apos;t let the timer expire, or you will lose.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>chatwidget</name>
    <message>
        <location filename="../gui/ui/ChatWidget.ui" line="13"/>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>playerswidget</name>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="19"/>
        <source>Players&apos; Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../gui/ui/PlayersWidget.ui" line="367"/>
        <source>➤</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="414"/>
        <source>Player&apos;s name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="117"/>
        <source>player 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="171"/>
        <source>Player&apos;s type: 
 - H: human 
 - A: artificial 
 - R: remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="177"/>
        <source>H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="270"/>
        <source>player 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="468"/>
        <source>Player&apos;s type. 
 - H: human 
 - A: artificial 
 - R: remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="327"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="417"/>
        <source>player 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="474"/>
        <source>R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="386"/>
        <source>Player&apos;s color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/PlayersWidget.ui" line="436"/>
        <source>Sets artificial player&apos;s skill.
Move the slider left for a beginner player, move it right for an expert one.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>preferences</name>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="22"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="49"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="74"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="98"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="103"/>
        <source>Italian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="132"/>
        <source>Background color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="221"/>
        <source>Cube color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="300"/>
        <source>AI players delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="323"/>
        <source> seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="341"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="61"/>
        <source>Set here the language used by the application.
Restart application for changes to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="120"/>
        <source>Set here the background color of the 3D view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="208"/>
        <source>Set here the color used for free cubes.
Restart game for changes to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/ui/Preferences.ui" line="288"/>
        <source>Set here the minimum amount of time used by artificial players to choose their next move.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
