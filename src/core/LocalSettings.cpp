/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "LocalSettings.h"

void LocalSettings::setPlayersInfo(QList<PlayerInfo> info)
{

    for (int i = 0; i < info.size(); i++) {
        setValue("Local/player_" + QString::number(i) + "_name", info.at(i).name());
        setValue("Local/player_" + QString::number(i) + "_color", info.at(i).color());
        setValue("Local/player_" + QString::number(i) + "_type", info.at(i).type());
    }
}

void LocalSettings::setDifficulty1(int d1)
{
    setValue("Local/d1", d1);
}

void LocalSettings::setDifficulty2(int d2)
{
    setValue("Local/d2", d2);
}

void LocalSettings::setNumberOfPlayers(int n)
{
    setValue("Local/players", n);
}

QList<PlayerInfo> LocalSettings::playersInfo() const
{
    QList<PlayerInfo> info;
    QList<QColor> defaultColors;
    defaultColors.append(QColor(255, 0, 0));
    defaultColors.append(QColor(0, 255, 0));
    defaultColors.append(QColor(0, 0, 255));
    QList<QString> defaultTypes;
    defaultTypes.append("H");
    defaultTypes.append("A");
    defaultTypes.append("");

    for (int i = 0; i < 3; i++) {
        QString n = value("Local/player_" + QString::number(i) + "_name", "").toString();
        QColor c = value("Local/player_" + QString::number(i) + "_color",defaultColors[i]).value<QColor>();
        QString t = value("Local/player_" + QString::number(i) + "_type", defaultTypes[i]).toString();
        info.append(PlayerInfo(n, c, t));
    }
    return info;
}

int LocalSettings::difficulty1() const
{
    return value("Local/d1", 5).toInt();
}

int LocalSettings::difficulty2() const
{
    return value("Local/d2", 1).toInt();
}

int LocalSettings::numberOfPlayers() const
{
    return value("Local/players", 2).toInt();
}
