/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef LOCALSETTINGS_H
#define LOCALSETTINGS_H

#include "GUISettings.h"
#include "SettingsManager.h"

class LocalSettings : public GUISettings, public SettingsManager
{
    public:
        virtual void setPlayersInfo(QList<PlayerInfo> info);
        virtual void setDifficulty1(int d1);
        virtual void setDifficulty2(int d2);
        virtual void setNumberOfPlayers(int n);
        virtual QList<PlayerInfo> playersInfo() const;
        virtual int difficulty1() const;
        virtual int difficulty2() const;
        virtual int numberOfPlayers() const;
};

#endif
