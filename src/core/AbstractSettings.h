/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef ABSTRACTSETTINGS_H
#define ABSTRACTSETTINGS_H

#include <QString>
#include <QByteArray>
#include <QColor>

class AbstractSettings
{
    public:
        inline AbstractSettings() {}
        inline virtual ~AbstractSettings() {}
        virtual void setLanguage(QString lang) = 0;
        virtual void setGeometry(QByteArray geom) = 0;
        virtual void setDockState(QByteArray state) = 0;
        virtual void setDefaultCubeColor(QColor color) = 0;
        virtual void setBackgroundColor(QColor color) = 0;
        virtual QString language() const = 0;
        virtual QByteArray geometry() const = 0;
        virtual QByteArray dockState() const = 0;
        virtual QColor defaultCubeColor() const = 0;
        virtual QColor backgroundColor() const = 0;
};

#endif
