/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef GAMELOOP_H
#define GAMELOOP_H

#include <QThread>
#include <QList>
#include <QMutex>

#include "Global.h"
#include "Move.h"
#include "Player.h"
#include "GameMatrix.h"
#include "GUISettings.h"

class GameLoop : public QThread
{
    Q_OBJECT

    TEST_FRIEND(GameLoopTest)

    public:
        GameLoop(QList<Player*> players);
        virtual ~GameLoop();
        void setTurn(int plyUndone);
        void onLoadSetTurn(int id);
        void winStatus(QList<bool> status);
        void stop();
        bool aboutToQuit() const;

    public slots:
        void forceMove();

    protected:
        virtual void run();

    private:
        GameMatrix *_mat;
        Player *_currentPlayer;
        QList<Player*> _players;
        QList<bool> _winStatus;
        bool _resetLoop;
        GUISettings _settings;
        bool _aboutToQuit;
        QMutex _mutex;

    signals:
        void moved(Move move);
        void turn(int playerId);
        void win(int playerId);
        void draw(QList<int> playerIds);
};

#endif
