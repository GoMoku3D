/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef POINT_H
#define POINT_H

#include <QtGlobal>
#include <QMetaType>
#include <QString>

#include "Global.h"

//! Rappresenta una posizione nella matrice di gioco.
/*! Dipende dalla GameMatrix presente al momento.
*/

class Point
{
    TEST_FRIEND(PointTest)

    public:
        //! Costruttore di default
        /*! Costruisce un Point nullo, ovvero per cui la chiamata al metodo isNull() ritorna true.
        */
        Point();
        //! Costruttore
        /*! Costruisce un Point a partire dalle sue coordinate.
            \param x intero che rappresenta la componente x delle coordinate del Point
            \param y intero che rappresenta la componente y delle coordinate del Point
            \param z intero che rappresenta la componente z delle coordinate del Point
        */
        Point(int x, int y, int z);
        //! Ritorna la componente x delle coordinate del Point
        int x() const;
        //! Ritorna la componente y delle coordinate del Point
        int y() const;
        //! Ritorna la componente z delle coordinate del Point
        int z() const;
        /*! \return true se il Point ha coordinate interna alla matrice di gioco, altrimenti false.
        */
        bool isValid() const;
        /*! \return true se è il Point costruito con il costruttore di default.
        */
        bool isNull() const;
        /*! \return rappresentazione (x, y, z) del Point.
         */
        QString toString() const;
        bool operator== (const Point &p) const;
        bool operator!= (const Point &p) const;
        bool operator< (const Point &p) const;
        //! metodo di convenienza, comportamento equivalente a isValid()
        operator bool() const;

    private:
        int _x;
        int _y;
        int _z;
};

Q_DECLARE_METATYPE(Point)

uint qHash(Point p);

#endif
