/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef GAMEMATRIX_H
#define GAMEMATRIX_H

#include <QList>
#include <QSet>

#include "Global.h"
#include "Point.h"
#include "Move.h"

class GameMatrix
{
    friend class ThreatSearchAI;
    friend class CCThreatSearchAI;
    friend class Threat;
    friend class CCThreat;
    TEST_FRIEND(GameMatrixTest)

    public:
        static const int EmptyPoint;
        ~GameMatrix();
        static GameMatrix *create(int d1, int d2, int numPlayers);
        static void destroy();
        static GameMatrix *instance();
        bool check(Point point) const;
        bool add(Move move);
        int d1() const;
        int d2() const;
        void clear(Point point);
        int elementAt(int x, int y, int z) const;
        int elementAt(Point point) const;
        bool isFull() const;
        int numberOfPlayers() const;
        void setLastRound(QList<Point> round);

    private:
        GameMatrix(int d1, int d2, int numPlayers);
        static GameMatrix *_instance;
        int *_matrix;
        QList<Point> _lastRound;
        int _d1;
        int _d2;
        int _freeCounter;
        void extractCC(Point p, int id, QSet<Point> &set);

};

#endif
