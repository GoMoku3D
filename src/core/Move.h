/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef MOVE_H
#define MOVE_H

#include <QMetaType>

#include "Global.h"
#include "Point.h"

class Move
{
    TEST_FRIEND(MoveTest)

    public:
        Move();
        Move(int id, Point p);
        bool isValid() const;
        Point point() const;
        int playerId() const;
        bool operator== (const Move &m) const;
    private:
        int _playerId;
        Point _coord;
};

Q_DECLARE_METATYPE(Move)

#endif
