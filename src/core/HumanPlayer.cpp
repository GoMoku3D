/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "HumanPlayer.h"
#include "SyncSharedCondition.h"
#include "RenderWidget.h"

HumanPlayer::HumanPlayer(int id, RenderWidget *render) : Player(id)
{
    _render = render;
}

HumanPlayer::~HumanPlayer()
{
}

Point HumanPlayer::doMove()
{
    _render->acceptMove(true);
    return SyncSharedCondition::instance()->waitForMove();
}
