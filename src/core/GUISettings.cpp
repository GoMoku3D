/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "GUISettings.h"

void GUISettings::setLanguage(QString lang)
{
    setValue("GUI/language", lang);
}

void GUISettings::setGeometry(QByteArray geom)
{
    setValue("GUI/geometry", geom);
}

void GUISettings::setDockState(QByteArray state)
{
    setValue("GUI/dock_state", state);
}

void GUISettings::setDefaultCubeColor(QColor color)
{
    setValue("GUI/cube_color", color);
}

void GUISettings::setBackgroundColor(QColor color)
{
    setValue("GUI/bg_color", color);
}

void GUISettings::setAIDelay(int sec)
{
    setValue("GUI/delay", sec);
}

QString GUISettings::language() const
{
    return value("GUI/language", "en").toString();
}

QByteArray GUISettings::geometry() const
{
    return value("GUI/geometry", QByteArray()).toByteArray();
}

QByteArray GUISettings::dockState() const
{
    return value("GUI/dock_state", QByteArray()).toByteArray();
}

QColor GUISettings::defaultCubeColor() const
{
    return value("GUI/cube_color", QColor(127, 127, 127, 150)).value<QColor>();
}

QColor GUISettings::backgroundColor() const
{
    return value("GUI/bg_color", QColor(0, 0, 0)).value<QColor>();
}

int GUISettings::aiDelay() const
{
    return value("GUI/delay", 1).toInt();
}

