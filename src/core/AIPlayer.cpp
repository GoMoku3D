/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QMutexLocker>
#include <QtDebug>

#include "AIPlayer.h"
#include "GameMatrix.h"
#include "ThreatSearchAI.h"
#include "CCThreatSearchAI.h"

const int AIPlayer::_DefaultSkill = 2;

AIPlayer::AIPlayer(int id) : Player(id)
{
    _skill = _DefaultSkill;
    if (GameMatrix::instance()->d2() == 3) {
        _ai = new CCThreatSearchAI(id);
    }
    else {
        _ai = new ThreatSearchAI(id);
    }
}

AIPlayer::~AIPlayer()
{
    delete _ai;
}

void AIPlayer::forceMove()
{
    _ai->forceMove();
}

int AIPlayer::skill() const
{
    QMutexLocker(&const_cast<AIPlayer*>(this)->_mutex);
    return _skill;
}

void AIPlayer::setSkill(int skill)
{
    QMutexLocker(&this->_mutex);
    _skill = skill;
}

void AIPlayer::undoMove()
{
    _ai->movesUndone();
}

Point AIPlayer::doMove()
{
    return _ai->move(skill());
}
