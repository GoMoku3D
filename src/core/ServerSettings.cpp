/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "ServerSettings.h"

void ServerSettings::setPlayersInfo(QList<PlayerInfo> info)
{
    for (int i = 0; i < info.size(); i++) {
        setValue("Server/player_" + QString::number(i) + "_name", info.at(i).name());
        setValue("Server/player_" + QString::number(i) + "_color", info.at(i).color());
        setValue("Server/player_" + QString::number(i) + "_type", info.at(i).type());
    }
}

void ServerSettings::setDifficulty1(int d1)
{
    setValue("Server/d1", d1);
}

void ServerSettings::setDifficulty2(int d2)
{
    setValue("Server/d2", d2);
}

void ServerSettings::setServerPort(quint16 port)
{
    setValue("Server/port", port);
}

void ServerSettings::setTimerDuration(int sec)
{
    setValue("Server/timer", sec);
}

void ServerSettings::setMyColor(QColor color)
{
    setValue("Server/my_color", color);
}

void ServerSettings::setMyName(QString name)
{
    setValue("Server/my_name", name);
}

void ServerSettings::setNumberOfPlayers(int n)
{
    setValue("Server/players", n);
}

QList<PlayerInfo> ServerSettings::playersInfo() const
{
    QList<PlayerInfo> info;
    QList<QColor> defaultColors;
    defaultColors.append(QColor(255, 0, 0));
    defaultColors.append(QColor(0, 255, 0));
    defaultColors.append(QColor(0, 0, 255));

    for (int i = 0; i < 3; i++) {
        QString n = value("Server/player_" + QString::number(i) + "_name", "").toString();
        QColor c = value("Server/player_" + QString::number(i) + "_color",defaultColors[i]).value<QColor>();
        QString t = value("Server/player_" + QString::number(i) + "_type", "R").toString();
        info.append(PlayerInfo(n, c, t));
    }
    return info;
}

int ServerSettings::difficulty1() const
{
    return value("Server/d1", 5).toInt();
}

int ServerSettings::difficulty2() const
{
    return value("Server/d2", 1).toInt();
}

quint16 ServerSettings::serverPort() const
{
    return value("Server/port", 42000).toUInt();
}

int ServerSettings::timerDuration() const
{
    return value("Server/timer", 120).toInt();
}

QColor ServerSettings::myColor() const
{
    return value("Server/my_color", QColor(255, 255, 0)).value<QColor>();
}

QString ServerSettings::myName() const
{
    return value("Server/my_name","").toString();
}

int ServerSettings::numberOfPlayers() const
{
    return value("Server/players", 2).toInt();
}
