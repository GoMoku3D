/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QMutexLocker>

#include "GameLoop.h"
#include "GameMatrix.h"
#include "AIPlayer.h"
#include "SyncSharedCondition.h"


GameLoop::GameLoop(QList<Player*> players) : _players(players)
{
    _mat = GameMatrix::instance();
    _aboutToQuit = false;
    _resetLoop = false;
    _currentPlayer = _players[0];
    for (int i = 0; i < _players.size(); i++) {
        _winStatus.append(false);
    }
}

GameLoop::~GameLoop()
{
    Player *e = 0;
    foreach (e, _players) {
        delete e;
    }
    delete _mat;
    //SyncSharedCondition::instance()->tryLock();
    //SyncSharedCondition::instance()->unlock();
}

void GameLoop::setTurn(int plyUndone)
{
    _resetLoop = true;
    int newId = _currentPlayer->id() - plyUndone;
    while (newId < 0) {
        newId += _players.size();
    }
    if (plyUndone == 1) {
        _winStatus[newId] = false;
    }
    else {
        for (int i = 0; i < _winStatus.size(); i++) {
            _winStatus[i] = false;
        }
    }
    _currentPlayer = _players[newId];
}

void GameLoop::onLoadSetTurn(int id)
{
    _currentPlayer = _players[id];
}

void GameLoop::winStatus(QList<bool> status)
{
    _winStatus = status;
}

void GameLoop::forceMove()
{
    if (aboutToQuit()) {
        return;
    }
    AIPlayer *aiPlayer = 0;
    aiPlayer = qobject_cast<AIPlayer*>(_currentPlayer);
    if (aiPlayer != 0) aiPlayer->forceMove();
}

void GameLoop::stop()
{
    QMutexLocker locker(&_mutex);
    _aboutToQuit = true;
}

bool GameLoop::aboutToQuit() const
{
    QMutexLocker locker(&const_cast<GameLoop*>(this)->_mutex);
    return _aboutToQuit;
}

void GameLoop::run()
{
    bool someoneWon = false;
    bool valid = true;
    bool full = false;
    bool movesUndone = false;

    while (!someoneWon && valid && !aboutToQuit()) {
        if (movesUndone) {
            Player *e = 0;
            foreach (e, _players) {
                AIPlayer *ai = qobject_cast<AIPlayer*>(e);
                if (ai != 0) {
                    ai->undoMove();
                }
            }
        }
        int currentPlayerIndex = _currentPlayer->id();
        movesUndone = false;
        for (int i = currentPlayerIndex; i < _players.size() && valid && !full && !movesUndone && !aboutToQuit(); i++) {
            emit turn(i);
            AIPlayer *aiPlayer = qobject_cast<AIPlayer*>(_currentPlayer);
            if (aiPlayer != 0 && _settings.aiDelay() > 0) {
                sleep(_settings.aiDelay());
            }

            Move m = _currentPlayer->move();
            if (!_resetLoop && !aboutToQuit()) {
                valid = _mat->check(m.point());
                if (valid) {
                    emit moved(m);
                    _winStatus[i] = _mat->add(m);
                    if (_mat->isFull()) {
                        full = true;
                        for (int i = 0; i < _winStatus.size(); i++) {
                            _winStatus[i] = true;
                            someoneWon = true;
                        }
                    }
                    if (_winStatus.at(i)) {
                        someoneWon = true;
                    }
                    _currentPlayer = _players[(i + 1) % _players.size()];
                } else {
                    stop();
                    emit moved(m);
                }
            }
            else {
                _resetLoop = false;
                movesUndone = true;
            }
        }
    }

    if (valid && !aboutToQuit()) {
        QList<int> winners;
        for (int i = 0; i < _winStatus.size(); i++) {
            if (_winStatus[i] == true) {
                winners.append(i);
            }
        }
        if (winners.size() == 1) {
            emit win(winners.takeFirst());
        }
        if (winners.size() > 1) {
            emit draw(winners);
        }
    }
}

