/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "Point.h"
#include "GameMatrix.h"

class GameMatrix;

Point::Point()
{
    _x = -1;
    _y = -1;
    _z = -1;
}

Point::Point(int x, int y, int z)
{
    _x = x;
    _y = y;
    _z = z;
}

int Point::x() const
{
    return _x;
}

int Point::y() const
{
    return _y;
}

int Point::z() const
{
    return _z;
}

bool Point::isValid() const
{
    //Q_ASSERT(GameMatrix::instance() != 0);
    int upperBound = GameMatrix::instance()->d1() * GameMatrix::instance()->d2();
    return _x >= 0 && _y >= 0 && _z >= 0 && _x < upperBound && _y < upperBound && _z < upperBound;
}

bool Point::isNull() const
{
    return *this == Point();
}

QString Point::toString() const
{
    return QString("(" + QString::number(_x) + ", " + QString::number(_y) + ", " + QString::number(_z) + ")");
}

bool Point::operator== (const Point &p) const
{
    return _x == p._x && _y == p._y && _z == p._z;
}

bool Point::operator!= (const Point &p) const
{
    return !(*this == p);
}

bool Point::operator< (const Point &p) const
{
    if (_x < p._x) {
        return true;
    }
    if (_x == p._x && _y < p._y) {
        return true;
    }
    if (_x == p._x && _y == p._y && _z < p._z) {
        return true;
    }
    return false;
}

Point::operator bool() const
{
    return isValid();
}


uint qHash(Point p)
{
    return (qHash(p.x()) / 6 + qHash(p.y()) / 3 + qHash(p.z()) / 2);
}
