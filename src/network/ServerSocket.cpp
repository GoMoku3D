/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "ServerSocket.h"
#include "ServerSettings.h"

#define HANDLER_SIGNATURE(token) \
    void ServerSocket::parse_##token() { \
        QString elementName = name().toString(); \
        Q_ASSERT_X(elementName == #token, \
                   (QString("ServerSocket::parse_") + #token + "()").toUtf8().constData(), \
                   ("called while parsing " + elementName).toUtf8().constData()); \
        SET_HANDLER(#token)

ServerSocket::ServerSocket(QTcpSocket *socket) : StreamSocket(socket)
{
    changeState(Connected);
}

void ServerSocket::acceptJoin(int id)
{
    Q_ASSERT_X(id >= -1 && id <= 2, "ServerSocket::acceptJoin()", "invalid id");

    writeTextElement("joinACK", QString::number(id));
    changeState(AwaitingPlayers);
}

void ServerSocket::refuseJoin(int cause)
{
    writeTextElement("joinNAK", QString::number(cause));
}

void ServerSocket::sendGameSettings(int d1, int d2, int num, int timer, bool playing)
{
    writeStartElement("settings");
    writeAttribute("playing", playing ? "true" : "false");
    writeTextElement("difficultyOne", QString::number(d1));
    writeTextElement("difficultyTwo", QString::number(d2));
    writeTextElement("numberOfPlayers", QString::number(num));
    if (timer > 0) {
        writeTextElement("timerDuration", QString::number(timer));
    }
    writeEndElement();

    changeState(AwaitingJoinRequest);
}

void ServerSocket::sendHistory(QList<Move> history)
{
    writeStartElement("history");
    Move m;
    foreach (m, history) {
        serialize(m);
    }
    writeEndElement();
}

void ServerSocket::sendPlayerJoined(int id, QString name, QString type)
{
    Q_ASSERT_X(id >= 0 && id <= 2, "ServerSocket::sendPlayerJoined()", "invalid id");

    if (type != "H" && type != "R" && type != "A") {
        WARN(tr("refusing to send <playerJoined> with invalid player type"));
        return;
    }

    writeStartElement("playerJoined");
    writeAttribute("id", QString::number(id));
    writeTextElement("name", name);
    writeTextElement("type", type);
    writeEndElement();
}

void ServerSocket::sendPlayerLeft(int id)
{
    Q_ASSERT_X(id >= 0 && id <= 2, "ServerSocket::sendPlayerLeft()", "invalid id");

    writeTextElement("playerLeft", QString::number(id));
}

void ServerSocket::sendStartGame()
{
    changeState(Playing);

    writeEmptyElement("startGame");
    writeEmptyElement("foo");
}

BEGIN_TERMINAL_HANDLER(joinRequest)
    QString mode = attributes().value("gameMode").toString().toLower();
    if (atEnd()) return;
    QString name = readElementText();
    if (state() == AwaitingJoinRequest) {
        emit joinRequested(mode, name);
    } else {
        LOG("dropping <joinRequest> (state = " + stateString() + ")");
    }
    RESTORE_HANDLER("")
END_TERMINAL_HANDLER
