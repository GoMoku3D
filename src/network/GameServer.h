/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef GAMESERVER_H
#define GAMESERVER_H

#include <QList>
#include <QLinkedList>
#include <QMutex>
#include <QTcpServer>
#include <QStringList>

#include "Network.h"
#include "ServerSocket.h"

class HistoryModel;
class ChatWidget;
class ServerSettings;

class GameServer : public Network
{
	Q_OBJECT

    public:
        GameServer(QWidget *gui, HistoryModel *history);
        virtual ~GameServer();

        virtual Point requestMove();
        virtual void setupChat(ChatWidget *widget);
        virtual void setupGameLoop(GameLoop *gameLoop);

    public slots:
        void broadcastChatMessage(QString sender, QString message);
        virtual void broadcastMove(Move move);

    private:
        ServerSettings *_settings;
        QTcpServer *_listener;
        QLinkedList<ServerSocket*> _pendingConnections;
        QList<ServerSocket*> _remotePlayers;
        QList<ServerSocket*> _spectators;
        QStringList _names;
        HistoryModel *_history;
        ChatWidget *_chat;
        bool _gameInProgress;
        int _numberOfPlayers;
        int _turn;
        QString _localPlayerName;
        QMutex _mutex;

    private slots:
        void bootstrapClient(StreamSocket *socket);
        void checkStartGame();
        void handleError(QString errorMessage);
        void handleIncomingConnection();
        void handleJoinRequest(QString mode, QString name);
        void setTurn(int playerId);

    signals:
        void playerJoined(int id, QString name, QString type);
        void playerLeft(int id);
        void startGame();
};

#endif
