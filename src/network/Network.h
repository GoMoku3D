/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>

#include "Point.h"
#include "Move.h"

class QWidget;
class ChatWidget;
class GameLoop;

class Network : public QObject
{
	Q_OBJECT

	public:
		inline Network(QWidget *gui) : _gui(gui), _localPlayer(-1) {}
		inline virtual ~Network() {}

		virtual Point requestMove() = 0;
		virtual void setupChat(ChatWidget *widget) = 0;
        virtual void setupGameLoop(GameLoop *gameLoop) = 0;

    public slots:
        virtual void broadcastMove(Move move) = 0;

    protected:
		QWidget *_gui;
        int _localPlayer;

    signals:
        void error(QString errorMessage);
};

#endif
