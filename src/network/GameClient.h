/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef GAMECLIENT_H
#define GAMECLIENT_H

#include "Network.h"
#include "ClientSocket.h"

class GameClient : public Network
{
	Q_OBJECT

	public:
		GameClient(QWidget *gui, QString serverAddress, quint16 serverPort);
		virtual ~GameClient();

		virtual Point requestMove();
		virtual void setupChat(ChatWidget *widget);
        virtual void setupGameLoop(GameLoop *gameLoop);

	public slots:
        virtual void broadcastMove(Move move);
		void setLocalPlayer(int id);

	private:
		ClientSocket *_server;
};

#endif
