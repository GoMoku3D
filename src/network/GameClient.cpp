/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QTcpSocket>

#include "GameClient.h"
#include "GameLoop.h"
#include "ChatWidget.h"
#include "SyncSharedCondition.h"

GameClient::GameClient(QWidget *gui, QString serverAddress, quint16 serverPort) : Network(gui)
{
    Q_ASSERT_X(gui != 0, "GameClient::GameClient()", "gui must not be null");

	QTcpSocket *s = new QTcpSocket();
	_server = new ClientSocket(s);

    connect(_server, SIGNAL(statusChanged(QString)), gui, SLOT(setStatus(QString)));
    connect(_server, SIGNAL(receivedGameSettings(int, int, int, int, bool)), gui, SLOT(displaySettings(int, int, int, int, bool)));
    connect(_server, SIGNAL(joinAccepted(int)), this, SLOT(setLocalPlayer(int)));
    connect(_server, SIGNAL(joinAccepted(int)), gui, SLOT(acceptedJoinRequest(int)));
    connect(_server, SIGNAL(joinRefused(QString)), gui, SLOT(refusedJoinRequest(QString)));
    connect(_server, SIGNAL(playerJoined(int, QString, QString)), gui, SLOT(addPlayer(int, QString, QString)));
    connect(_server, SIGNAL(playerLeft(int)), gui, SLOT(removePlayer(int)));
    connect(_server, SIGNAL(receivedHistory(QList<Move>)), gui->parent(), SLOT(updateHistory(QList<Move>)));
    connect(_server, SIGNAL(startGame()), gui, SLOT(gameStarted()));
    connect(_server, SIGNAL(receivedMove(Move)), this, SLOT(broadcastMove(Move)));
    connect(_server, SIGNAL(protocolError(QString)), this, SIGNAL(error(QString)));
    connect(this, SIGNAL(error(QString)), gui, SLOT(networkError(QString)));
    connect(gui, SIGNAL(sendJoin(QString, QString)), _server, SLOT(joinGame(QString, QString)));

    _server->changeState(StreamSocket::Connecting);
	s->connectToHost(serverAddress, serverPort);
}

GameClient::~GameClient()
{
	if (_server) {
		delete _server;
	}
}

void GameClient::broadcastMove(Move m)
{
    // GameLoop sent the move
    GameLoop *gl = qobject_cast<GameLoop*>(sender());
    if (gl && (m.playerId() == _localPlayer)) {
        _server->sendMove(m);
    }

    // the server sent the move
    ClientSocket *socket = qobject_cast<ClientSocket*>(sender());
    if (socket) {
        QMutexLocker lock(SyncSharedCondition::instance());
        SyncSharedCondition::instance()->notifyMove(m.point());
    }
}

Point GameClient::requestMove()
{
    Point p = _server->takeFirstMove().point();
    if (p.isNull()) {
        _server->changeState(StreamSocket::AwaitingMove);
    }
    return p;
}

void GameClient::setupChat(ChatWidget *widget)
{
	if (widget) {
		connect(_server, SIGNAL(receivedChatMessage(QString, QString)), widget, SLOT(displayMessage(QString, QString)));
		connect(widget, SIGNAL(textEntered(QString, QString)), _server, SLOT(sendChatMessage(QString, QString)));
	}
}

void GameClient::setupGameLoop(GameLoop *gameLoop)
{
    connect(gameLoop, SIGNAL(moved(Move)), this, SLOT(broadcastMove(Move)));
}

void GameClient::setLocalPlayer(int id)
{
	Q_ASSERT_X(id >= -1 && id <= 2, "GameClient::setLocalPlayer()", "invalid id");
	Q_ASSERT_X(_server->state() == StreamSocket::AwaitingJoinAnswer,
               "GameClient::setLocalPlayer()", "called outside 'AwaitingJoinAnswer' state");

	_localPlayer = id;
}
