/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef STREAMSOCKET_H
#define STREAMSOCKET_H

#include <QtDebug>
#include <QLinkedList>
#include <QHostAddress>
#include <QObject>
#include <QMetaObject>
#include <QTcpSocket>
#include <QTimer>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "Global.h"
#include "Move.h"

#define CLASS_NAME(c) (c)->metaObject()->className()
#define PRINT_DEBUG_INFO(c) (QString(CLASS_NAME(c)) + " [" + (c)->_socket->peerAddress().toString() + "] : ")
#define LOG(msg) qDebug() << PRINT_DEBUG_INFO(this) + (msg)
#define WARN(msg) qWarning() << PRINT_DEBUG_INFO(this) + (msg)
#define ERR(msg) qCritical() << PRINT_DEBUG_INFO(this) + (msg)

#define SET_HANDLER(handler) _currentHandler = (handler);
#define RESTORE_HANDLER(handler) SET_HANDLER(handler)

#define BEGIN_NONTERMINAL_HANDLER(token) \
    HANDLER_SIGNATURE(token) \
    while (!atEnd()) { \
        readNext(); \
        elementName = name().toString(); \
        if (isStartElement()) { \
            parse(elementName); \
        } else if (isEndElement()) { \
            Q_ASSERT(elementName == #token);

#define END_NONTERMINAL_HANDLER(token) \
        } else { \
            LOG(QString("unexpected data in <") + #token + "> {" + tokenString() + "}"); \
        } \
    } \
    SET_HANDLER("") \
    }

#define BEGIN_TERMINAL_HANDLER(token) \
    HANDLER_SIGNATURE(token) \
    if (atEnd()) return;

#define END_TERMINAL_HANDLER }


class StreamSocket : public QObject, protected QXmlStreamReader, protected QXmlStreamWriter
{
    Q_OBJECT

    TEST_FRIEND(StreamSocketTest)
    TEST_FRIEND(ClientSocketTest)
    TEST_FRIEND(ServerSocketTest)

    public:
        virtual ~StreamSocket();

        enum ProtocolState {
            Unconnected,
            Listening,
            Connecting,
            Connected,
            OpeningStream,
            FullyOpened,
            Idle,
            AwaitingJoinRequest,
            AwaitingJoinAnswer,
            AwaitingPlayers,
            AwaitingGameStart,
            Playing,
            AwaitingMove,
            Closing,
            Closed
        };

        void changeState(ProtocolState state);
        ProtocolState state() const;
        QString stateString() const;
        Move takeFirstMove();

    public slots:
        void sendChatMessage(QString sender, QString msg);
        void sendMove(Move move);

    protected:
        StreamSocket(QTcpSocket *socket);
        bool parse(QString elementName);
        void serialize(Move m);

        static const QString _supportedProtocolVersion;
        QString _currentHandler;
        QString _localPlayerName;
        QTcpSocket *_socket;

    protected slots:
        virtual void openStream();
        virtual void closeStream();
        virtual void parseData();

    private:
        QLinkedList<Move> _buffer;
        ProtocolState _state;
        QTimer _pingTimer;
        QTimer _pongTimer;

    private slots:
        void handleError(QAbstractSocket::SocketError);
        void resetTimer();
        void sendPing();
        void sendPong();
        void timedOut();
        void parse_stream();
        void parse_playerJoined();
        void parse_name();
        void parse_color();
        void parse_type();
        void parse_playerLeft();
        void parse_chatMessage();
        void parse_move();
        void parse_player();
        void parse_point();
        void parse_x();
        void parse_y();
        void parse_z();
        void parse_startGame();
        void parse_ping();
        void parse_pong();

    signals:
        void handshakeCompleted(StreamSocket *socket);
        void protocolError(QString errorMessage);
        void statusChanged(QString status);
        void playerJoined(int id, QString name, QString type);
        void playerLeft(int id);
        void receivedChatMessage(QString sender, QString msg);
        void receivedMove(Move move);
        void startGame();
        void ping();
        void pong();
};

#endif
