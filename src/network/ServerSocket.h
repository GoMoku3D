/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef SERVERSOCKET_H
#define SERVERSOCKET_H

#include <QList>
#include <QTcpSocket>

#include "StreamSocket.h"
#include "Move.h"

class ServerSocket : public StreamSocket
{
    Q_OBJECT

    public:
        ServerSocket(QTcpSocket *socket);
        void acceptJoin(int id);
        void refuseJoin(int cause);

    public slots:
        void sendGameSettings(int d1, int d2, int num, int timer, bool playing);
        void sendHistory(QList<Move> history);
        void sendPlayerJoined(int id, QString name, QString type);
        void sendPlayerLeft(int id);
        void sendStartGame();

    private slots:
        void parse_joinRequest();

    signals:
        void joinRequested(QString mode, QString name);
};

#endif
