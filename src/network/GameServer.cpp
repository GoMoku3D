/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QMutexLocker>

#include "GameServer.h"
#include "ServerSettings.h"
#include "SyncSharedCondition.h"
#include "GameLoop.h"
#include "ChatWidget.h"
#include "HistoryModel.h"

GameServer::GameServer(QWidget *gui, HistoryModel *history) : Network(gui)
{
    Q_ASSERT_X(gui != 0, "GameClient::GameClient()", "gui must not be null");
    Q_ASSERT_X(history != 0, "GameClient::GameClient()", "history must not be null");

    _listener = new QTcpServer(this);
    _gameInProgress = false;
    _history = history;
    _chat = 0;
    _turn = -1;
    _localPlayerName = "";
    _settings = new ServerSettings();
    _numberOfPlayers = _settings->numberOfPlayers();
    QList<PlayerInfo> players = _settings->playersInfo();
    for (int i = 0; i < _numberOfPlayers; i++) {
        _remotePlayers.append(0);
        if (players.at(i).type() == "H") {
            // Human player
            _localPlayerName = players.at(i).name();
            _names.append(_localPlayerName);
            _localPlayer = i;
        } else if (players.at(i).type() == "A") {
            // AI player
            _names.append(players.at(i).name());
        } else {
            // Remote player
            _names.append("");
        }
    }
    if (_localPlayerName.isEmpty()) {
        _localPlayerName = _settings->myName();
    }

    // check that lists have been correctly populated
    Q_ASSERT(_remotePlayers.size() == _numberOfPlayers);
    Q_ASSERT(_spectators.size() == 0);
    Q_ASSERT(_pendingConnections.size() == 0);
    Q_ASSERT(_names.size() == _numberOfPlayers);

    connect(this, SIGNAL(error(QString)), gui, SLOT(networkError(QString)));
    connect(this, SIGNAL(playerJoined(int, QString, QString)), gui, SLOT(addPlayer(int, QString, QString)));
    connect(this, SIGNAL(playerLeft(int)), gui, SLOT(removePlayer(int)));
    connect(this, SIGNAL(startGame()), gui, SLOT(gameStarted()));

    // start listening for incoming connections
    connect(_listener, SIGNAL(newConnection()), this, SLOT(handleIncomingConnection()));
    if (!_listener->listen(QHostAddress::Any, _settings->serverPort())) {
        emit error(_listener->errorString());
    }

    checkStartGame();
}

GameServer::~GameServer()
{
    ServerSocket *s;
    foreach (s, _remotePlayers) {
        if (s) {
            delete s;
        }
    }

    qDeleteAll(_spectators);
    _spectators.clear();
    qDeleteAll(_pendingConnections);
    _pendingConnections.clear();

    delete _listener;
    delete _settings;
}

void GameServer::broadcastChatMessage(QString sender, QString message)
{
    if (message.isEmpty()) {
        return;
    }

    if (sender.isEmpty()) {
        sender = _localPlayerName;
    }
    _chat->displayMessage(sender, message);

    ServerSocket *socket;
    foreach (socket, _remotePlayers) {
        if (socket) {
            socket->sendChatMessage(sender, message);
        }
    }
    foreach (socket, _spectators) {
        socket->sendChatMessage(sender, message);
    }
}

void GameServer::broadcastMove(Move m)
{
    bool broadcast = false;

    // a client sent the move
    ServerSocket *s = qobject_cast<ServerSocket*>(sender());
    if (s) {
        QMutexLocker lock(SyncSharedCondition::instance());
        SyncSharedCondition::instance()->notifyMove(m.point());
        broadcast = true;
    }

    // GameLoop sent the move
    GameLoop *gl = qobject_cast<GameLoop*>(sender());
    if (gl) {
        QString playerType = _settings->playersInfo().at(m.playerId()).type();
        if (playerType == "H" || playerType == "A") {
            broadcast = true;
        }
    }

    if (broadcast) {
        // send the move to remote players and spectators
        ServerSocket *socket;
        foreach(socket, _remotePlayers) {
            if (socket && socket != s) {
                socket->sendMove(m);
            }
        }
        foreach(socket, _spectators) {
            socket->sendMove(m);
        }
    }
}

Point GameServer::requestMove()
{
    QMutexLocker lock(&_mutex);
    Move m = _remotePlayers.at(_turn)->takeFirstMove();
    if (m.point().isNull()) {
        _remotePlayers.at(_turn)->changeState(StreamSocket::AwaitingMove);
    } else {
        broadcastMove(m);
    }
    return m.point();
}

void GameServer::setupChat(ChatWidget *widget)
{
    if (!widget) {
        return;
    }

    _chat = widget;
    connect(_chat, SIGNAL(textEntered(QString, QString)), this, SLOT(broadcastChatMessage(QString, QString)));

    ServerSocket *s;
    foreach (s, _remotePlayers) {
        if (s) {
            connect(s, SIGNAL(receivedChatMessage(QString, QString)), this, SLOT(broadcastChatMessage(QString, QString)));
        }
    }
    foreach (s, _spectators) {
        connect(s, SIGNAL(receivedChatMessage(QString, QString)), this, SLOT(broadcastChatMessage(QString, QString)));
    }
}

void GameServer::setupGameLoop(GameLoop *gameLoop)
{
    connect(gameLoop, SIGNAL(turn(int)), this, SLOT(setTurn(int)), Qt::DirectConnection);
    connect(gameLoop, SIGNAL(moved(Move)), this, SLOT(broadcastMove(Move)));
}

void GameServer::bootstrapClient(StreamSocket *socket)
{
    ServerSocket *client = qobject_cast<ServerSocket*>(socket);
    if (!client) {
        return;
    }

    // send settings
    client->sendGameSettings(_settings->difficulty1(), _settings->difficulty2(), _numberOfPlayers,
                             _settings->timerDuration(), _gameInProgress);

    // send a list of players that have already joined
    for (int i = 0; i < _numberOfPlayers; i++) {
        if (!_names.at(i).isEmpty()) {
            QString type = _settings->playersInfo().at(i).type();
            client->sendPlayerJoined(i, _names.at(i), (type == "H" ? "R" : type));
        }
    }
}

void GameServer::checkStartGame()
{
    // check to see if all players have joined
    int i = 0;
    for (; i < _numberOfPlayers && !_names.at(i).isEmpty(); i++) {}
    if (i == _numberOfPlayers) {
        // start the game
        _gameInProgress = true;
        ServerSocket *s;
        foreach (s, _remotePlayers) {
            if (s) {
                s->sendStartGame();
            }
        }
        foreach (s, _spectators) {
            s->sendStartGame();
        }
        emit startGame();
    }
}

void GameServer::handleError(QString errorMessage)
{
    ServerSocket *socket = qobject_cast<ServerSocket*>(sender());
    if (!socket) {
        return;
    }

    QLinkedList<ServerSocket*>::iterator it = qFind(_pendingConnections.begin(), _pendingConnections.end(), socket);
    if (it != _pendingConnections.end()) {
        _pendingConnections.erase(it);
        socket->deleteLater();
    }
    int i = _spectators.indexOf(socket);
    if (i >= 0) {
        _spectators.removeAt(i);
        _names.removeAt(i + _numberOfPlayers);
        socket->deleteLater();
    }
    i = _remotePlayers.indexOf(socket);
    if (i >= 0) {
        _remotePlayers.removeAt(i);
        _names[i] = "";
        if (_gameInProgress) {
            emit error(errorMessage);
        } else {
            emit playerLeft(i);
            ServerSocket *s;
            foreach (s, _remotePlayers) {
                if (s && s != socket) {
                    s->sendPlayerLeft(i);
                }
            }
            foreach (s, _spectators) {
                s->sendPlayerLeft(i);
            }
        }
        socket->deleteLater();
    }
}

void GameServer::handleIncomingConnection()
{
    while (_listener->hasPendingConnections()) {
        ServerSocket *s = new ServerSocket(_listener->nextPendingConnection());
        connect(s, SIGNAL(protocolError(QString)), this, SLOT(handleError(QString)));
        connect(s, SIGNAL(handshakeCompleted(StreamSocket *)), this, SLOT(bootstrapClient(StreamSocket *)));
        connect(s, SIGNAL(joinRequested(QString, QString)), this, SLOT(handleJoinRequest(QString, QString)));
        _pendingConnections.append(s);
    }
}

void GameServer::handleJoinRequest(QString mode, QString name)
{
    ServerSocket *requester = qobject_cast<ServerSocket*>(sender());
    if (!requester) {
        return;
    }

    if (name == _localPlayerName || _names.contains(name)) {
        requester->refuseJoin(2);
    } else if (mode == "player") {
        if (_gameInProgress) {
            requester->refuseJoin(1);
        } else {
            // find first free id
            int freePos = -1;
            for (int i = 0; i < _numberOfPlayers && freePos == -1; i++) {
                if (_names.at(i).isEmpty()) {
                    freePos = i;
                }
            }
            if (freePos >= 0) {
                // accept player, moving its socket from _pendingConnections to _remotePlayers
                _pendingConnections.removeAll(requester);
                _remotePlayers[freePos] = requester;
                _names[freePos] = name;
                connect(requester, SIGNAL(receivedMove(Move)), this, SLOT(broadcastMove(Move)));
                if (_chat) {
                    connect(requester, SIGNAL(receivedChatMessage(QString, QString)), this, SLOT(broadcastChatMessage(QString, QString)));
                }
                requester->acceptJoin(freePos);
                emit playerJoined(freePos, name, "R");
                ServerSocket *s;
                foreach (s, _remotePlayers) {
                    if (s && s != requester) {
                        s->sendPlayerJoined(freePos, name, "R");
                    }
                }
                foreach (s, _spectators) {
                    s->sendPlayerJoined(freePos, name, "R");
                }
                checkStartGame();
            } else {
                qWarning() << "GameServer::handleJoinRequest() : all players seem to have joined but game hasn't started!";
            }
        }
    } else if (mode == "spectator") {
        // accept new spectator
        if (_gameInProgress) {
            requester->sendHistory(_history->moveList());
        }
        _pendingConnections.removeAll(requester);
        _spectators.append(requester);
        _names.append(name);
        if (_chat) {
            connect(requester, SIGNAL(receivedChatMessage(QString, QString)), this, SLOT(broadcastChatMessage(QString, QString)));
        }
        requester->acceptJoin(-1);
        if (_gameInProgress) {
            requester->sendStartGame();
        }
    } else {
        qDebug() << "GameServer::handleJoinRequest() : invalid mode value" << mode;
    }
}

void GameServer::setTurn(int playerId)
{
    Q_ASSERT_X(playerId >= 0 && playerId < _numberOfPlayers, "GameServer::setTurn()", "invalid id");
    Q_ASSERT(_gameInProgress == true);

    QMutexLocker lock(&_mutex);
    _turn = playerId;
}
