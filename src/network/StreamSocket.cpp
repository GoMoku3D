/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QCoreApplication>
#include <QVariant>

#include "StreamSocket.h"

#define HANDLER_SIGNATURE(token) \
    void StreamSocket::parse_##token() { \
        QString elementName = name().toString(); \
        Q_ASSERT_X(elementName == #token, \
                   (QString("StreamSocket::parse_") + #token + "()").toUtf8().constData(), \
                   ("called while parsing " + elementName).toUtf8().constData()); \
        SET_HANDLER(#token)

const QString StreamSocket::_supportedProtocolVersion = "1.0";

StreamSocket::StreamSocket(QTcpSocket *socket)
    : QXmlStreamReader(socket), QXmlStreamWriter(socket)
{
    Q_ASSERT_X(socket != 0, "StreamSocket::StreamSocket()", "socket must not be null");

    _socket = socket;
    _socket->setParent(this);
    _state = Unconnected;
    _pingTimer.setInterval(20000);
    _pongTimer.setInterval(10000);
    _pongTimer.setSingleShot(true);

    connect(_socket, SIGNAL(readyRead()), this, SLOT(parseData()));
    connect(_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(handleError(QAbstractSocket::SocketError)));
    connect(&_pingTimer, SIGNAL(timeout()), this, SLOT(sendPing()));
    connect(&_pingTimer, SIGNAL(timeout()), this, SLOT(resetTimer()));
    connect(&_pongTimer, SIGNAL(timeout()), this, SLOT(timedOut()));
    connect(this, SIGNAL(ping()), this, SLOT(sendPong()));
    connect(this, SIGNAL(pong()), &_pongTimer, SLOT(stop()));
}

StreamSocket::~StreamSocket()
{
    closeStream();
    _socket->deleteLater();
    QCoreApplication::sendPostedEvents(_socket, 0);
}

void StreamSocket::changeState(ProtocolState state)
{
    _state = state;

    if (state == Connecting) {
        emit statusChanged(YELLOW_TEXT(tr("resolving hostname...")));
    } else if (state == Connected) {
        openStream();
    } else if (state == OpeningStream) {
        emit statusChanged(YELLOW_TEXT(tr("negotiating stream...")));
    } else if (state == FullyOpened) {
        emit statusChanged(GREEN_TEXT(tr("connected")));
        emit handshakeCompleted(this);
        _pingTimer.start();
    } else if (state == Closing) {
        closeStream();
    } else if (state == Closed) {
        emit statusChanged(RED_TEXT(tr("not connected")));
    }
}

StreamSocket::ProtocolState StreamSocket::state() const
{
    return _state;
}

QString StreamSocket::stateString() const
{
    switch(_state) {
        case Unconnected: return "Unconnected";
        case Listening: return "Listening";
        case Connecting: return "Connecting";
        case Connected: return "Connected";
        case OpeningStream: return "OpeningStream";
        case FullyOpened: return "FullyOpened";
        case Idle: return "Idle";
        case AwaitingJoinRequest: return "AwaitingJoinRequest";
        case AwaitingJoinAnswer: return "AwaitingJoinAnswer";
        case AwaitingPlayers: return "AwaitingPlayers";
        case AwaitingGameStart: return "AwaitingGameStart";
        case Playing: return "Playing";
        case AwaitingMove: return "AwaitingMove";
        case Closing: return "Closing";
        case Closed: return "Closed";
        default: return "Unknown";
    }
}

Move StreamSocket::takeFirstMove()
{
    if (_buffer.isEmpty()) {
        return Move();
    } else {
        return _buffer.takeFirst();
    }
}

void StreamSocket::sendChatMessage(QString sender, QString msg)
{
    if (msg.isEmpty()) {
        return;
    }
    if (sender.isEmpty()) {
        if (_localPlayerName.isEmpty()) {
            WARN(tr("refusing to send <chatMessage> with empty sender"));
            return;
        }
        sender = _localPlayerName;
    }

    writeStartElement("chatMessage");
    writeAttribute("from", sender);
    writeCharacters(msg);
    writeEndElement();
}

void StreamSocket::sendMove(Move move)
{
    serialize(move);
}

void StreamSocket::openStream()
{
    changeState(OpeningStream);

    writeStartDocument();
    writeDefaultNamespace("http://www.itworks.org/GoMoku3D/NetworkProtocol");
    writeStartElement("stream");
    writeAttribute("version", _supportedProtocolVersion);
    writeEmptyElement("foo");
}

void StreamSocket::closeStream()
{
    _pingTimer.stop();
    _pongTimer.stop();

    if (_socket->state() == QAbstractSocket::ConnectedState) {
        writeEndElement();
        writeEndDocument();
        _socket->disconnectFromHost();
    }

    changeState(Closed);
}

void StreamSocket::handleError(QAbstractSocket::SocketError)
{
    emit protocolError(_socket->errorString());
}

void StreamSocket::timedOut()
{
    emit protocolError(tr("Communication with the remote host timed out"));
}

void StreamSocket::serialize(Move m)
{
    writeStartElement("move");
    writeTextElement("player", QString::number(m.playerId()));
    writeStartElement("point");
    writeTextElement("x", QString::number(m.point().x()));
    writeTextElement("y", QString::number(m.point().y()));
    writeTextElement("z", QString::number(m.point().z()));
    writeEndElement();
    writeEndElement();
}

void StreamSocket::sendPing()
{
    if (_socket->state() == QAbstractSocket::ConnectedState) {
        writeEmptyElement("ping");
        writeEmptyElement("ping");
    }
}

void StreamSocket::sendPong()
{
    if (_socket->state() == QAbstractSocket::ConnectedState) {
        writeEmptyElement("pong");
        writeEmptyElement("pong");
    }
}

void StreamSocket::resetTimer()
{
    _pongTimer.start(10000);
}

bool StreamSocket::parse(QString elementName)
{
    if (elementName.isEmpty()) {
        return false;
    }

    QString methodName = "parse_" + elementName;
    if (QMetaObject::invokeMethod(this, methodName.toUtf8().constData(), Qt::DirectConnection)) {
        return true;
    } else {
        if (elementName != "foo") {
            WARN(tr("unknown protocol message %1").arg("<" + elementName + ">"));
        }
        return false;
    }
}

void StreamSocket::parseData()
{
    while (!atEnd()) {
        if (parse(_currentHandler)) {
            continue;
        }

        QXmlStreamReader::TokenType token = readNext();

        switch (token) {
            case StartDocument:
                LOG("document start");
                // do nothing
                break;
            case StartElement:
                parse(name().toString());
                break;
            case EndDocument:
                LOG("document end");
                // do nothing
                break;
            case EndElement:
            case Comment:
            case Invalid:
                // do nothing
                break;
            default:
                LOG("unable to handle unexpected token {" + tokenString() + "}");
        }
    }

    if (hasError()) {
        switch (error()) {
            case NoError:
            case PrematureEndOfDocumentError:
                // do nothing
                break;
            default:
                emit protocolError(errorString());
        }
    }
}

BEGIN_TERMINAL_HANDLER(stream)
    QString protocolVersion = attributes().value("version").toString();
    if (atEnd()) return;
    if (protocolVersion == _supportedProtocolVersion) {
        changeState(FullyOpened);
    } else {
        changeState(Closing);
    }
    RESTORE_HANDLER("")
END_TERMINAL_HANDLER

HANDLER_SIGNATURE(playerJoined)
    if (isStartElement() && elementName == "playerJoined") {
        bool ok;
        int id = attributes().value("id").toString().toInt(&ok);
        if (!ok) {
            WARN(tr("invalid value for attribute 'id' in <playerJoined> : %1").arg(attributes().value("id").toString()));
            id = -1;
        }
        setProperty("playerJoined/id", id);
    }
    while (!atEnd()) {
        readNext();
        elementName = name().toString();
        if (isStartElement()) {
            parse(elementName);
        } else if (isEndElement()) {
            Q_ASSERT(elementName == "playerJoined");
            emit playerJoined(property("playerJoined/id").toInt(),
                              property("playerJoined/name").toString(),
                              property("playerJoined/type").toString());
            RESTORE_HANDLER("")
            return;
END_NONTERMINAL_HANDLER(playerJoined)

BEGIN_TERMINAL_HANDLER(name)
    setProperty("playerJoined/name", QVariant(readElementText()));
    RESTORE_HANDLER("playerJoined")
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(color)
    LOG("ignoring <color> element in <playerJoined> message");
    RESTORE_HANDLER("playerJoined")
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(type)
    setProperty("playerJoined/type", QVariant(readElementText()));
    RESTORE_HANDLER("playerJoined")
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(playerLeft)
    bool ok;
    int id = readElementText().toInt(&ok);
    if (ok) {
        emit playerLeft(id);
    } else {
        WARN(tr("invalid content in <playerLeft> : %1").arg(readElementText()));
    }
    RESTORE_HANDLER("")
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(chatMessage)
    QString from = attributes().value("from").toString();
    if (atEnd()) return;
    QString msg = readElementText();
    if (from.isEmpty() && msg.isEmpty()) {
        LOG("dropping empty <chatMessage>");
    } else if (from.isEmpty()) {
        LOG("dropping <chatMessage> without sender");
    } else if (msg.isEmpty()) {
        LOG("dropping <chatMessage> with empty message");
    } else if (state() == Playing || state() == AwaitingMove) {
        emit receivedChatMessage(from, msg);
    } else {
        LOG("dropping <chatMessage> (state = " + stateString() + ")");
    }
    RESTORE_HANDLER("")
END_TERMINAL_HANDLER

BEGIN_NONTERMINAL_HANDLER(move)
    Move m(property("move/player").toInt(),
           property("move/point").value<Point>());
    if (state() == AwaitingMove) {
        emit receivedMove(m);
    } else if (state() == Playing) {
        _buffer.append(m);
    } else {
        LOG("dropping <move> (state = " + stateString() + ")");
    }
    RESTORE_HANDLER("")
    return;
END_NONTERMINAL_HANDLER(move)

BEGIN_TERMINAL_HANDLER(player)
    bool ok;
    int id = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <player> value in <move> : %1").arg(readElementText()));
        id = -1;
    }
    setProperty("move/player", QVariant(id));
    RESTORE_HANDLER("move")
END_TERMINAL_HANDLER

BEGIN_NONTERMINAL_HANDLER(point)
    Point p(property("move/point/x").toInt(),
            property("move/point/y").toInt(),
            property("move/point/z").toInt());
    setProperty("move/point", QVariant::fromValue(p));
    RESTORE_HANDLER("move")
    return;
END_NONTERMINAL_HANDLER(point)

BEGIN_TERMINAL_HANDLER(x)
    bool ok;
    int value = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <x> value in <point> : %1").arg(readElementText()));
        value = -1;
    }
    setProperty("move/point/x", QVariant(value));
    RESTORE_HANDLER("point")
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(y)
    bool ok;
    int value = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <y> value in <point> : %1").arg(readElementText()));
        value = -1;
    }
    setProperty("move/point/y", QVariant(value));
    RESTORE_HANDLER("point")
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(z)
    bool ok;
    int value = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <z> value in <point> : %1").arg(readElementText()));
        value = -1;
    }
    setProperty("move/point/z", QVariant(value));
    RESTORE_HANDLER("point")
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(startGame)
    readNext();
    if (atEnd()) return;
    elementName = name().toString();
    if (isEndElement()) {
        Q_ASSERT(elementName == "startGame");
        RESTORE_HANDLER("")
        if (state() == AwaitingGameStart) {
            changeState(Playing);
            emit startGame();
        }
    } else {
        LOG("unexpected data in <startGame> - ignoring");
    }
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(ping)
    readNext();
    if (atEnd()) return;
    elementName = name().toString();
    if (isEndElement()) {
        Q_ASSERT(elementName == "ping");
        RESTORE_HANDLER("")
        emit ping();
    } else {
        LOG("unexpected data in <ping> - ignoring");
    }
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(pong)
    readNext();
    if (atEnd()) return;
    elementName = name().toString();
    if (isEndElement()) {
        Q_ASSERT(elementName == "pong");
        RESTORE_HANDLER("")
        emit pong();
    } else {
        LOG("unexpected data in <pong> - ignoring");
    }
END_TERMINAL_HANDLER
