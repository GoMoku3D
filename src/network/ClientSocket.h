/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef CLIENTSOCKET_H
#define CLIENTSOCKET_H

#include <QList>
#include <QTcpSocket>

#include "StreamSocket.h"
#include "Move.h"

class ClientSocket : public StreamSocket
{
	Q_OBJECT

	public:
		ClientSocket(QTcpSocket *socket);

    public slots:
        void joinGame(QString mode, QString name);
        void cancelJoin();

    private slots:
        void notifyHostFound();
        void parse_joinACK();
        void parse_joinNAK();
        void parse_settings();
        void parse_difficultyOne();
        void parse_difficultyTwo();
        void parse_numberOfPlayers();
        void parse_timerDuration();
        void parse_history();
        void parse_history_move();
        void parse_history_player();
        void parse_history_point();
        void parse_history_x();
        void parse_history_y();
        void parse_history_z();

    signals:
        void joinAccepted(int id);
        void joinRefused(QString cause);
        void receivedGameSettings(int d1, int d2, int num, int timer, bool playing);
        void receivedHistory(QList<Move> history);
};

#endif
