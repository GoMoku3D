/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QVariant>

#include "ClientSocket.h"

#define HANDLER_SIGNATURE(token) \
    void ClientSocket::parse_##token() { \
        QString elementName = name().toString(); \
        Q_ASSERT_X(elementName == #token, \
                   (QString("ClientSocket::parse_") + #token + "()").toUtf8().constData(), \
                   ("called while parsing " + elementName).toUtf8().constData()); \
        SET_HANDLER(#token)

ClientSocket::ClientSocket(QTcpSocket *socket) : StreamSocket(socket)
{
    connect(_socket, SIGNAL(hostFound()), this, SLOT(notifyHostFound()));
	connect(_socket, SIGNAL(connected()), this, SLOT(openStream()));
}

void ClientSocket::joinGame(QString mode, QString name)
{
    mode = mode.toLower();

    Q_ASSERT_X(mode == "player" || mode == "spectator", "ClientSocket::joinGame()", "invalid mode value");
    Q_ASSERT_X(!name.isEmpty(), "ClientSocket::joinGame()", "null or empty string passed as name");

    if (state() != Idle) {
        LOG("not sending <joinRequest> while state = " + stateString());
        return;
    }

    writeStartElement("joinRequest");
    writeAttribute("gameMode", mode);
    writeCharacters(name);
    writeEndElement();

    _localPlayerName = name;
    changeState(AwaitingJoinAnswer);
}

void ClientSocket::cancelJoin()
{
    if (state() != AwaitingJoinAnswer && state() != AwaitingGameStart) {
        LOG("no previous join request can be cancelled (state = " + stateString() + ")");
        return;
    }

    writeTextElement("playerLeft", QString::number(-1));

    _localPlayerName = "";
    changeState(Idle);
}

void ClientSocket::notifyHostFound()
{
    emit statusChanged(YELLOW_TEXT(tr("connecting...")));
}

BEGIN_TERMINAL_HANDLER(joinACK)
    bool ok;
    int id = readElementText().toInt(&ok);
    if (!ok || id < -1 || id > 2) {
        WARN(tr("invalid content in <joinACK> : %1").arg(readElementText()));
    } else {
        emit joinAccepted(id);
        changeState(AwaitingGameStart);
    }
    RESTORE_HANDLER("")
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(joinNAK)
    bool ok;
    int errorCode = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid content in <joinNAK> : %1").arg(readElementText()));
        errorCode = -1;
    }
    QString cause;
    switch (errorCode) {
        case 1:
            cause = tr("You cannot join while a match\nis already in progress.");
            break;
        case 2:
            cause = tr("The name you have chosen is already\nbeing used by someone else.");
            break;
        case 3:
            cause = tr("The server you are connecting to\nis using an incompatible protocol version.");
            break;
        default:
            cause = tr("An unknown error occurred.");
    }
    _localPlayerName = "";
    emit joinRefused(cause);
    changeState(Idle);
    RESTORE_HANDLER("")
END_TERMINAL_HANDLER

BEGIN_NONTERMINAL_HANDLER(settings)
    changeState(Idle);
    QString playing = attributes().value("playing").toString().toLower();
    emit receivedGameSettings(property("settings/d1").toInt(),
                              property("settings/d2").toInt(),
                              property("settings/numberOfPlayers").toInt(),
                              property("settings/timerDuration").toInt(),
                              playing == "true" ? true : false);
    RESTORE_HANDLER("")
    return;
END_NONTERMINAL_HANDLER(settings)

BEGIN_TERMINAL_HANDLER(difficultyOne)
    bool ok;
    int value = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <difficultyOne> value in <settings> : %1").arg(readElementText()));
        value = -1;
    }
    setProperty("settings/d1", QVariant(value));
    RESTORE_HANDLER("settings")
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(difficultyTwo)
    bool ok;
    int value = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <difficultyTwo> value in <settings> : %1").arg(readElementText()));
        value = -1;
    }
    setProperty("settings/d2", QVariant(value));
    RESTORE_HANDLER("settings")
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(numberOfPlayers)
    bool ok;
    int value = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <numberOfPlayers> value in <settings> : %1").arg(readElementText()));
        value = -1;
    }
    setProperty("settings/numberOfPlayers", QVariant(value));
    RESTORE_HANDLER("settings")
END_TERMINAL_HANDLER

BEGIN_TERMINAL_HANDLER(timerDuration)
    bool ok;
    int value = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <timerDuration> value in <settings> : %1").arg(readElementText()));
        value = -1;
    }
    setProperty("settings/timerDuration", QVariant(value));
    RESTORE_HANDLER("settings")
END_TERMINAL_HANDLER

HANDLER_SIGNATURE(history)
    while (!atEnd()) {
        readNext();
        elementName = name().toString();
        if (isStartElement()) {
            parse("history_" + elementName);
        } else if (isEndElement()) {
            Q_ASSERT(elementName == "history");
            QList<Move> moveList;
            for (int i = 0; i < property("history/size").toInt(); i++) {
                QVariant x = property(("history/move_" + QString::number(i)).toUtf8().constData());
                moveList.append(x.value<Move>());
            }
            emit receivedHistory(moveList);
            setProperty("history/size", QVariant(0));
            RESTORE_HANDLER("")
            return;
END_NONTERMINAL_HANDLER(history)

void ClientSocket::parse_history_move() {
    QString elementName = name().toString();
    qDebug() << "parsing" << "history_" + elementName;
    Q_ASSERT_X(elementName == "move", "ClientSocket::parse_history_move()",
               ("called while parsing " + elementName).toUtf8().constData());
    SET_HANDLER("history_move")
    while (!atEnd()) {
        readNext();
        elementName = name().toString();
        if (isStartElement()) {
            parse("history_" + elementName);
        } else if (isEndElement()) {
            Q_ASSERT(elementName == "move");
            Move m(property("move/player").toInt(),
                   property("move/point").value<Point>());
            int n = property("history/size").toInt();
            setProperty(("history/move_" + QString::number(n)).toUtf8().constData(), QVariant::fromValue(m));
            n++;
            setProperty("history/size", QVariant(n));
            RESTORE_HANDLER("history")
            return;
        } else {
            LOG("unexpected data in <move> {" + tokenString() + "}");
        }
    }
    RESTORE_HANDLER("")
}

void ClientSocket::parse_history_player() {
    QString elementName = name().toString();
    qDebug() << "parsing" << "history_" + elementName;
    Q_ASSERT_X(elementName == "player", "ClientSocket::parse_history_player()",
               ("called while parsing " + elementName).toUtf8().constData());
    SET_HANDLER("history_player")
    if (atEnd()) return;
    bool ok;
    int id = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <player> value in <move> : %1").arg(readElementText()));
        id = -1;
    }
    setProperty("move/player", QVariant(id));
    RESTORE_HANDLER("history_move")
}

void ClientSocket::parse_history_point() {
    QString elementName = name().toString();
    qDebug() << "parsing" << "history_" + elementName;
    Q_ASSERT_X(elementName == "point", "ClientSocket::parse_history_point()",
               ("called while parsing " + elementName).toUtf8().constData());
    SET_HANDLER("history_point")
    while (!atEnd()) {
        readNext();
        elementName = name().toString();
        if (isStartElement()) {
            parse("history_" + elementName);
        } else if (isEndElement()) {
            Q_ASSERT(elementName == "point");
            Point p(property("move/point/x").toInt(),
                    property("move/point/y").toInt(),
                    property("move/point/z").toInt());
            setProperty("move/point", QVariant::fromValue(p));
            RESTORE_HANDLER("history_move")
            return;
        } else {
            LOG("unexpected data in <point> {" + tokenString() + "}");
        }
    }
    RESTORE_HANDLER("")
}

void ClientSocket::parse_history_x() {
    QString elementName = name().toString();
    qDebug() << "parsing" << "history_" + elementName;
    Q_ASSERT_X(elementName == "x", "ClientSocket::parse_history_x()",
                   ("called while parsing " + elementName).toUtf8().constData());
    SET_HANDLER("history_x")
    if (atEnd()) return;
    bool ok;
    int value = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <x> value in <point> : %1").arg(readElementText()));
        value = -1;
    }
    setProperty("move/point/x", QVariant(value));
    RESTORE_HANDLER("history_point")
}

void ClientSocket::parse_history_y() {
    QString elementName = name().toString();
    qDebug() << "parsing" << "history_" + elementName;
    Q_ASSERT_X(elementName == "y", "ClientSocket::parse_history_y()",
               ("called while parsing " + elementName).toUtf8().constData());
    SET_HANDLER("history_y")
    if (atEnd()) return;
    bool ok;
    int value = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <y> value in <point> : %1").arg(readElementText()));
        value = -1;
    }
    setProperty("move/point/y", QVariant(value));
    RESTORE_HANDLER("history_point")
}

void ClientSocket::parse_history_z() {
    QString elementName = name().toString();
    qDebug() << "parsing" << "history_" + elementName;
    Q_ASSERT_X(elementName == "z", "ClientSocket::parse_history_z()",
               ("called while parsing " + elementName).toUtf8().constData());
    SET_HANDLER("history_z")
    if (atEnd()) return;
    bool ok;
    int value = readElementText().toInt(&ok);
    if (!ok) {
        WARN(tr("invalid <z> value in <point> : %1").arg(readElementText()));
        value = -1;
    }
    setProperty("move/point/z", QVariant(value));
    RESTORE_HANDLER("history_point")
}
