/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef CCTHREAT_H
#define CCTHREAT_H

#include <QVector>
#include <QList>

#include "Threat.h"
#include "CCThreatSearchAI.h"
#include "Point.h"

class CCThreat : public Threat
{
    friend class CCThreatSearchAI;
    TEST_FRIEND(CCThreatTest)

    private:
        class ConnectedComponent;

    public:
        CCThreat(CCThreatSearchAI *ai);

    protected:
        virtual void insertHook(Point p);
        virtual void scanFromMatrixHook();

    private:
        QVector< QList<ConnectedComponent> > _opponentCC;
        int find(int id, Point p) const;
        void explore(Point p, int id, QSet<Point> &set);
        void merge(Point p1, Point p2);
        static QList<Point> adjacentList(Point p);

    private:
        class ConnectedComponent
        {
            public:
                QSet<Point> nodes;
                int min_x;
                int min_y;
                int min_z;
                int max_x;
                int max_y;
                int max_z;
                ConnectedComponent(QSet<Point> items);
                ConnectedComponent(Point p);
        };
};

#endif

