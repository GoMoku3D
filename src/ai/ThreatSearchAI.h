/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef THREATSEARCHAI_H
#define THREATSEARCHAI_H

#include <QList>

#include "AI.h"
#include "GameMatrix.h"
#include "Point.h"
#include "Threat.h"

class ThreatSearchAI : public AI
{
    TEST_FRIEND(ThreatSearchAITest)

    public:
        ThreatSearchAI(int id);
        virtual ~ThreatSearchAI();
        virtual Point move(int level);
        void forceMove();
        void init();
        virtual void movesUndone();

    protected:
        int _d1;
        int _d2;
        Threat *_threat;
        GameMatrix *_mat;
        virtual Point defenseHook() const;
        virtual Threat *createThreat();

    private:
        Point defense(int level) const;
        Point randomPoint() const;
        int countId(int id, Point p, Threat::Direction dir) const;
        QList<Point> freePoints(Point p, Threat::Direction dir) const;
};

#endif
