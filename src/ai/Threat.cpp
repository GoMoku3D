/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QTime>
#include <QVector>

#include "Threat.h"
#include "ThreatSearchAI.h"

Threat::Threat(ThreatSearchAI *ai)
{
    qsrand(QTime::currentTime().msec());
    _ai = ai;
    _mat = GameMatrix::instance();
    _d1 = _mat->d1();
    _d2 = _mat->d2();
    _priority = new Node*[_d1 - 1];
    for (int i = 0; i < _d1 - 1; i++) {
        _priority[i] = 0;
    }
}

Threat::~Threat()
{
    for (int i = 0; i < _d1 - 1; i++) {
        delete _priority[i];
    }
    delete _priority;
}

void Threat::insert(Point point)
{
    for (int i = 0; i < _d1; i++) {
        evalInsert(point, i, DIR_X);
        evalInsert(point, i, DIR_Y);
        evalInsert(point, i, DIR_Z);
    }
    insertHook(point);
}

void Threat::scanFromMatrix()
{
    int dim = _d1 * _d2;
    for (int x = 0; x < dim; x++) {
        for (int y = 0; y < dim; y++) {
            for (int z = 0; z < dim; z++) {
                Point key = Point(x, y, z);

                int pri = evalPriority(key, DIR_X);
                if (pri >= 0) {
                    QMap<Point, Node*>::iterator it = _x.insert(key, new Node(pri, key, DIR_X));
                    insertInPriority(*it);
                }

                pri = evalPriority(key, DIR_Y);
                if (pri >= 0) {
                    QMap<Point, Node*>::iterator it = _y.insert(key, new Node(pri, key, DIR_Y));
                    insertInPriority(*it);
                }

                pri = evalPriority(key, DIR_Z);
                if (pri >= 0) {
                    QMap<Point, Node*>::iterator it = _z.insert(key, new Node(pri, key, DIR_Z));
                    insertInPriority(*it);
                }
            }
        }
    }

    scanFromMatrixHook();
}

void Threat::insertHook(Point p)
{
    Q_UNUSED(p)
}

void Threat::scanFromMatrixHook()
{
}

void Threat::evalInsert(Point p, int index, Direction dir)
{
    QMap<Point, Node*> *map = 0;
    QVector<int> coeff(3,0);

    switch (dir) {
        case DIR_X :
            map = &_x;
            coeff[0] = 1;
            break;
        case DIR_Y :
            map = &_y;
            coeff[1] = 1;
            break;
        case DIR_Z :
            map = &_z;
            coeff[2] = 1;
            break;
    }

    int start = coeff[0] * p.x() + coeff[1] * p.y() + coeff[2] * p.z() - index;
    if (start < 0) {
        return;
    }
    Point key(p.x() - coeff[0] * index, p.y() - coeff[1] * index, p.z() - coeff[2] * index);
    QMap<Point, Node*>::iterator it = map->find(key);
    int pri = evalPriority(key, dir);

    if (it == map->end()) {
        if (pri >= 0) {
            it = map->insert(key, new Node(pri, key, dir));
            insertInPriority(*it);
        }
    }
    else {
        removeFromPriority(*it);
        if (pri < 0) {
            delete *it;
            map->erase(it);
        }
        else {
            (*it)->value = pri;
            insertInPriority(*it);
        }
    }
}

int Threat::evalPriority(Point p, Direction dir) const
{
    QVector<int> coeff(3,0);

    switch (dir) {
        case DIR_X :
            coeff[0] = 1;
            break;
        case DIR_Y :
            coeff[1] = 1;
            break;
        case DIR_Z :
            coeff[2] = 1;
            break;
    }

    int start = p.x() * coeff[0] + p.y() * coeff[1] + p.z() * coeff[2];
    if (start < 0 || start + _d1 > _d1*_d2) {
        return -1;
    }

    int pri = 0;
    bool valid = true;

    for (int j = 0; j < _d1 && valid; j++) {
        int piece = _mat->elementAt(p.x() + coeff[0] * j, p.y() + coeff[1] * j, p.z() + coeff[2] * j);
        if (piece == _ai->playerId()) {
            pri++;
        }
        else {
            if (piece != GameMatrix::EmptyPoint) {
                valid = false;
            }
        }
    }

    if (!valid) {
        pri=0;
    }
    return pri-1;
}

void Threat::insertInPriority(Node *it)
{
    int pri = it->value;
    if (pri >= 0 && pri < _d1 - 1) {
        it->next = _priority[pri];
        it->par = &(_priority[pri]);
        if (_priority[pri]) {
            _priority[pri]->par = &(it->next);
        }
        _priority[pri] = it;
    }
}

void Threat::removeFromPriority(Node *it)
{
    *(it->par) = it->next;
    if (it->next) {
        it->next->par = it->par;
    }
    it->par = 0;
    it->next = 0;
}

QList<Point> Threat::findEmptyPoints(Node *node)
{
    QList<Point> result;

    QVector<int> coeff(3,0);

    switch (node->dir) {
        case DIR_X :
            coeff[0] = 1;
            break;
        case DIR_Y :
            coeff[1] = 1;
            break;
        case DIR_Z :
            coeff[2] = 1;
            break;
    }

    for (int i = 0; i < _d1; i++) {
        Point key(node->point.x() + i * coeff[0], node->point.y() + i * coeff[1], node->point.z() + i * coeff[2]);
        if (_mat->elementAt(key) == GameMatrix::EmptyPoint) {
            result.append(key);
        }
    }
    return result;
}

Threat::Node::Node(int val, Point p, Direction dir)
{
    value = val;
    point = p;
    this->dir = dir;
    par = 0;
    next = 0;
}

Threat::Node::~Node()
{
    if (next != 0) {
        delete next;
    }
}
