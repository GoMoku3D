/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "Suggester.h"
#include "GameMatrix.h"
#include "CCThreatSearchAI.h"
#include "ThreatSearchAI.h"

const int Suggester::_SuggestedMoveLevel = 4;

Suggester::Suggester(int id)
{
    if (GameMatrix::instance()->d2() == 3) {
        _ai = new CCThreatSearchAI(id);
    }
    else {
        _ai = new ThreatSearchAI(id);
    }
}

Suggester::~Suggester()
{
    delete _ai;
}

Point Suggester::suggestMove() const
{
    return const_cast<AI*>(_ai)->move(_SuggestedMoveLevel);
}

void Suggester::run()
{
    ThreatSearchAI *aiPointer = dynamic_cast<ThreatSearchAI*>(_ai);
    aiPointer->init();
    Point result = suggestMove();
    emit suggestedMoveReady(result);
}
