/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef SUGGESTER_H
#define SUGGESTER_H

#include <QThread>

#include "AI.h"

class Suggester : public QThread
{
    Q_OBJECT

    public:
        Suggester(int id);
        ~Suggester();
        Point suggestMove() const;

    protected:
        virtual void run();

    private:
        static const int _SuggestedMoveLevel;
        AI *_ai;

    signals:
        void suggestedMoveReady(Point point);
};

#endif
