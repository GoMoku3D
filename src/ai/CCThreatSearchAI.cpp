/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "CCThreatSearchAI.h"
#include "CCThreat.h"


CCThreatSearchAI::CCThreatSearchAI(int id) : ThreatSearchAI(id)
{
}

Threat *CCThreatSearchAI::createThreat()
{
    return new CCThreat(this);
}

bool CCThreatSearchAI::areMatrixBounds(int min, int max) const
{
    return min == 0 && max == _d1 * _d2 - 1;
}

Point CCThreatSearchAI::defenseHook() const
{
    CCThreat *threat = dynamic_cast<CCThreat*>(_threat);
    QList<Point> defense;
    Point e;
    foreach (e, _mat->_lastRound) {
        int oppId = _mat->elementAt(e);

        if (oppId != playerId() && oppId != -2) {
            int index = threat->find(oppId, e);

            QList<Point> adjacentFree;

            Point j;
            foreach (j, threat->_opponentCC[oppId][index].nodes) {

                QList<Point> adjacent = CCThreat::adjacentList(j);

                Point i;
                foreach (i, adjacent) {
                    if (_mat->elementAt(i) == GameMatrix::EmptyPoint) {
                        adjacentFree.append(i);
                    }
                }
            }

            Point k;
            foreach (k, adjacentFree) {
                QSet<Point> simulated;
                simulated.insert(k);

                QList<Point> adjacent = CCThreat::adjacentList(k);

                Point i;
                foreach (i, adjacent) {
                    if (_mat->elementAt(i) == oppId) {
                        int cc = threat->find(oppId, i);
                        if (cc != -1) {
                            simulated.unite(threat->_opponentCC[oppId][cc].nodes);
                        }
                    }
                }

                CCThreat::ConnectedComponent simulatedCC(simulated);
                bool x = areMatrixBounds(simulatedCC.min_x, simulatedCC.max_x);
                bool y = areMatrixBounds(simulatedCC.min_y, simulatedCC.max_y);
                bool z = areMatrixBounds(simulatedCC.min_z, simulatedCC.max_z);

                if (x || y || z) {
                    defense.append(k);
                }
            }
        }
    }

    if (defense.isEmpty()) {
        return Point();
    }

    return defense[qrand() % defense.size()];
}
