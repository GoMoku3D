/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QVector>
#include <QList>

#include "CCThreat.h"

CCThreat::CCThreat(CCThreatSearchAI *ai) : Threat(ai)
{

    _opponentCC = QVector< QList<ConnectedComponent> >(_mat->numberOfPlayers());
}

QList<Point> CCThreat::adjacentList(Point p)
{
    QList<Point> adjacent;
    adjacent.append(Point(p.x() - 1, p.y(), p.z()));
    adjacent.append(Point(p.x() + 1, p.y(), p.z()));
    adjacent.append(Point(p.x(), p.y() - 1, p.z()));
    adjacent.append(Point(p.x(), p.y() + 1, p.z()));
    adjacent.append(Point(p.x(), p.y(), p.z() - 1));
    adjacent.append(Point(p.x(), p.y(), p.z() + 1));

    return adjacent;
}

void CCThreat::insertHook(Point p)
{
    int id = _mat->elementAt(p);
    if (id == _ai->playerId() || find(id, p) != -1) {
        return;
    }

    _opponentCC[id].append(ConnectedComponent(p));

    QList<Point> adjacent = adjacentList(p);

    Point e;
    foreach (e, adjacent) {
        if (_mat->elementAt(e) == id) {
            merge(e, p);
        }
    }
}

void CCThreat::scanFromMatrixHook()
{
    Point e;
    foreach (e, _mat->_lastRound) {
        int oppId = _mat->elementAt(e);
        int matrixSize = _d1 * _d2;
        if (!e.isNull() && oppId != _ai->playerId()) {
            for (int x = 0; x < matrixSize; x++) {
                for (int y = 0; y < matrixSize; y++) {
                    for (int z = 0; z < matrixSize; z++) {
                        Point c(x, y, z);
                        if (_mat->elementAt(c) == oppId && find(oppId,c) == -1) {
                            QSet<Point> set;
                            explore(c, oppId, set);
                            _opponentCC[oppId].append(ConnectedComponent(set));
                        }
                    }
                }
            }
        }
    }
}

int CCThreat::find(int id, Point p) const
{
    QList<ConnectedComponent> list = _opponentCC[id];
    for (int i = 0; i < list.size(); i++) {
        if (list.at(i).nodes.find(p) != list.at(i).nodes.end()) {
            return i;
        }
    }
    return -1;
}

void CCThreat::explore(Point p, int id, QSet<Point> &set)
{
    set.insert(p);

    QList<Point> adjacent = adjacentList(p);

    Point e;

    foreach (e, adjacent) {
        if (id == _mat->elementAt(e) && set.find(e) == set.end()) {
            explore(e, id, set);
        }
    }
}

void CCThreat::merge(Point p1, Point p2)
{
    int id = _mat->elementAt(p1);
    int component1 = find(id, p1);
    int component2 = find(id, p2);

    if (component1 == component2) {
        return;
    }

    ConnectedComponent &cc1 = _opponentCC[id][component1];
    ConnectedComponent &cc2 = _opponentCC[id][component2];

    cc1.nodes.unite(cc2.nodes);

    cc1.min_x = qMin(cc1.min_x, cc2.min_x);
    cc1.min_y = qMin(cc1.min_y, cc2.min_y);
    cc1.min_z = qMin(cc1.min_z, cc2.min_z);
    cc1.max_x = qMax(cc1.max_x, cc2.max_x);
    cc1.max_y = qMax(cc1.max_y, cc2.max_y);
    cc1.max_z = qMax(cc1.max_z, cc2.max_z);

    _opponentCC[id].removeAt(component2);
}

CCThreat::ConnectedComponent::ConnectedComponent(QSet<Point> items)
{
    int matrixSize = GameMatrix::instance()->d1() * GameMatrix::instance()->d2();
    max_x = -1;
    max_y = -1;
    max_z = -1;
    min_x = matrixSize;
    min_y = matrixSize;
    min_z = matrixSize;
    nodes = items;
    Point e;
    foreach (e, items) {
        min_x = qMin(min_x, e.x());
        min_y = qMin(min_y, e.y());
        min_z = qMin(min_z, e.z());
        max_x = qMax(max_x, e.x());
        max_y = qMax(max_y, e.y());
        max_z = qMax(max_z, e.z());
    }
}

CCThreat::ConnectedComponent::ConnectedComponent(Point p)
{
    nodes.insert(p);
    min_x = p.x();
    max_x = p.x();
    min_y = p.y();
    max_y = p.y();
    min_z = p.z();
    max_z = p.z();
}
