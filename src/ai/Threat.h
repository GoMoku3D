/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef THREAT_H
#define THREAT_H

#include <QMap>
#include <QList>

#include "GameMatrix.h"
#include "Point.h"

class ThreatSearchAI;

class Threat
{
    friend class ThreatSearchAI;
    friend class CCThreatSearchAI;
    TEST_FRIEND(ThreatTest)

    private:
        class Node;
    public:
        enum Direction{DIR_X, DIR_Y, DIR_Z};

    public:
        Threat(ThreatSearchAI *ai);
        virtual ~Threat();
        void insert(Point point);
        void scanFromMatrix();

    protected:
        ThreatSearchAI *_ai;
        GameMatrix *_mat;
        int _d1;
        int _d2;
        virtual void insertHook(Point p);
        virtual void scanFromMatrixHook();

    private:
        QMap<Point, Node*> _x;
        QMap<Point, Node*> _y;
        QMap<Point, Node*> _z;
        Node **_priority;
        void evalInsert(Point p, int index, Direction dir);
        int evalPriority(Point p, Direction dir) const;
        void insertInPriority(Node *it);
        void removeFromPriority(Node *it);
        QList<Point> findEmptyPoints(Node *node); //cerca le posizioni vuote e le ritorna in lista


    private:
        class Node
        {
            public:
                int value;
                Point point;
                Threat::Direction dir;
                Node **par;
                Node *next;
                Node(int val, Point p, Direction dir);
                ~Node();
        };
};

#endif
