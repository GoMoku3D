/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QVector>

#include "ThreatSearchAI.h"
#include "Threat.h"
#include "Point.h"

ThreatSearchAI::ThreatSearchAI(int id) : AI(id)
{
    _mat = GameMatrix::instance();
    _d1 = _mat->d1();
    _d2 = _mat->d2();
    _threat = 0;
}

ThreatSearchAI::~ThreatSearchAI()
{
    delete _threat;
}

Point ThreatSearchAI::move(int level)
{
    if (_threat == 0) {
        _threat = createThreat();
    }
    level = qBound(1, level, 4);

    Point e;
    foreach (e, _mat->_lastRound) {
        if (!e.isNull()) {
            _threat->insert(e);
        }
    }

    if (_mat->_lastRound.last().isNull()) {
        return randomPoint();
    }

    if (_threat->_priority[_d1 - 2] != 0) {
        QList<Point> result = _threat->findEmptyPoints(_threat->_priority[_d1 - 2]);
        return result.first();
    }

    Point difesa = defense(level);

    if (!difesa.isNull()) {
        return difesa;
    }

    if (level >= 3) {
        difesa = defenseHook();
        if (!difesa.isNull()) {
            return difesa;
        }
    }

    QList<Point> result;
    int numNodesVisited = 0;
    int i = _d1 - 3;
    Threat::Node *it = 0;
    while (i >= 0 && numNodesVisited <= _mat->numberOfPlayers()) {
        it = _threat->_priority[i];
        while (it != 0) {
            result += _threat->findEmptyPoints(it);
            numNodesVisited++;
            it = it->next;
        }
        i--;
    }

    if (result.isEmpty()) {
        return randomPoint();
    }

    return result[qrand() % result.size()];
}

Point ThreatSearchAI::randomPoint() const
{
    int x = 0;
    int y = 0;
    int z = 0;
    int dim = _mat->d1() * _mat->d2();
    for (int i = 0; i < 10; i++) {
        x = qrand() % dim;
        y = qrand() % dim;
        z = qrand() % dim;
        if(_mat->elementAt(x, y, z) == GameMatrix::EmptyPoint) {
            return Point(x, y, z);
        }
    }

    for (x = 0; x < dim; x++) {
        for (y = 0; y < dim; y++) {
            for ( z = 0; z < dim; z++) {
                if(_mat->elementAt(x, y, z) == GameMatrix::EmptyPoint) {
                    return Point(x,y,z);
                }
            }
        }
    }
    return Point();
}

void ThreatSearchAI::forceMove()
{
}

void ThreatSearchAI::init()
{
    if (_threat != 0) {
        delete _threat;
    }
    _threat = createThreat();
    _threat->scanFromMatrix();
}

void ThreatSearchAI::movesUndone()
{
    init();
}

Point ThreatSearchAI::defenseHook() const
{
    return Point();
}

Threat *ThreatSearchAI::createThreat()
{
    return new Threat(this);
}

Point ThreatSearchAI::defense(int level) const
{
    QList<Point> contromosse;
    Point e;
    foreach (e, _mat->_lastRound) {
        if (_mat->elementAt(e) != playerId()) {
            for (int i = 0; i < _d1; i++) {

                Point p_x(e.x() - i, e.y(), e.z());
                int count = countId(_mat->elementAt(e), p_x, Threat::DIR_X);
                if (count == _d1 - 1) {
                    return freePoints(p_x, Threat::DIR_X).first();
                }
                else {
                    if (count == _d1 - 2) {
                        contromosse += freePoints(p_x, Threat::DIR_X);
                    }
                }

                Point p_y(e.x(), e.y() - i, e.z());
                count = countId(_mat->elementAt(e), p_y, Threat::DIR_Y);
                if (count == _d1 - 1) {
                    return freePoints(p_y, Threat::DIR_Y).first();
                }
                else {
                    if (count == _d1 - 2) {
                        contromosse += freePoints(p_y, Threat::DIR_Y);
                    }
                }

                Point p_z(e.x(), e.y(), e.z() - i);
                count = countId(_mat->elementAt(e), p_z, Threat::DIR_Z);
                if (count == _d1 - 1) {
                    return freePoints(p_z, Threat::DIR_Z).first();
                }
                else {
                    if (count == _d1 - 2) {
                        contromosse += freePoints(p_z, Threat::DIR_Z);
                    }
                }
            }
        }
    }

    if (contromosse.isEmpty() || (qrand() % 4 > level -1)) {
        return Point();
    }

    return contromosse[qrand() % contromosse.size()];
}

int ThreatSearchAI::countId(int id, Point p, Threat::Direction dir) const
{
    QVector<int> coeff(3,0);

    switch (dir) {
        case Threat::DIR_X :
            coeff[0] = 1;
            break;
        case Threat::DIR_Y :
            coeff[1] = 1;
            break;
        case Threat::DIR_Z :
            coeff[2] = 1;
            break;
    }

    int start = p.x() * coeff[0] + p.y() * coeff[1] + p.z() * coeff[2];
    if (start < 0 || start + _d1 > _d1*_d2) {
        return -1;
    }

    int pri = 0;
    bool valid = true;

    for (int j = 0; j < _d1 && valid; j++) {
        int piece = _mat->elementAt(p.x() + coeff[0] * j, p.y() + coeff[1] * j, p.z() + coeff[2] * j);
        if (piece == id) {
            pri++;
        }
        else {
            if (piece != GameMatrix::EmptyPoint) {
                valid = false;
            }
        }
    }

    if (!valid) {
        pri=0;
    }
    return pri;
}

QList<Point> ThreatSearchAI::freePoints(Point p, Threat::Direction dir) const
{
    QList<Point> result;

    QVector<int> coeff(3,0);

    switch (dir) {
        case Threat::DIR_X :
            coeff[0] = 1;
            break;
        case Threat::DIR_Y :
            coeff[1] = 1;
            break;
        case Threat::DIR_Z :
            coeff[2] = 1;
            break;
    }

    for (int i = 0; i < _d1; i++) {
        Point point(p.x() + i * coeff[0], p.y() + i * coeff[1], p.z() + i * coeff[2]);
        if (_mat->elementAt(point) == GameMatrix::EmptyPoint) {
            result.append(point);
        }
    }
    return result;
}
