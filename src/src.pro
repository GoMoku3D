TEMPLATE = app
TARGET = gomoku3d
CONFIG += debug warn_on
QT += network xml

unix:LIBS += -lSoQt
macx:LIBS += -Wl,-framework,SoQt -Wl,-framework,Inventor -Wl,-framework,OpenGL

DEPENDPATH += . ai core gui gui/3d gui/ui network
INCLUDEPATH += . ai core gui gui/3d network
OBJECTS_DIR = ../build
MOC_DIR = .moc
UI_DIR = .ui

HEADERS += Global.h \
           ai/AI.h \
           ai/CCThreat.h \
           ai/CCThreatSearchAI.h \
           ai/Suggester.h \
           ai/Threat.h \
           ai/ThreatSearchAI.h \
           core/AbstractSettings.h \
           core/AIPlayer.h \
           core/GameLoop.h \
           core/GameMatrix.h \
           core/GUISettings.h \
           core/HumanPlayer.h \
           core/LocalSettings.h \
           core/Move.h \
           core/Player.h \
           core/Point.h \
           core/RemotePlayer.h \
           core/ServerSettings.h \
           core/SettingsManager.h \
           core/SyncSharedCondition.h \
           gui/AboutDialog.h \
           gui/ChatWidget.h \
           gui/ColorLabel.h \
           gui/HistoryModel.h \
           gui/MainWindow.h \
           gui/OnlineDialog.h \
           gui/PlayerInfo.h \
           gui/PlayersWidget.h \
           gui/Preferences.h \
           gui/ServerSettingsDialog.h \
           gui/StandAloneDialog.h \
           gui/TimerWidget.h \
           gui/3d/CubeRenderArea.h \
           gui/3d/RenderWidget.h \
           gui/3d/SceneGraph.h \
           network/ClientSocket.h \
           network/Network.h \
           network/GameClient.h \
           network/GameServer.h \
           network/ServerSocket.h \
           network/StreamSocket.h
FORMS += gui/ui/About.ui \
         gui/ui/ChatWidget.ui \
         gui/ui/MainWindow.ui \
         gui/ui/OnlineDialog.ui \
         gui/ui/PlayersWidget.ui \
         gui/ui/Preferences.ui \
         gui/ui/ServerSettingsDialog.ui \
         gui/ui/StandAloneDialog.ui
SOURCES += main.cpp \
           ai/AI.cpp \
           ai/CCThreat.cpp \
           ai/CCThreatSearchAI.cpp \
           ai/Suggester.cpp \
           ai/Threat.cpp \
           ai/ThreatSearchAI.cpp \
           core/AIPlayer.cpp \
           core/GameLoop.cpp \
           core/GameMatrix.cpp \
           core/GUISettings.cpp \
           core/HumanPlayer.cpp \
           core/LocalSettings.cpp \
           core/Move.cpp \
           core/Player.cpp \
           core/Point.cpp \
           core/RemotePlayer.cpp \
           core/ServerSettings.cpp \
           core/SyncSharedCondition.cpp \
           gui/AboutDialog.cpp \
           gui/ChatWidget.cpp \
           gui/ColorLabel.cpp \
           gui/HistoryModel.cpp \
           gui/MainWindow.cpp \
           gui/OnlineDialog.cpp \
           gui/PlayerInfo.cpp \
           gui/PlayersWidget.cpp \
           gui/Preferences.cpp \
           gui/ServerSettingsDialog.cpp \
           gui/StandAloneDialog.cpp \
           gui/TimerWidget.cpp \
           gui/3d/CubeRenderArea.cpp \
           gui/3d/RenderWidget.cpp \
           gui/3d/SceneGraph.cpp \
           network/ClientSocket.cpp \
           network/GameClient.cpp \
           network/GameServer.cpp \
           network/ServerSocket.cpp \
           network/StreamSocket.cpp
TRANSLATIONS += translations/gomoku3d_en.ts \
                translations/gomoku3d_it.ts
