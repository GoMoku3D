/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia and Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef GLOBAL_H
#define GLOBAL_H

#ifdef UNIT_TESTS
#define TEST_FRIEND(c) friend class c;
#else
#define TEST_FRIEND(c)
#endif

#define GREEN_TEXT(txt) \
    QString("<html><head><meta name=\"qrichtext\" content=\"1\"/></head><body><p><span style=\"font-size:11pt; color:#008b00;\">") \
            + txt + "</span></p></body></html>"
#define YELLOW_TEXT(txt) \
    QString("<html><head><meta name=\"qrichtext\" content=\"1\"/></head><body><p><span style=\"font-size:11pt; color:#ebab00;\">") \
            + txt + "</span></p></body></html>"
#define RED_TEXT(txt) \
    QString("<html><head><meta name=\"qrichtext\" content=\"1\"/></head><body><p><span style=\"font-size:11pt; color:#8b0000;\">") \
            + txt + "</span></p></body></html>"

#endif
