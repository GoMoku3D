/********************************************************************
 *
 * Copyright (C) 2008  Tobia Zorzan
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef RENDERWIDGET_H
#define RENDERWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QColor>
#include <QList>
#include <QVBoxLayout>

#include "CubeRenderArea.h"
#include "GUISettings.h"

class RenderWidget : public QWidget
{
    Q_OBJECT

    public:
        RenderWidget(QWidget *parent, int dim = 0);
        virtual ~RenderWidget();
		void init(int dim);
		void reset();
        void acceptMove(bool value);
        void drawMove(Point p, QColor c);
        void uncheckedDraw(QList<Point> pointsList, QList<QColor> colorsList);
		void setAntialiasing(int value);
        void updateBackground();
        CubeRenderArea *renderarea();

    public slots:
        void highlightPoint(Point p);

    private:
        CubeRenderArea *_renderarea;
        QVBoxLayout *_layout;
		QLabel *_imageLabel;
		QPixmap _image;
        QColor _defaultCubeColor;
};

#endif
