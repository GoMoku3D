/********************************************************************
 *
 * Copyright (C) 2008  Tobia Zorzan
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/
 
#ifndef SCENEGRAPH_H
#define SCENEGRAPH_H

#include "RenderWidget.h"
#include "CubeRenderArea.h"
#include "GUISettings.h"

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoGroup.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/engines/SoInterpolateVec3f.h>
#include <Inventor/engines/SoOneShot.h>
#include <Inventor/sensors/SoOneShotSensor.h>

#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoMaterial.h>

#include <Inventor/misc/SoChildList.h>


class CubeRenderArea;

enum CubeRenderArea::ORIENTATION;

class SceneGraph 
{
	private:
		CubeRenderArea *_renderarea;
		int _dim;
		SoSeparator *_root;
		SoCamera *_camera;
		SoLight *_light;
		SoSelection *_selector;
		SoGroup *_fixedCubes;
		SoBaseList *_movedCubes;
		SoShape *_shape;
		SoBaseList *_movers;
		SoBaseList *_interpolators;
		SoBaseList *_timers;
		SoOneShotSensor *_explodeSensor;
		SoOneShotSensor *_implodeSensor;
		SoSeparator **_graphMatrix; 
		SoSeparator *_selectedCube;
		CubeRenderArea::ORIENTATION _directionOfExplosion;
		CubeRenderArea::ORIENTATION _desiredDirectionOfExplosion;
		bool _allImplosion;
		int _explodedLevels;
		
		void explodeLevel();
		void implodeLevel();
		
	public:
		SceneGraph(CubeRenderArea *render, int dim);
		~SceneGraph();
		void applyRotationToCamera(SbRotation rotation);
		void applyTranslationToCamera(int delta);
		static void explodeCube(void *userdata, SoSensor *sensor);
		static void implodeCube(void *userdata, SoSensor *sensor);
		void applyExplosionToCube(CubeRenderArea::ORIENTATION desired);
		void applyImplosionToCube(bool all);
	
		CubeRenderArea *renderarea();
		SoSeparator *root();
		SoCamera *camera();
		SoSelection *selector();
		SoGroup *fixedCubes();
		SoBaseList *movedCubes();
		SoShape *shape();
		SoBaseList *movers();
		SoBaseList *interpolators();
		SoBaseList *timers();
		SoOneShotSensor *explodeSensor();
		SoOneShotSensor *implodeSensor();
		SoSeparator **graphMatrix();
		SoSeparator *selectedCube();
		CubeRenderArea::ORIENTATION directionOfExplosion();
		CubeRenderArea::ORIENTATION desiredDirectionOfExplosion();
		bool allImplosion();
		int explodedLevels();
		
		void setSelectedCube(SoSeparator *cube);
		void setDirectionOfExplosion(CubeRenderArea::ORIENTATION direction);
		void setDesiredDirectionOfExplosion(CubeRenderArea::ORIENTATION direction);
		void setAllImplosion(bool value);
		void setExplodedLevels(int num);
		
};

#endif
