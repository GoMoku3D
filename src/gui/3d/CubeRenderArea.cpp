/********************************************************************
 *
 * Copyright (C) 2008  Tobia Zorzan
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "CubeRenderArea.h"

/*!
 * @class CubeRenderArea CubeRenderArea.h
 * @brief CubeRenderArea esegue il rendering della scena 3D.
 *
 * CubeRenderArea è una classe derivata da \c SoQtRenderArea fornita dalle librerie SoQt.
 * E' la classe che effettivamente gestisce e renderizza la scena 3D. Usa un oggetto di
 * di tipo \ref SceneGraph per salvare e gestire i nodi della scena.
 */

/*!
 * Costruttore della classe.
 * @param parent Puntatore al widget genitore.
 * @param dim La dimensione della matrice di gioco.
 */

CubeRenderArea::CubeRenderArea(QWidget *parent, int dim) : SoQtRenderArea(parent),
        _acceptMove(false),
        _numCubes(dim),
        _isExploded(false),
        _isRotating(false),
        _hasPreviousPick(false),
        _lastXPick(0),
        _lastYPick(0),
        _mutex(),
        _frontside(CubeRenderArea::Z_MINUS),
        _upside(CubeRenderArea::Y_PLUS)
{
    _scene = new SceneGraph(this, dim);

    // projector is a sphere projector, with a sphere centered in (0, 0, 0) and radius equal to 0.6
    _projector = new SbSphereSheetProjector(SbSphere(SbVec3f(0, 0, 0), 0.6f));

    // draws a wireframe around selected objects
    _highlightAction = new SoLineHighlightRenderAction;

    GUISettings settings;
    SbColor background;
    (convertColor(settings.backgroundColor())).getRGB(background);

    this->setBackgroundColor(background);
    this->setGLRenderAction(_highlightAction);
    this->setTransparencyType(SoGLRenderAction::DELAYED_BLEND);
    this->setDoubleBuffer(true);

    this->setSceneGraph(_scene->root());
    _scene->camera()->viewAll(_scene->root(), this->getViewportRegion());
    _scene->camera()->nearDistance.setValue(0.01f);

    SbViewVolume volume;
    volume.ortho(-1, 1, -1, 1, -1, 1);
    _projector->setViewVolume(volume);

    this->setEventCallback(eventDispatcher, this);
    _scene->selector()->addSelectionCallback(cubeSelected, this);
    _scene->selector()->addDeselectionCallback(cubeDeselected, this);
}

/*!
 * Distruttore della classe.
 * Dealloca gli oggetti puntati dai campi dato, nel caso l'interfaccia sia in
 * attesa di una mossa dall'utente, viene inviata una mossa nulla.
 */
CubeRenderArea::~CubeRenderArea()
{
    delete _scene;
    delete _projector;
    delete _highlightAction;
}

/*!
 * Disegna una mossa.
 * @param p Posizione della mossa da rappresentare.
 * @param c Colore della mossa. Il colore è nella forma \c RGBA e incapsulato in
 * un oggetto di tipo \ref SbColor4f.
 */
void CubeRenderArea::drawMove(Point p, SbColor4f c)
{
    int index = p.x() * _numCubes * _numCubes + p.y() * _numCubes + p.z();
    SoMaterial *cubeMaterial = (SoMaterial *)(_scene->graphMatrix()[index])->getChild(1);
    SbColor plaincolor;
    c.getRGB(plaincolor);
    cubeMaterial->diffuseColor.setValue(plaincolor);

    cubeMaterial->transparency.setValue(c[3]);
    qDebug() << "Draw: " << p.x() << " " << p.y() << " " << p.z() << " with color: " << c[0] << " " << c[1] << " " << c[2];
}

/*!
 * Esegue una mossa.
 * Metodo invocato dal gestore degli eventi. Invia una mossa contente la posizione
 * attuale del cursore.
 */
void CubeRenderArea::doMove()
{
    if (_scene->selectedCube() && acceptMove()) {
        //qDebug() << "Executing CubeRenderArea::doMove().";
        SoTranslation *cubeTranslation = (SoTranslation *) _scene->selectedCube()->getChild(0);
        Point current = Point(int(cubeTranslation->translation.getValue()[0] / 3), int(cubeTranslation->translation.getValue()[1] / 3), int(cubeTranslation->translation.getValue()[2] / 3));
        if (GameMatrix::instance()->check(current)) {
            setAcceptMove(false);

            SyncSharedCondition::instance()->lock();
            SyncSharedCondition::instance()->notifyMove(current);
            SyncSharedCondition::instance()->unlock();

            qDebug() << "User sent a valid move.";
        }
    }
    return;
}

/*!
 * Calcola l'asse data una direzione.
 * Data l'attuale posizione dell'oservatore, calcola l'asse in tre dimensioni che
 * corrisponde alla direzione del paramtero.
 * @param direction Di tipo \ref CubeRenderArea::ORIENTATION , rappresenta una direzione.
 */
CubeRenderArea::ORIENTATION CubeRenderArea::getOrientation(CubeRenderArea::DIRECTION direction)
{
    ORIENTATION result = CubeRenderArea::NO_ORIENTATION;
    switch (direction) {
    case UP : {
        result = _upside;
        break;
    }
    case DOWN : {
        switch (_upside) {
        case X_PLUS : {
            result = X_MINUS;
            break;
        }
        case X_MINUS : {
            result = X_PLUS;
            break;
        }
        case Y_PLUS : {
            result = Y_MINUS;
            break;
        }
        case Y_MINUS : {
            result = Y_PLUS;
            break;
        }
        case Z_PLUS : {
            result = Z_MINUS;
            break;
        }
        case Z_MINUS : {
            result = Z_PLUS;
            break;
        }
        case NO_ORIENTATION : {
            result = NO_ORIENTATION;
            break;
        }
        }
        break;
    }
    case LEFT : {
        switch (_frontside) {
        case X_PLUS : {
            switch (_upside) {
            case Y_PLUS : {
                result = Z_MINUS;
                break;
            }
            case Z_PLUS : {
                result = Y_PLUS;
                break;
            }
            case Y_MINUS : {
                result = Z_PLUS;
                break;
            }
            case Z_MINUS : {
                result = Y_MINUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case X_MINUS : {
            switch (_upside) {
            case Y_PLUS : {
                result = Z_PLUS;
                break;
            }
            case Z_MINUS : {
                result = Y_PLUS;
                break;
            }
            case Y_MINUS : {
                result = Z_MINUS;
                break;
            }
            case Z_PLUS : {
                result = Y_MINUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case Y_PLUS : {
            switch (_upside) {
            case Z_PLUS : {
                result = X_MINUS;
                break;
            }
            case X_PLUS : {
                result = Z_PLUS;
                break;
            }
            case Z_MINUS : {
                result = X_PLUS;
                break;
            }
            case X_MINUS : {
                result = Z_MINUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case Y_MINUS : {
            switch (_upside) {
            case Z_MINUS : {
                result = X_MINUS;
                break;
            }
            case X_PLUS : {
                result = Z_MINUS;
                break;
            }
            case Z_PLUS : {
                result = X_PLUS;
                break;
            }
            case X_MINUS : {
                result = Z_PLUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case Z_PLUS : {
            switch (_upside) {
            case Y_PLUS : {
                result = X_PLUS;
                break;
            }
            case X_MINUS : {
                result = Y_PLUS;
                break;
            }
            case Y_MINUS : {
                result = X_MINUS;
                break;
            }
            case X_PLUS : {
                result = Y_MINUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case Z_MINUS : {
            switch (_upside) {
            case Y_PLUS : {
                result = X_MINUS;
                break;
            }
            case X_PLUS : {
                result = Y_PLUS;
                break;
            }
            case Y_MINUS : {
                result = X_PLUS;
                break;
            }
            case X_MINUS : {
                result = Y_MINUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case NO_ORIENTATION : {
            result = NO_ORIENTATION;
            break;
        }
        }
        break;
    }
    case RIGHT : {
        switch (_frontside) {
        case X_PLUS : {
            switch (_upside) {
            case Y_PLUS : {
                result = Z_PLUS;
                break;
            }
            case Z_PLUS : {
                result = Y_MINUS;
                break;
            }
            case Y_MINUS : {
                result = Z_MINUS;
                break;
            }
            case Z_MINUS : {
                result = Y_PLUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case X_MINUS : {
            switch (_upside) {
            case Y_PLUS : {
                result = Z_MINUS;
                break;
            }
            case Z_MINUS : {
                result = Y_MINUS;
                break;
            }
            case Y_MINUS : {
                result = Z_PLUS;
                break;
            }
            case Z_PLUS : {
                result = Y_PLUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case Y_PLUS : {
            switch (_upside) {
            case Z_PLUS : {
                result = X_PLUS;
                break;
            }
            case X_PLUS : {
                result = Z_MINUS;
                break;
            }
            case Z_MINUS : {
                result = X_MINUS;
                break;
            }
            case X_MINUS : {
                result = Z_PLUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case Y_MINUS : {
            switch (_upside) {
            case Z_MINUS : {
                result = X_PLUS;
                break;
            }
            case X_PLUS : {
                result = Z_PLUS;
                break;
            }
            case Z_PLUS : {
                result = X_MINUS;
                break;
            }
            case X_MINUS : {
                result = Z_MINUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case Z_PLUS : {
            switch (_upside) {
            case Y_PLUS : {
                result = X_MINUS;
                break;
            }
            case X_MINUS : {
                result = Y_MINUS;
                break;
            }
            case Y_MINUS : {
                result = X_PLUS;
                break;
            }
            case X_PLUS : {
                result = Y_PLUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case Z_MINUS : {
            switch (_upside) {
            case Y_PLUS : {
                result = X_PLUS;
                break;
            }
            case X_PLUS : {
                result = Y_MINUS;
                break;
            }
            case Y_MINUS : {
                result = X_MINUS;
                break;
            }
            case X_MINUS : {
                result = Y_PLUS;
                break;
            }
            default : {
                break;
            }
            }
            break;
        }
        case NO_ORIENTATION : {
            result = NO_ORIENTATION;
            break;
        }
        }
        break;
    }
    case IN : {
        result = _frontside;
        break;
    }
    case OUT : {
        switch (_frontside) {
        case X_PLUS : {
            result = X_MINUS;
            break;
        }
        case X_MINUS : {
            result = X_PLUS;
            break;
        }
        case Y_PLUS : {
            result = Y_MINUS;
            break;
        }
        case Y_MINUS : {
            result = Y_PLUS;
            break;
        }
        case Z_PLUS : {
            result = Z_MINUS;
            break;
        }
        case Z_MINUS : {
            result = Z_PLUS;
            break;
        }
        case NO_ORIENTATION : {
            result = NO_ORIENTATION;
            break;
        }
        }
        break;
    }
    }
    //qDebug() << "Frontside: " << _frontside << "     Upside: " << _upside << ".";
    //qDebug() << "Getting orientation of: " << direction << "... is: " << result << ".";
    return result;
}

Point CubeRenderArea::getAdiacentPosition(Point oldpoint, ORIENTATION orientation)
{
    int x = 0;
    int y = 0;
    int z = 0;
    switch (orientation) {
    case X_PLUS : {
        x++;
        break;
    }
    case X_MINUS : {
        x--;
        break;
    }
    case Y_PLUS : {
        y++;
        break;
    }
    case Y_MINUS : {
        y--;
        break;
    }
    case Z_PLUS : {
        z++;
        break;
    }
    case Z_MINUS : {
        z--;
        break;
    }
    default : {
        break;
    }
    }
    Point newpoint = Point(oldpoint.x() + x, oldpoint.y() + y, oldpoint.z() + z);
    if (newpoint.isValid()) {
        return newpoint;
    } else {
        return oldpoint;
    }
}


void CubeRenderArea::selectCube(Point p, SbColor c)
{
    //qDebug() << "Method: selectCube().";
    int index = p.x() * _numCubes * _numCubes + p.y() * _numCubes + p.z();
    if (p.isValid()) {
        _highlightAction->setColor(c);
        _scene->selector()->deselectAll();

        _scene->selector()->removeSelectionCallback(cubeSelected, this);
        _scene->selector()->select(_scene->graphMatrix()[index]);
        _scene->selector()->addSelectionCallback(cubeSelected, this);

        _scene->graphMatrix()[index]->touch();
        _scene->setSelectedCube(_scene->graphMatrix()[index]);
        qDebug() << "Selected cube at:" << p.toString();

        if (this->isExploded()) {
            //Call Explosion function to update te cube view
            _scene->setDesiredDirectionOfExplosion(_scene->directionOfExplosion());
            _scene->explodeSensor()->schedule();
            _scene->setAllImplosion(false);
            _scene->implodeSensor()->schedule();
        }
    }
}

void CubeRenderArea::rotateCamera(int newX, int newY)
{
    if (this->hasPreviousPick()) {

        //qDebug() << "Mouse move from: " << this->_lastXPick << "," << this->_lastYPick << " to: " << newX << "," << newY << ".";

        SbVec2s glsize = this->getViewportRegion().getViewportSizePixels();

        //qDebug() << "Viewport size: " << glsize[0] << "," << glsize[1] << ".";

        SbVec2f lastpos;
        lastpos[0] = this->_lastXPick / float(SoQtMax((int)(glsize[0] - 1), 1));
        lastpos[1] = 1 - (this->_lastYPick / float(SoQtMax((int)(glsize[1] - 1), 1)));

        SbVec2f newpos;
        newpos[0] = newX / float(SoQtMax((int)(glsize[0] - 1), 1));
        newpos[1] = 1 - (newY / float(SoQtMax((int)(glsize[1] - 1), 1)));

        SbVec3f startpoint = this->_projector->project(lastpos);
        SbRotation rot;
        SbVec3f endpoint = this->_projector->projectAndGetRotation(newpos, rot);
        rot.invert();

        //qDebug() << "Moving from: " << startpoint[0] << "," << startpoint[1] << "," << startpoint[2] << " to: " << endpoint[0] << "," << endpoint[1] << "," << endpoint[2] << ".";

        _scene->applyRotationToCamera(rot);
    }
    this->setHasPreviousPick(true);
    this->_lastXPick = newX;
    this->_lastYPick = newY;
}

void CubeRenderArea::zoomCamera(int delta)
{
    _scene->applyTranslationToCamera(delta);
}

SbColor4f CubeRenderArea::convertColor(QColor qtcolor)
{
    SbColor4f coincolor = SbColor4f(qtcolor.redF(), qtcolor.greenF(), qtcolor.blueF(), 1 - qtcolor.alphaF());
    return coincolor;
}

SbBool CubeRenderArea::eventDispatcher(void *userdata, QEvent *event)
{
    //qDebug() << "Event dispatcher: type->"<<event->type();
    CubeRenderArea *renderarea = (CubeRenderArea *) userdata;
    SbBool handled = false;

    switch (event->type()) {
    case QEvent::MouseButtonPress : {
        //qDebug() << "Event dispatcher: mouse button press.";
        renderarea->setIsRotating(true);
        break;
    }
    case QEvent::MouseButtonRelease : {
        //qDebug() << "Event dispatcher: mouse button release.";
        renderarea->setHasPreviousPick(false);
        renderarea->setIsRotating(false);
        break;
    }
    case QEvent::MouseMove : {
        //qDebug() << "Event dispatcher: mouse move.";
        QMouseEvent *mouseevent = (QMouseEvent *) event;
        if (renderarea->isRotating() == true) {
            //needs to handle mouse dragging
            renderarea->rotateCamera(mouseevent->x(), mouseevent->y());
        }
        handled = true;
        break;
    }
    case QEvent::Wheel : {
        QWheelEvent *wheelevent = (QWheelEvent *) event;
        renderarea->zoomCamera(wheelevent->delta() / 120);
        handled = true;
        break;
    }
    case QEvent::KeyPress: {
        QKeyEvent *keyevent = (QKeyEvent *) event;
        if (renderarea->scene()->selectedCube()) {
            ORIENTATION keyOrientation = NO_ORIENTATION;
            SoTranslation *cubeTranslation = (SoTranslation *) renderarea->scene()->selectedCube()->getChild(0);
            Point oldpoint = Point(int(cubeTranslation->translation.getValue()[0] / 3), int(cubeTranslation->translation.getValue()[1] / 3), int(cubeTranslation->translation.getValue()[2] / 3));
            Point newpoint;
            switch (keyevent->key()) {
                //Directional keys
            case Qt::Key_Up : {
                keyOrientation = renderarea->getOrientation(UP);
                break;
            }
            case Qt::Key_Down : {
                keyOrientation = renderarea->getOrientation(DOWN);
                break;
            }
            case Qt::Key_Left : {
                keyOrientation = renderarea->getOrientation(LEFT);
                break;
            }
            case Qt::Key_Right : {
                keyOrientation = renderarea->getOrientation(RIGHT);
                break;
            }
            case Qt::Key_W : {
                keyOrientation = renderarea->getOrientation(IN);
                break;
            }
            case Qt::Key_S : {
                keyOrientation = renderarea->getOrientation(OUT);
                break;
            }
            }
            if (keyOrientation != NO_ORIENTATION) {
                newpoint = renderarea->getAdiacentPosition(oldpoint, keyOrientation);
                renderarea->selectCube(newpoint);
            }
        }
        switch (keyevent->key()) {
            //Special keys
        case Qt::Key_Space: {
            //qDebug() << "Space Pressed.";
            if (renderarea->isExploded()) {
                renderarea->scene()->setAllImplosion(true);
                renderarea->scene()->implodeSensor()->schedule();
            } else {
                if (renderarea->scene()->selectedCube()) {
                    qDebug() << "New Explosion";
                    renderarea->scene()->setDesiredDirectionOfExplosion(renderarea->frontside());
                    renderarea->scene()->explodeSensor()->schedule();
                }
            }
            break;
        }
        case Qt::Key_Return : {
            //qDebug() << "Enter Pressed.";
            renderarea->doMove();
            break;
        }
        }
        handled = true;
        break;
    }

    default : {
        break;
    }
    }

    return handled;
}

void CubeRenderArea::cubeSelected(void *userdata, SoPath *path)
{
    CubeRenderArea *renderarea = (CubeRenderArea*) userdata;

    SoSeparator *cubeInstance = (SoSeparator *) path->getNode(2);
    SoTranslation *cubeTranslation = (SoTranslation*) cubeInstance->getChild(0);

    Point point(int(cubeTranslation->translation.getValue()[0] / 3), int(cubeTranslation->translation.getValue()[1] / 3), int(cubeTranslation->translation.getValue()[2] / 3));

    //qDebug() << "Selected cube callback.";
    renderarea->selectCube(point);
}

void CubeRenderArea::cubeDeselected(void *userdata, SoPath *path)
{
    CubeRenderArea *renderarea = (CubeRenderArea*) userdata;
    renderarea->scene()->setSelectedCube(NULL);

    path->getHead()->touch();

    //qDebug() << "Deselected cube.";

    return;
}

//Get methods
SceneGraph *CubeRenderArea::scene()
{
    return _scene;
}
bool CubeRenderArea::acceptMove()
{
    bool tmp;
    _mutex.lock();
    tmp = _acceptMove;
    _mutex.unlock();
    return tmp;
}
int CubeRenderArea::numCubes()
{
    return _numCubes;
}
bool CubeRenderArea::isExploded()
{
    return _isExploded;
}
bool CubeRenderArea::isRotating()
{
    return _isRotating;
}
bool CubeRenderArea::hasPreviousPick()
{
    return _hasPreviousPick;
}
QMutex &CubeRenderArea::mutex()
{
    return _mutex;
}
CubeRenderArea::ORIENTATION CubeRenderArea::frontside()
{
    return _frontside;
}
CubeRenderArea::ORIENTATION CubeRenderArea::upside()
{
    return _upside;
}

//Set methods
void CubeRenderArea::setAcceptMove(bool value)
{
    _mutex.lock();
    _acceptMove = value;
    //qDebug() << "Setting CubeRenderArea::_acceptMove to:" << _acceptMove << ".";
    _mutex.unlock();
    return;
}
void CubeRenderArea::setIsExploded(bool value)
{
    _isExploded = value;
    return;
}
void CubeRenderArea::setIsRotating(bool value)
{
    _isRotating = value;
    return;
}
void CubeRenderArea::setHasPreviousPick(bool value)
{
    _hasPreviousPick = value;
    return;
}
void CubeRenderArea::setFrontside(CubeRenderArea::ORIENTATION value)
{
    _frontside = value;
    return;
}
void CubeRenderArea::setUpside(CubeRenderArea::ORIENTATION value)
{
    _upside = value;
    return;
}
