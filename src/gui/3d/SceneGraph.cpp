/********************************************************************
 *
 * Copyright (C) 2008  Tobia Zorzan
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "SceneGraph.h"

SceneGraph::SceneGraph(CubeRenderArea *render, int dim) : _renderarea(render),
        _dim(dim),
        _selectedCube(0),
        _directionOfExplosion(CubeRenderArea::NO_ORIENTATION),
        _desiredDirectionOfExplosion(CubeRenderArea::NO_ORIENTATION),
        _allImplosion(false),
        _explodedLevels(0)
{
    //allocating all new nodes for the scene graph

    //root of the graph, must be referenced manually, all its childs don't need it
    _root = new SoSeparator();
    _root->ref();
    //new camera
    _camera = new SoPerspectiveCamera();
    //new light
    _light = new SoDirectionalLight();
    //container for all cubes, make them clickable
    _selector = new SoSelection();
    //group for the fixed part of the cube
    _fixedCubes = new SoGroup();

    //List of SoSeparator *, all cubes moved by the explosion
    _movedCubes = new SoBaseList(dim - 1);
    //List of Sotranslation *, used for animation
    _movers = new SoBaseList(dim - 1);
    //List of SoInterpolateVec3f, used for animation
    _interpolators = new SoBaseList(dim - 1);
    //List of SoOneShot *, used for animation
    _timers = new SoBaseList(dim - 1);

    //shape shared by all the cubes
    _shape = new SoCube();
    //trigger to invoke explosion method
    _explodeSensor = new SoOneShotSensor();
    //trigger to invoke implosion method
    _implodeSensor = new SoOneShotSensor();
    //matrix to have a direct acces to cubes by indexes
    _graphMatrix = new SoSeparator*[dim * dim * dim];

    _explodeSensor->setFunction(SceneGraph::explodeCube);
    _explodeSensor->setData(this);

    _implodeSensor->setFunction(SceneGraph::implodeCube);
    _implodeSensor->setData(this);

    _selector->policy = SoSelection::SINGLE;
    _selector->renderCaching = SoSeparator::ON;
    _selector->boundingBoxCaching = SoSeparator::ON;
    _selector->renderCulling = SoSeparator::ON;
    _selector->pickCulling = SoSeparator::ON;

    _root->addChild(_light);
    _root->addChild(_camera);
    _root->addChild(_selector);
    _selector->addChild(_fixedCubes);

    SoShapeHints *hints = new SoShapeHints();
    hints->vertexOrdering = SoShapeHints::COUNTERCLOCKWISE;
    hints->shapeType = SoShapeHints::SOLID;

    GUISettings settings;
    QColor defaultColor = settings.defaultCubeColor();
    //qDebug() << "Default cube color is:" << defaultColor << ".";
    SbColor coincolor;
    CubeRenderArea::convertColor(defaultColor).getRGB(coincolor);

    for (int x = 0; x < dim; x++) {
        for (int y = 0; y < dim; y++) {
            for (int z = 0; z < dim; z++) {
                SoSeparator *cubeInstance = new SoSeparator();

                SoTranslation *cubeTranslation = new SoTranslation();
                cubeTranslation->translation = SbVec3f(x * 3.0, y * 3.0, z * 3.0);

                SoMaterial *cubeMaterial = new SoMaterial();


                cubeMaterial->diffuseColor.setValue(coincolor);
                cubeMaterial->shininess.setValue(0.2);
                cubeMaterial->transparency.setValue(defaultColor.alphaF());

                cubeInstance->addChild(cubeTranslation);
                cubeInstance->addChild(cubeMaterial);
                cubeInstance->addChild(hints);
                cubeInstance->addChild(_shape);

                _fixedCubes->addChild(cubeInstance);

                _graphMatrix[x * dim * dim + y * dim + z] = cubeInstance;
            }
        }
    }
}

SceneGraph::~SceneGraph()
{
    delete _explodeSensor;
    delete _implodeSensor;

    // Destructor for the scene graph
    _root->unref();

    delete[] _graphMatrix;

    delete _movedCubes;
    delete _movers;
    delete _timers;
    delete _interpolators;
}

void SceneGraph::applyRotationToCamera(SbRotation rotation)
{
    // Find global coordinates of focal point.
    SbVec3f direction;
    _camera->orientation.getValue().multVec(SbVec3f(0, 0, -1), direction);
    SbVec3f focalpoint = _camera->position.getValue() + _camera->focalDistance.getValue() * direction;

    // Set new orientation value by accumulating the new rotation.
    _camera->orientation = rotation * _camera->orientation.getValue();

    // Reposition camera so we are still pointing at the same old focal point.
    _camera->orientation.getValue().multVec(SbVec3f(0, 0, -1), direction);
    _camera->position = focalpoint - _camera->focalDistance.getValue() * direction;

    //update CubeRenderArea::frontside/upside

    int max = 0;
    for (int i = 0; i < 3; i++) {
        if (SoQtAbs(direction[i]) > SoQtAbs(direction[max])) {
            max = i;
        }
    }
    switch (max) {
    case 0 : {
        if (direction[max] < 0) {
            _renderarea->setFrontside(CubeRenderArea::X_MINUS);
        } else {
            _renderarea->setFrontside(CubeRenderArea::X_PLUS);
        }
        break;
    }
    case 1 : {
        if (direction[max] < 0) {
            _renderarea->setFrontside(CubeRenderArea::Y_MINUS);
        } else {
            _renderarea->setFrontside(CubeRenderArea::Y_PLUS);
        }
        break;
    }
    case 2 : {
        if (direction[max] < 0) {
            _renderarea->setFrontside(CubeRenderArea::Z_MINUS);
        } else {
            _renderarea->setFrontside(CubeRenderArea::Z_PLUS);
        }
        break;
    }
    }


    SbVec3f upside;
    _camera->orientation.getValue().multVec(SbVec3f(0, 1, 0), upside);
    int maxu = 0;
    for (int i = 0; i < 3; i++) {
        if (SoQtAbs(upside[i]) > SoQtAbs(upside[maxu])) {
            maxu = i;
        }
    }
    switch (maxu) {
    case 0: {
        if (upside[maxu] < 0) {
            _renderarea->setUpside(CubeRenderArea::X_MINUS);
        } else {
            _renderarea->setUpside(CubeRenderArea::X_PLUS);
        }
        break;
    }
    case 1: {
        if (upside[maxu] < 0) {
            _renderarea->setUpside(CubeRenderArea::Y_MINUS);
        } else {
            _renderarea->setUpside(CubeRenderArea::Y_PLUS);
        }
        break;
    }
    case 2: {
        if (upside[maxu] < 0) {
            _renderarea->setUpside(CubeRenderArea::Z_MINUS);
        } else {
            _renderarea->setUpside(CubeRenderArea::Z_PLUS);
        }
        break;
    }
    }
    qDebug() << "Rotation:" << _renderarea->frontside() << " " << _renderarea->upside();
    return;
}
void SceneGraph::applyTranslationToCamera(int delta)
{
    SbVec3f direction;
    _camera->orientation.getValue().multVec(SbVec3f(0, 0, -1), direction);
    SbVec3f oldpos = _camera->position.getValue();
    SbVec3f newpos = oldpos + (delta) * - direction;

    _camera->position = newpos;
    _camera->focalDistance.setValue(_camera->focalDistance.getValue() + delta);
    _camera->farDistance.setValue(_camera->farDistance.getValue() + delta);

    return;
}

void SceneGraph::explodeCube(void *userdata, SoSensor *sensor)
{
    //qDebug() << "Explosion sensor callback.";
    Q_UNUSED(sensor);
    SceneGraph *scene = (SceneGraph *) userdata;
    scene->applyExplosionToCube(scene->desiredDirectionOfExplosion());
    return;
}

void SceneGraph::implodeCube(void *userdata, SoSensor *sensor)
{
    //qDebug() << "Implosion sensor callback.";
    Q_UNUSED(sensor);
    SceneGraph *scene = (SceneGraph *) userdata;
    scene->applyImplosionToCube(scene->allImplosion());
    return;
}

void SceneGraph::applyExplosionToCube(CubeRenderArea::ORIENTATION desired)
{
    //qDebug() << "Apply EXPLOSION to cube.\nDesired direction : " << desired << ".\nCurrent direction : " << _directionOfExplosion << ".";
    Q_ASSERT(_selectedCube);
    SoTranslation *cursorPosition = static_cast<SoTranslation *>(_selectedCube->getChild(0));

    if (desired != _directionOfExplosion) {
        //clean lists and graph
        //qDebug() << "New direction of explosion. Cleaning scene graph....done!";
        _directionOfExplosion = CubeRenderArea::NO_ORIENTATION;
        _explodedLevels = 0;

        //qDebug() << "Lists size: " << _movedCubes->getLength() <<".";

        for (int i = 0; i < _movedCubes->getLength(); i++) {
            SoSeparator *oldGroup = static_cast<SoSeparator *>(_movedCubes->get(i));
            SoChildList *cubeList = oldGroup->getChildren();
            for (int j = 1; j < cubeList->getLength(); j++) {
                _fixedCubes->addChild(static_cast<SoSeparator *>(cubeList->get(j)));
            }
            oldGroup->removeAllChildren();
            _selector->removeChild(oldGroup);
        }

        _movedCubes->truncate(0);
        _movers->truncate(0);
        _interpolators->truncate(0);
        _timers->truncate(0);

        //Fix cube path in _selector
        Point p(static_cast<int>(cursorPosition->translation.getValue()[0]) / 3, static_cast<int>(cursorPosition->translation.getValue()[1]) / 3, static_cast<int>(cursorPosition->translation.getValue()[2]) / 3);
        _renderarea->selectCube(p);

        _directionOfExplosion = desired;
    }

    //New Explosion to apply

    //calculate number of levels to explode

    int levelsToExplode = 0;
    switch (_directionOfExplosion) {
    case CubeRenderArea::X_PLUS : {
        levelsToExplode = static_cast<int>(cursorPosition->translation.getValue()[0]) / 3 - _explodedLevels;
        break;
    }
    case CubeRenderArea::X_MINUS : {
        levelsToExplode = _dim - static_cast<int>(cursorPosition->translation.getValue()[0]) / 3 - _explodedLevels - 1;
        break;
    }
    case CubeRenderArea::Y_PLUS : {
        levelsToExplode = static_cast<int>(cursorPosition->translation.getValue()[1]) / 3 - _explodedLevels;
        break;
    }
    case CubeRenderArea::Y_MINUS : {
        levelsToExplode = _dim - static_cast<int>(cursorPosition->translation.getValue()[1]) / 3 - _explodedLevels - 1;
        break;
    }
    case CubeRenderArea::Z_PLUS : {
        levelsToExplode = static_cast<int>(cursorPosition->translation.getValue()[2]) / 3 - _explodedLevels;
        break;
    }
    case CubeRenderArea::Z_MINUS : {
        levelsToExplode = _dim - static_cast<int>(cursorPosition->translation.getValue()[2]) / 3 - _explodedLevels - 1;
        break;
    }
    default : {
        break;
    }
    }
    //qDebug() << "Need to explode " << levelsToExplode << " levels.";

    //call explodeLevel() n times to explode n levels
    for (int i = 0; i < levelsToExplode; i++) {
        this->explodeLevel();
    }

    if (_directionOfExplosion != CubeRenderArea::NO_ORIENTATION) {
        _renderarea->setIsExploded(true);
    }
    return;
}
void SceneGraph::applyImplosionToCube(bool all)
{
    //qDebug() << "Apply IMPLOSION to cube.\nBoolean is: " << all << ".";

    int levelsToImplode = 0;

    if (all) {
        levelsToImplode = _explodedLevels;
        _renderarea->setIsExploded(false);
    } else {
        SoTranslation *cursorPosition = static_cast<SoTranslation *>(_selectedCube->getChild(0));

        switch (_directionOfExplosion) {
        case CubeRenderArea::X_PLUS : {
            levelsToImplode = _explodedLevels - static_cast<int>(cursorPosition->translation.getValue()[0]) / 3;
            break;
        }
        case CubeRenderArea::X_MINUS : {
            levelsToImplode = _explodedLevels - _dim + 1 + static_cast<int>(cursorPosition->translation.getValue()[0]) / 3;
            break;
        }
        case CubeRenderArea::Y_PLUS : {
            levelsToImplode = _explodedLevels - static_cast<int>(cursorPosition->translation.getValue()[1]) / 3;
            break;
        }
        case CubeRenderArea::Y_MINUS : {
            levelsToImplode = _explodedLevels - _dim + 1 + static_cast<int>(cursorPosition->translation.getValue()[1]) / 3;
            break;
        }
        case CubeRenderArea::Z_PLUS : {
            levelsToImplode = _explodedLevels - static_cast<int>(cursorPosition->translation.getValue()[2]) / 3;
            break;
        }
        case CubeRenderArea::Z_MINUS : {
            levelsToImplode = _explodedLevels - _dim + 1 + static_cast<int>(cursorPosition->translation.getValue()[2]) / 3;
            break;
        }
        case CubeRenderArea::NO_ORIENTATION : {
            qDebug() << "Error: Cannot apply Implosion with _directionOfExplosion == NO_ORIENTATION.";
            break;
        }
        }
    }

    //qDebug() << "Need to implode " << levelsToImplode << " levels.";

    for (int i = 0; i < levelsToImplode;i++) {
        this->implodeLevel();
    }

    return;
}

void SceneGraph::explodeLevel()
{
    _explodedLevels++;
    if (_explodedLevels - 1 < _movedCubes->getLength()) {
        //qDebug() << "Restore old explosion for level: " << _explodedLevels-1 << ".";

        SoInterpolateVec3f *levelInterpolator = static_cast<SoInterpolateVec3f *>(_interpolators->get(_explodedLevels - 1));
        SoOneShot *levelTimer = static_cast<SoOneShot *>(_timers->get(_explodedLevels - 1));
        levelInterpolator->input1 = levelInterpolator->input0;
        levelInterpolator->input0 = SbVec3f(0, 0, 0);
        levelTimer->trigger.touch();

    } else {
        //qDebug() << "New explosion for level: " << _explodedLevels - 1 << ".";

        SoGroup *newGroup = new SoSeparator();
        SoTranslation *newMover = new SoTranslation();
        SoInterpolateVec3f *newInterpolator = new SoInterpolateVec3f();
        SoOneShot *newTimer = new SoOneShot();

        _movedCubes->append(newGroup);
        _movers->append(newMover);
        _interpolators->append(newInterpolator);
        _timers->append(newTimer);

        _selector->addChild(newGroup);
        newGroup->addChild(newMover);
        newMover->translation.connectFrom(&newInterpolator->output);
        newInterpolator->input0 = SbVec3f(0, 0, 0);
        newInterpolator->alpha.connectFrom(&newTimer->ramp);
        newTimer->flags = SoOneShot::HOLD_FINAL;


        SbVec3f targetPosition = SbVec3f(0, 0, 0);
        for (int i = 0; i < _dim; i++) {
            for (int j = 0; j < _dim; j++) {
                switch (_directionOfExplosion) {
                case CubeRenderArea::X_PLUS : {
                    qDebug() << "Explosion: " << _explodedLevels - 1 << " " << i << " " << j << " selected for move.";

                    SoSeparator *cubeInstance = _graphMatrix[(_explodedLevels-1) * _dim * _dim + i * _dim + j];
                    newGroup->addChild(cubeInstance);
                    _fixedCubes->removeChild(cubeInstance);

                    targetPosition = SbVec3f(0, 0, _dim * 3);
                    break;
                }
                case CubeRenderArea::X_MINUS : {
                    qDebug() << "Explosion: " << _dim - _explodedLevels << " " << i << " " << j << " selected for move.";

                    SoSeparator *cubeInstance = _graphMatrix[(_dim-_explodedLevels) * _dim * _dim + i * _dim + j];
                    newGroup->addChild(cubeInstance);
                    _fixedCubes->removeChild(cubeInstance);

                    targetPosition = SbVec3f(0, 0, -_dim * 3);
                    break;
                }
                case CubeRenderArea::Y_PLUS : {
                    qDebug() << "Explosion: " << i << " " << _explodedLevels - 1 << " " << j << " selected for move.";

                    SoSeparator *cubeInstance = _graphMatrix[i * _dim * _dim + (_explodedLevels-1) * _dim + j];
                    newGroup->addChild(cubeInstance);
                    _fixedCubes->removeChild(cubeInstance);

                    targetPosition = SbVec3f(_dim * 3, 0, 0);
                    break;
                }
                case CubeRenderArea::Y_MINUS : {
                    qDebug() << "Explosion: " << i << " " << _dim - _explodedLevels << " " << j << " selected for move.";

                    SoSeparator *cubeInstance = _graphMatrix[i * _dim * _dim + (_dim-_explodedLevels) * _dim + j];
                    newGroup->addChild(cubeInstance);
                    _fixedCubes->removeChild(cubeInstance);

                    targetPosition = SbVec3f(-_dim * 3, 0, 0);
                    break;
                }
                case CubeRenderArea::Z_PLUS : {
                    qDebug() << "Explosion: " << j << " " << i << " " << _explodedLevels - 1 << " selected for move.";

                    SoSeparator *cubeInstance = _graphMatrix[j * _dim * _dim + i * _dim + _explodedLevels-1];
                    newGroup->addChild(cubeInstance);
                    _fixedCubes->removeChild(cubeInstance);

                    targetPosition = SbVec3f(_dim * 3, 0, 0);
                    break;
                }
                case CubeRenderArea::Z_MINUS : {
                    qDebug() << "Explosion: " << j << " " << i << " " << _dim - _explodedLevels << " selected for move.";

                    SoSeparator *cubeInstance = _graphMatrix[j * _dim * _dim + i * _dim + _dim-_explodedLevels];
                    newGroup->addChild(cubeInstance);
                    _fixedCubes->removeChild(cubeInstance);

                    targetPosition = SbVec3f(-_dim * 3, 0, 0);
                    break;
                }
                case CubeRenderArea::NO_ORIENTATION : {
                    qDebug() << "Error: Cannot apply Explosion with frontside == NO_ORIENTATION.";
                    break;
                }
                }
            }
        }
        //FIX ME:
        //target position is not upside dependent.
        //not so pretty...
        newInterpolator->input1 = targetPosition;

        //Start animation
        newTimer->trigger.touch();
    }
    //qDebug() << "Explosion for level " << _explodedLevels - 1 << "done.";
    return;
}

void SceneGraph::implodeLevel()
{
    //qDebug() << "Implosion for level " << _explodedLevels -1 << ".";
    _explodedLevels--;
    SoInterpolateVec3f *levelInterpolator = static_cast<SoInterpolateVec3f *>(_interpolators->get(_explodedLevels));
    SoOneShot *levelTimer = static_cast<SoOneShot *>(_timers->get(_explodedLevels));
    levelInterpolator->input0 = levelInterpolator->input1;
    levelInterpolator->input1 = SbVec3f(0, 0, 0);
    levelTimer->trigger.touch();
    return;
}

CubeRenderArea *SceneGraph::renderarea()
{
    return _renderarea;
}
SoSeparator *SceneGraph::root()
{
    return _root;
}
SoCamera *SceneGraph::camera()
{
    return _camera;
}
SoSelection *SceneGraph::selector()
{
    return _selector;
}
SoGroup *SceneGraph::fixedCubes()
{
    return _fixedCubes;
}
SoBaseList *SceneGraph::movedCubes()
{
    return _movedCubes;
}
SoShape *SceneGraph::shape()
{
    return _shape;
}
SoBaseList *SceneGraph::movers()
{
    return _movers;
}
SoBaseList *SceneGraph::interpolators()
{
    return _interpolators;
}
SoBaseList *SceneGraph::timers()
{
    return _timers;
}
SoOneShotSensor *SceneGraph::explodeSensor()
{
    return _explodeSensor;
}
SoOneShotSensor *SceneGraph::implodeSensor()
{
    return _implodeSensor;
}
SoSeparator **SceneGraph::graphMatrix()
{
    return _graphMatrix;
}
SoSeparator *SceneGraph::selectedCube()
{
    return _selectedCube;
}
CubeRenderArea::ORIENTATION SceneGraph::directionOfExplosion()
{
    return _directionOfExplosion;
}
CubeRenderArea::ORIENTATION SceneGraph::desiredDirectionOfExplosion()
{
    return _desiredDirectionOfExplosion;
}
bool SceneGraph::allImplosion()
{
    return _allImplosion;
}
int SceneGraph::explodedLevels()
{
    return _explodedLevels;
}

void SceneGraph::setSelectedCube(SoSeparator *cube)
{
    _selectedCube = cube;
    return;
}
void SceneGraph::setDirectionOfExplosion(CubeRenderArea::ORIENTATION direction)
{
    _directionOfExplosion = direction;
    return;
}
void SceneGraph::setDesiredDirectionOfExplosion(CubeRenderArea::ORIENTATION direction)
{
    _desiredDirectionOfExplosion = direction;
    return;
}
void SceneGraph::setAllImplosion(bool value)
{
    _allImplosion = value;
    return;
}
void SceneGraph::setExplodedLevels(int num)
{
    _explodedLevels = num;
    return;
}



