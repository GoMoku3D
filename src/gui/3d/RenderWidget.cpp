/********************************************************************
 *
 * Copyright (C) 2008  Tobia Zorzan
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "RenderWidget.h"

/*!
 * @class RenderWidget RenderWidget.h
 * @brief RenderWidget fornisce un widget Qt con visuale 3D.
 *
 * RenderWidget è la classe a più alto livello della componente 3D dell'applicazione.
 * I metodi di tale classe sono metodi usati dalle altre componenti, e quindi da
 * reimplementare in caso si desidere fornire un'altro tipo di visuale del campo di
 * gioco.
 */

/*!
 * Costruttore della classe.
 * @param parent Puntatore al widget genitore.
 * @param dim La dimensione della matrice di gioco. Se uguale a 0, non viene inizializzata
 * la parte 3D ma viene inserita un'immagine segnaposto.
 */
RenderWidget::RenderWidget(QWidget *parent, int dim)
        : QWidget(parent), _renderarea(0), _layout(0), _imageLabel(0)
{
    _layout = new QVBoxLayout(this);
    if (!_image.load("./gomoku3d.png")) {
        qWarning() << "Failed to load image file.";
    }
    if (dim > 0) {
        init(dim);
    } else {
        reset();
    }

    setWhatsThis(tr("Here is displayed the game matrix. Possible interactions are:\n"
                    "- left mouse button click and drag: rotate the game matrix;\n"
                    "- left mouse button: select a cube;\n"
                    "- mouse wheel: zoom in and out;\n"
                    "- ENTER key: confirm a move for the current player;\n"
                    "- SPACE key: enable the explode function;\n"
                    "- UP, DOWN, LEFT, RIGHT arrows: change selected cube;\n"
                    "- W, S keys: change selected cube.\n"));
}

/*!
 * Distruttore della classe.
 * Dealloca gli oggetti puntati dai campi dato della classe al momento della distruzione.
 */
RenderWidget::~RenderWidget()
{
    if (_renderarea) {
        delete _renderarea;
    }
    if (_layout) {
        delete _layout;
    }
}

/*!
 * Inizializza la componente 3D e visualizza una matrice di gioco di dimensione uguale a \c dim.
 * @param dim Dimensione della matrice di gioco da rappresentare.
 */
void RenderWidget::init(int dim)
{
    if (_imageLabel) {
        _layout->removeWidget(_imageLabel);
        delete _imageLabel;
        _imageLabel = 0;
    }
    Q_ASSERT(_renderarea == 0);
    _renderarea = new CubeRenderArea(this, dim);
    _layout->addWidget(_renderarea->getWidget());
    GUISettings settings;
    _defaultCubeColor = settings.defaultCubeColor();
    this->setFocusProxy(_renderarea->getWidget());
}

/*!
 * Resetta il widget. Viene distrutta la componente 3D e sostituita con un'immagine segnaposto.
 */
void RenderWidget::reset()
{
    this->setFocusProxy(0);
    if (_renderarea) {
        _renderarea->getWidget()->hide();
        _layout->removeWidget(_renderarea->getWidget());
        delete _renderarea;
        _renderarea = 0;
    }
    if (!_imageLabel) {
        _imageLabel = new QLabel();
        _imageLabel->setPixmap(_image);
        _layout->addWidget(_imageLabel, 0, Qt::AlignCenter);
    }
}

/*!
 * Abilita e disabilita la possibilità di inviare mosse dall'interfaccia grafica.
 * @param value Se \c true permette all'utente di inviare una mossa. Se \c false disabilita l'invio delle mosse.
 */
void RenderWidget::acceptMove(bool value)
{
    if (_renderarea) {
        _renderarea->setAcceptMove(value);
    }
}

/*!
 * Disegna una mossa in posizione indicata da \c p con il colore \c c.
 * @param p La posizione da colorare.
 * @param c Il colore da usare per rappresentare la mossa.
 */
void RenderWidget::drawMove(Point p, QColor c)
{
    if (_renderarea) {
        if (p.isValid()) {
            _renderarea->drawMove(p, _renderarea->convertColor(c));
        } else {
            qWarning() << "RenderWidget::drawMove() :" << tr("invalid point");
        }
    }
}

/*!
 * Disegna una serie di mosse contnute nelle liste parametro. Utilizzata principalmente
 * per rispristinare una situazione di gioco precedente. Se il parametro \c colorsList è
 * una lista vuota, per titti i punti da rappresentare viene usato il colore di default.
 * @param pointsList Una lista di punti.
 * @param colorsList Una lista di colori da utilizzare per le mosse.
 */
void RenderWidget::uncheckedDraw(QList<Point> pointsList, QList<QColor> colorsList)
{
    if (_renderarea) {
        if (colorsList.isEmpty()) {
            // draw all points with the default color
            SbColor4f color = _renderarea->convertColor(_defaultCubeColor);
            Point point;
            foreach(point, pointsList) {
                _renderarea->drawMove(point, color);
            }
        } else {
            Q_ASSERT(pointsList.size() == colorsList.size());
            for (int i = 0; i < pointsList.size(); i++) {
                _renderarea->drawMove(pointsList.at(i), _renderarea->convertColor(colorsList.at(i)));
            }
        }
    }
}

/*!
 * Imposta il colore di sfondo della scena.
 * Viene utilizzato il colore impostato nelle preferenze.
 */
void RenderWidget::updateBackground()
{
    if (_renderarea) {
        GUISettings settings;
        SbColor background;
        (_renderarea->convertColor(settings.backgroundColor())).getRGB(background);
        _renderarea->setBackgroundColor(background);
    }
}

/*!
 * Imposta il valore della funzione di antialiasing.
 * @param value Intero \c >=0 \c <= 6 . Più alto è il valore, maggiore è la qualità dello smussamento.
 */
void RenderWidget::setAntialiasing(int value)
{
    // TODO
    qDebug() << "Setting antialiasing to:" << value;
}

/*!
 * Ritorna il valore del campo dato privato \c _renderarea .
 */
CubeRenderArea *RenderWidget::renderarea()
{
    return _renderarea;
}

/*!
 * Seleziona una particolare posizione nella visuale 3D.
 * @param p La posizione da selezionare.
 */
void RenderWidget::highlightPoint(Point p)
{
    _renderarea->selectCube(p, SbColor(0.9, 0.9, 0));
}
