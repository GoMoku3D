/********************************************************************
 *
 * Copyright (C) 2008  Tobia Zorzan
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/
 
#ifndef CUBERENDERAREA_H
#define CUBERENDERAREA_H

#include <Inventor/Qt/SoQtRenderArea.h>
#include <Inventor/projectors/SbSphereSheetProjector.h>
#include <Inventor/actions/SoLineHighlightRenderAction.h>
#include <Inventor/SbColor4f.h>

#include <QDebug>
#include <QMutex>
#include <QEvent>
#include <QMouseEvent>

#include "Point.h"
#include "GameMatrix.h"
#include "SyncSharedCondition.h"
#include "GUISettings.h"


class SceneGraph;

class CubeRenderArea : public SoQtRenderArea 
{
	private:
		SceneGraph *_scene;
		bool _acceptMove;
		int _numCubes;
		bool _isExploded;
		bool _isRotating;
		bool _hasPreviousPick;
		int _lastXPick;
		int _lastYPick;
		SbSphereSheetProjector *_projector;
		SoLineHighlightRenderAction *_highlightAction;
		QMutex _mutex;

	public:
		enum ORIENTATION 
		{
			X_PLUS,
			X_MINUS,
			Y_PLUS,
			Y_MINUS,
			Z_PLUS,
			Z_MINUS,
			NO_ORIENTATION,
		};
		
		enum DIRECTION
		{
			UP,
			DOWN,
			LEFT,
			RIGHT,
			IN,
			OUT,
		};
		
		CubeRenderArea(QWidget *parent, int dim);
		~CubeRenderArea();
		void drawMove(Point p, SbColor4f c);
		void doMove();
		ORIENTATION getOrientation(DIRECTION direction);
		Point getAdiacentPosition(Point oldpoint, ORIENTATION orientation);
		void selectCube(Point p, SbColor c = SbColor(1.0, 0.0, 0.0));
		void rotateCamera(int newX, int newY);
		void zoomCamera(int delta);
		static SbColor4f convertColor(QColor qtcolor);
		
		static SbBool eventDispatcher(void *userdata, QEvent *event);
		static void cubeSelected(void *userdata, SoPath *path);
		static void cubeDeselected(void *userdata, SoPath *path);
		
		SceneGraph *scene();
		bool acceptMove();
		int numCubes();
		bool isExploded();
		bool isRotating();
		bool hasPreviousPick();
		QMutex &mutex();

		void setAcceptMove(bool value);
		void setIsExploded(bool value);
		void setIsRotating(bool value);
		void setHasPreviousPick(bool value);

	private:
		ORIENTATION _frontside;
		ORIENTATION _upside;

	public:
		ORIENTATION frontside();
		ORIENTATION upside();
		
		void setFrontside(ORIENTATION value);
		void setUpside(ORIENTATION value);
};

#endif
#include "SceneGraph.h"
