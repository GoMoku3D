/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "TimerWidget.h"

TimerWidget::TimerWidget(QWidget *parent) : QLCDNumber(3, parent)
{
    setSegmentStyle(Filled);
    _duration = 0;
    _timer.setInterval(1000);
    display(_duration);

    setWhatsThis(tr("Timer for your turn.\nDon't let the timer expire, or you will lose."));

    QObject::connect(&_timer, SIGNAL(timeout()), this, SLOT(handleTimeout()));
}

void TimerWidget::setDuration(int sec)
{
    _duration = sec;
}

void TimerWidget::start()
{
    display(_duration);
    if (_duration > 0) {
        _timer.start();
    }
}


void TimerWidget::stop()
{
    display(0);
    _timer.stop();
}

void TimerWidget::handleTimeout()
{
    int val = intValue();
    if (val > 0) {
        val--;
        display(val);
        _timer.start();
    }
    else {
        //emetto un segnale a MW che deve dire all'utente che ha perso e fare le cose da fare
        emit timerExpired();
    }
}
