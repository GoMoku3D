/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento and Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QMessageBox>
#include <QFileDialog>
#include <QWhatsThis>

#include "MainWindow.h"
#include "GameLoop.h"
#include "GameMatrix.h"
#include "AIPlayer.h"
#include "RemotePlayer.h"
#include "HumanPlayer.h"
#include "RenderWidget.h"
#include "HistoryModel.h"
#include "ChatWidget.h"
#include "TimerWidget.h"
#include "Network.h"
#include "OnlineDialog.h"
#include "Suggester.h"
#include "Preferences.h"
#include "PlayersWidget.h"
#include "StandAloneDialog.h"
#include "ServerSettingsDialog.h"
#include "LocalSettings.h"
#include "AboutDialog.h"

MainWindow::MainWindow() : Ui_MainWindow(*this)
{
    _loop = 0;
    _net = 0;
    _suggester = 0;
    _history = 0;
    _actualPlayerIndex = -1;
    _isStandAloneGame = false;
    _isOnlineGame = false;
    _isServerGame = false;
    _isDedicatedServer = false;
    _timerDuration = 0;

    setupUi(this);

    QAction *whatsThis = QWhatsThis::createAction(this);
    whatsThis->setText(tr("What's This?"));
    menuHelp->addAction(whatsThis);

    _render = new RenderWidget(this);
    setCentralWidget(_render);

    QDockWidget *playersDock = new QDockWidget(this);
    QDockWidget *historyDock = new QDockWidget(this);
    QDockWidget *timerDock = new QDockWidget(this);
    QDockWidget *chatDock = new QDockWidget(this);
    playersDock->setWindowTitle(tr("Players' Info"));
    historyDock->setWindowTitle(tr("History"));
    timerDock->setWindowTitle(tr("Timer"));
    chatDock->setWindowTitle(tr("Chat"));
    playersDock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    historyDock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    timerDock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    chatDock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    historyDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    timerDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    chatDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    _playersWidget = new PlayersWidget(playersDock);
    _historyView = new QTableView(historyDock);
    _timer = new TimerWidget(timerDock);
    _chat = new ChatWidget(chatDock);
    playersDock->setWidget(_playersWidget);
    historyDock->setWidget(_historyView);
    timerDock->setWidget(_timer);
    chatDock->setWidget(_chat);
    addDockWidget(Qt::LeftDockWidgetArea, playersDock);
    addDockWidget(Qt::LeftDockWidgetArea, historyDock);
    addDockWidget(Qt::RightDockWidgetArea, timerDock);
    addDockWidget(Qt::RightDockWidgetArea, chatDock);

    _history = new HistoryModel(this, &_playersInfo);
    _historyView->setModel(_history);
    _historyView->setSelectionBehavior(QTableView::SelectRows);

    connect(_timer, SIGNAL(timerExpired()), this, SLOT(timerExpired()));
    connect(_historyView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(undoMoves(QModelIndex)));
    connect(_historyView, SIGNAL(clicked(QModelIndex)), this, SLOT(showPastGameStatus(QModelIndex)));
    connect(actionShow_last_move, SIGNAL(triggered()), this, SLOT(showLastMove()));
    connect(actionSuggest_move, SIGNAL(triggered()), this, SLOT(suggestMove()));
    connect(actionTake_back_move, SIGNAL(triggered()), this, SLOT(undoTurn()));
    connect(actionHost_Game, SIGNAL(triggered()), this, SLOT(newServerGame()));
    connect(actionConnect_To_Server, SIGNAL(triggered()), this, SLOT(newOnlineGame()));
    connect(actionNew_local_game,SIGNAL(triggered()), this, SLOT(newStandAloneGame()));
    connect(actionSave_game, SIGNAL(triggered()), this, SLOT(saveGame()));
    connect(actionLoad_game, SIGNAL(triggered()), this, SLOT(loadGame()));
    connect(actionPreferences, SIGNAL(triggered()), this, SLOT(showPreferences()));
    connect(actionAbout_Qt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(showAbout()));
}

MainWindow::~MainWindow()
{
    GUISettings set;
    set.setGeometry(this->saveGeometry());

    if (_loop != 0) {
        // tell the GameLoop to stop running
        _loop->stop();
    }

    delete _render;

    if (_suggester != 0) {
        _suggester->wait();
        _suggester->deleteLater();
    }

    if (_net != 0) {
        _net->deleteLater();
    }

    if (_loop != 0) {
        // wake up GameLoop if it's sleeping to avoid any possible deadlock
        SyncSharedCondition::instance()->lock();
        SyncSharedCondition::instance()->notifyMove(Point());
        SyncSharedCondition::instance()->unlock();
        // wait for GameLoop to finish execution...
        _loop->wait();
        // ...and delete it
        _loop->deleteLater();
    }
}

void MainWindow::drawMove(Move move)
{
    if (_playersInfo.isEmpty()) {
        return;
    }

    QColor color;
    if (move.playerId() == 0) {
        color = _playersInfo.at(0).color();
    }
    if (move.playerId() == 1) {
        color = _playersInfo.at(1).color();
    }
    if (move.playerId() == 2) {
        color = _playersInfo.at(2).color();
    }

    _timer->stop();

    bool ok = move.isValid();
    if (ok) {
        _history->append(move);
    }
    else {
        if (!_isDedicatedServer) {
            _render->acceptMove(false);
        }
        // messaggio di errore
        // se alla drawMove arriva un point nullo cosa vuol dire?
        //   - dal network: scaduto il timer del giocatore
        //   - quando viene rilasciato il lock in fase di distruzione ma non dovrebbe succedere niente
        QMessageBox::information(this, tr("Game Ended"), tr("Timer expired:\n%1 lost the game.").arg(_playersInfo.at(move.playerId()).name()), QMessageBox::Ok);
        disableOnEndGame();
    }

    if (!_isDedicatedServer && ok) {
        if (!_actualViewIndex.isValid() || (_actualViewIndex.row() == _history->moveList().size() - 2)) {
            _render->drawMove(move.point(), color);
            _actualViewIndex = _history->index(_history->moveList().size() - 1, 0);
            _historyView->setCurrentIndex(_actualViewIndex);
        }
        actionShow_last_move->setEnabled(true);
        if (_isStandAloneGame) {
            actionTake_back_move->setEnabled(true);
        }
    }
    else {
        actionShow_last_move->setEnabled(false);
        actionTake_back_move->setEnabled(false);
    }
}

QModelIndex MainWindow::actualViewIndex() const
{
    return _actualViewIndex;
}

bool MainWindow::askUserConfirmation()
{
    QMessageBox::StandardButton result = QMessageBox::question(this,
            tr("Abandon Game?"),
            tr("If you continue, the current\ngame will be lost.\n\nAre you sure you want to continue?"),
            QMessageBox::Yes | QMessageBox::No);

    if (result == QMessageBox::Yes) {
        return true;
    } else {
        return false;
    }
}

void MainWindow::newStandAloneGame()
{
    if (_loop != 0 && !askUserConfirmation()) {
        return;
    }

    resetOnNewGame();

    StandAloneDialog dialog(this);
    dialog.exec();
    if (dialog.result() == QDialog::Rejected) {
        return;
    }

    qobject_cast<QDockWidget*>(_timer->parent())->setVisible(false);
    qobject_cast<QDockWidget*>(_chat->parent())->setVisible(false);
    qobject_cast<QDockWidget*>(_playersWidget->parent())->setVisible(true);
    qobject_cast<QDockWidget*>(_historyView->parent())->setVisible(true);

    _isStandAloneGame = true;

    // abilito alcune voci del menu
    actionLoad_game->setEnabled(true);
    actionSave_game->setEnabled(true);

    int d1 = dialog.d1Box->currentText().toInt();
    int d2 = dialog.d2Box->currentText().toInt();
    int numPlayers = dialog.numPlayersBox->currentText().toInt();
    Q_ASSERT(d1 != 0);
    Q_ASSERT(d2 != 0);
    Q_ASSERT(numPlayers != 0);
    GameMatrix::create(d1, d2, numPlayers);
    Q_ASSERT(GameMatrix::instance() != 0);

    _render->init(d1 * d2);

    QString type;
    QList<Player*> players;
    QList<QColor> color = dialog.colorList();
    if (dialog.human_0->isChecked()) {
        type = "H";
        players.append(new HumanPlayer(0, _render));
    }
    else {
        type = "A";
        players.append(new AIPlayer(0));
    }
    _playersInfo.append(PlayerInfo(dialog.playername_0->text(), color.at(0), type));

    if (dialog.human_1->isChecked()) {
        type = "H";
        players.append(new HumanPlayer(1, _render));
    }
    else {
        type = "A";
        players.append(new AIPlayer(1));
    }
    _playersInfo.append(PlayerInfo(dialog.playername_1->text(), color.at(1), type));

    if (numPlayers == 3) {
        if (dialog.human_2->isChecked()) {
            type = "H";
            players.append(new HumanPlayer(2, _render));
        }
        else {
            type = "A";
            players.append(new AIPlayer(2));
        }
        _playersInfo.append(PlayerInfo(dialog.playername_2->text(), color.at(2), type));
    }

    _playersWidget->set(&_playersInfo);
    connectAISkillSlider(players);

    //costruisco GL
    _loop = new GameLoop(players);
    //move to thread dei player
    Player *p;
    foreach (p, players) {
        p->moveToThread(_loop);
    }

    connect(actionForce_Move, SIGNAL(triggered()), _loop, SLOT(forceMove()));
    connect(_loop, SIGNAL(turn(int)), this, SLOT(turn(int)));
    connect(_loop, SIGNAL(turn(int)), _playersWidget, SLOT(updateTurn(int)));
    connect(_loop, SIGNAL(moved(Move)), this, SLOT(drawMove(Move)));
    connect(_loop, SIGNAL(win(int)), this, SLOT(playerWin(int)));
    connect(_loop, SIGNAL(draw(QList<int>)), this, SLOT(playersDraw(QList<int>)));

    _loop->start();
}

void MainWindow::newOnlineGame()
{
    if (_loop != 0 && !askUserConfirmation()) {
        return;
    }

    resetOnNewGame();

    OnlineDialog dialog(this);
    dialog.exec();
    if (dialog.result() == QDialog::Rejected) {
        if (_net != 0) {
            _net->deleteLater();
            QApplication::processEvents();
            _net = 0;
        }
        return;
    }

    qobject_cast<QDockWidget*>(_timer->parent())->setVisible(true);
    qobject_cast<QDockWidget*>(_chat->parent())->setVisible(true);
    qobject_cast<QDockWidget*>(_playersWidget->parent())->setVisible(true);
    qobject_cast<QDockWidget*>(_historyView->parent())->setVisible(true);

    connect(_net, SIGNAL(error(QString)), this, SLOT(networkError(QString)));

    _isOnlineGame = true;

    //estraggo i dati
    int d1 = dialog.d1Label->text().toInt();
    int d2 = dialog.d2Label->text().toInt();
    int numPlayers = dialog.numPlayersLabel->text().toInt();
    GameMatrix::create(d1, d2, numPlayers);

    _render->init(d1 * d2);

    actionShow_last_move->setEnabled(true);
    //estraggo e costruisco la lista di playersInfo
    //costruisco i player
    QList<Player*> players;
    QList<QColor> color = dialog.colorList();
    if (dialog._typeList[0] == "H") {
        players.append(new HumanPlayer(0, _render));
    }
    else {
        players.append(new RemotePlayer(0, _net));
    }
    _playersInfo.append(PlayerInfo(dialog.name_0->text(), color.at(0), dialog._typeList[0]));
    if (dialog._typeList[1] == "H") {
        players.append(new HumanPlayer(1, _render));
    }
    else {
        players.append(new RemotePlayer(1, _net));
    }
    _playersInfo.append(PlayerInfo(dialog.name_1->text(), color.at(1), dialog._typeList[1]));
    if (numPlayers == 3) {
        if (dialog._typeList[2] == "H") {
            players.append(new HumanPlayer(2, _render));
        }
        else {
            players.append(new RemotePlayer(2, _net));
        }
        _playersInfo.append(PlayerInfo(dialog.name_2->text(), color.at(2), dialog._typeList[2]));
    }


    _playersWidget->set(&_playersInfo);

    _net->setupChat(_chat);

    int t = dialog.timerLabel->text().toInt();
    _timerDuration = t;
    _timer->setDuration(t);

    //costruisco GL
    _loop = new GameLoop(players);

    if (!_buffer.isEmpty()) {
        Move m;
        GameMatrix *mat = GameMatrix::instance();
        QList<bool> win;
        for (int i = 0; i < _playersInfo.size(); i++) {
            win.append(false);
        }

        foreach (m, _buffer) {
            win[m.playerId()] = mat->add(m);
            _history->append(m);
            _render->drawMove(m.point(), _playersInfo.at(m.playerId()).color());
        }
        _loop->winStatus(win);
        _loop->onLoadSetTurn(_buffer.size() % _playersInfo.size());
        _actualViewIndex = _history->index(_history->moveList().size() - 1, 0);
    }
    _buffer.clear();
    //move to thread dei player
    Player *p;
    foreach (p, players) {
        p->moveToThread(_loop);
    }

    _net->setupGameLoop(_loop);

    //connetto
    connect(_loop, SIGNAL(turn(int)), this, SLOT(turn(int)));
    connect(_loop, SIGNAL(turn(int)), _playersWidget, SLOT(updateTurn(int)));
    connect(_loop, SIGNAL(moved(Move)), this, SLOT(drawMove(Move)));
    connect(_loop, SIGNAL(win(int)), this, SLOT(playerWin(int)));
    connect(_loop, SIGNAL(draw(QList<int>)), this, SLOT(playersDraw(QList<int>)));

    _loop->start();
}

void MainWindow::updateHistory(QList<Move> list)
{
    _buffer = list;
}

void MainWindow::newServerGame()
{
    if (_loop != 0 && !askUserConfirmation()) {
        return;
    }

    resetOnNewGame();

    ServerSettingsDialog dialog(this);
    dialog.exec();
    if (dialog.result() == QDialog::Rejected) {
        if (_net != 0) {
            _net->deleteLater();
            QApplication::processEvents();
            _net = 0;
        }
        return;
    }

    connect(_net, SIGNAL(error(QString)), this, SLOT(networkError(QString)));

    int d1 = dialog.d1Box->currentText().toInt();
    int d2 = dialog.d2Box->currentText().toInt();
    int numPlayers = dialog.numPlayersBox->currentText().toInt();

    GameMatrix::create(d1, d2, numPlayers);

    QString type;
    QList<Player*> players;
    QList<QColor> color = dialog.colorList();
    if (dialog.human_0->isChecked()) {
        type = "H";
        actionShow_last_move->setEnabled(true);
        players.append(new HumanPlayer(0, _render));
    }
    else {
        if (dialog.ai_0->isChecked()) {
            type = "A";
            players.append(new AIPlayer(0));
        }
        else {
            type = "R";
            players.append(new RemotePlayer(0, _net));
        }
    }
    _playersInfo.append(PlayerInfo(dialog.playername_0->text(), color.at(0), type));
    if (dialog.human_1->isChecked()) {
        type = "H";
        actionShow_last_move->setEnabled(true);
        players.append(new HumanPlayer(1, _render));
    }
    else {
        if (dialog.ai_1->isChecked()) {
            type = "A";
            players.append(new AIPlayer(1));
        }
        else {
            type = "R";
            players.append(new RemotePlayer(1, _net));
        }
    }
    _playersInfo.append(PlayerInfo(dialog.playername_1->text(), color.at(1), type));
    if (numPlayers == 3) {
        if (dialog.human_2->isChecked()) {
            type = "H";
            actionShow_last_move->setEnabled(true);
            players.append(new HumanPlayer(2, _render));
        }
        else {
            if (dialog.ai_2->isChecked()) {
                type = "A";
                players.append(new AIPlayer(2));
            }
            else {
                type = "R";
                players.append(new RemotePlayer(2, _net));
            }
        }
        _playersInfo.append(PlayerInfo(dialog.playername_2->text(), color.at(2), type));
    }

    if (dialog.timerCheckBox->isChecked()) {
        int t = dialog.timerSpinBox->value();
        _timerDuration = t;
        _timer->setDuration(t);
    }

    //costruisco GL
    _loop = new GameLoop(players);
    //move to thread dei player
    Player *p;
    foreach (p, players) {
        p->moveToThread(_loop);
    }
    connectAISkillSlider(players);

    _net->setupGameLoop(_loop);

    bool noHuman = true;
    for (int i = 0; i < _playersInfo.size(); i++) {
        if (_playersInfo.at(i).type() == "H") {
            noHuman = false;
        }
    }

    if (noHuman && dialog.dedicatedserver->isChecked()) {
        // modalità server dedicato
        _isDedicatedServer = true;
        qobject_cast<QDockWidget*>(_timer->parent())->setVisible(false);
        qobject_cast<QDockWidget*>(_chat->parent())->setVisible(false);
    } else {
        // modalità giocatore o spettatore
        _isServerGame = true;
        //TODO
        _render->init(d1 * d2);
        _net->setupChat(_chat);
        qobject_cast<QDockWidget*>(_timer->parent())->setVisible(true);
        qobject_cast<QDockWidget*>(_chat->parent())->setVisible(true);
    }
    _playersWidget->set(&_playersInfo);
    qobject_cast<QDockWidget*>(_playersWidget->parent())->setVisible(true);
    qobject_cast<QDockWidget*>(_historyView->parent())->setVisible(true);
    connect(_loop, SIGNAL(turn(int)), this, SLOT(turn(int)));
    connect(_loop, SIGNAL(turn(int)), _playersWidget, SLOT(updateTurn(int)));
    connect(_loop, SIGNAL(moved(Move)), this, SLOT(drawMove(Move)));
    connect(_loop, SIGNAL(win(int)), this, SLOT(playerWin(int)));
    connect(_loop, SIGNAL(draw(QList<int>)), this, SLOT(playersDraw(QList<int>)));
    _loop->start();
}

void MainWindow::showPreferences()
{
    Preferences pref(this);
    int result = pref.exec();
    if (result == QDialog::Accepted) {
        _render->updateBackground();
    }
}

void MainWindow::disableOnEndGame()
{
    //disabilita menu
    actionTake_back_move->setEnabled(false);
    actionSuggest_move->setEnabled(false);
    actionForce_Move->setEnabled(false);
    actionSave_game->setEnabled(false);
    actionShow_last_move->setEnabled(false);
    _playersWidget->arrow_0->setEnabled(false);
    _playersWidget->arrow_1->setEnabled(false);
    _playersWidget->arrow_2->setEnabled(false);
}

void MainWindow::playerWin(int playerId)
{
    disableOnEndGame();
    QMessageBox::information(this, tr("Game Ended"), tr("And the winner is ...\n%1!").arg(_playersInfo.at(playerId).name()), QMessageBox::Ok);
}

void MainWindow::playersDraw(QList<int> playersIds)
{
    disableOnEndGame();
    QString result;
    int id;
    foreach (id, playersIds) {
        result += "\n - " + _playersInfo.at(id).name();
    }
    QMessageBox::information(this, tr("Game Ended"), tr("Game ended in a draw:%1").arg(result), QMessageBox::Ok);
}

void MainWindow::turn(int playerId)
{
    _actualPlayerIndex = playerId;
    if (_playersInfo.at(playerId).type() == "H" && _isStandAloneGame) {
        actionSuggest_move->setEnabled(true);
    }
    else {
        actionSuggest_move->setEnabled(false);
    }
    if (_playersInfo.at(playerId).type() == "A" && !_isOnlineGame) {
        actionForce_Move->setEnabled(true);
    }
    else {
        actionForce_Move->setEnabled(false);
    }
    bool startTimer = _playersInfo.at(playerId).type() == "H";
    if (!_isStandAloneGame && _timerDuration > 0 && startTimer) {
        _timer->start();
    }
}

void MainWindow::resetOnNewGame()
{
    disableOnEndGame();

    if (_timer != 0) {
        _timer->stop();
    }

    if (_loop != 0) {
        // tell the GameLoop to stop running
        _loop->stop();
    }

    _render->reset();

    if (_suggester != 0) {
        _suggester->wait();
        _suggester->deleteLater();
        _suggester = 0;
    }

    if (_net != 0) {
        _net->deleteLater();
        QApplication::processEvents();
        _net = 0;
    }

    if (_loop != 0) {
        // wake up GameLoop if it's sleeping to avoid any possible deadlock
        SyncSharedCondition::instance()->lock();
        SyncSharedCondition::instance()->notifyMove(Point());
        SyncSharedCondition::instance()->unlock();
        // wait for GameLoop to finish execution...
        _loop->wait();
        // ...and delete it
        _loop->deleteLater();
        _loop = 0;
    }

    if (_chat != 0) {
        _chat->reset();
    }

    if (_playersWidget != 0) {
        _playersWidget->reset();
    }

    _playersInfo.clear();

    if (_history != 0) {
        _history->reset();
    }

    _isStandAloneGame = false;
    _isOnlineGame = false;
    _isServerGame = false;
    _isDedicatedServer = false;
    _actualViewIndex = QModelIndex();
    _actualPlayerIndex = 0;
    _timerDuration = 0;
    _buffer.clear();
}

void MainWindow::suggestMove()
{
    if (_isStandAloneGame) {
        if (_suggester != 0) {
            _suggester->wait();
            _suggester->deleteLater();
            _suggester = 0;
        }
        _suggester = new Suggester(_actualPlayerIndex);
        connect(_suggester, SIGNAL(suggestedMoveReady(Point)), _render, SLOT(highlightPoint(Point)));
        _suggester->start();
    }
}

bool MainWindow::isOnlineGame() const
{
    return _isOnlineGame;
}

void MainWindow::connectAISkillSlider(const QList<Player*> &players)
{
    AIPlayer *ai0 = qobject_cast<AIPlayer*>(players.at(0));
    if (ai0 != 0) {
        connect(_playersWidget->skill_0, SIGNAL(valueChanged(int)), ai0, SLOT(setSkill(int)), Qt::DirectConnection);
    }
    AIPlayer *ai1 = qobject_cast<AIPlayer*>(players.at(1));
    if (ai1 != 0) {
        connect(_playersWidget->skill_1, SIGNAL(valueChanged(int)), ai1, SLOT(setSkill(int)), Qt::DirectConnection);
    }
    if (players.size() == 3) {
        AIPlayer *ai2 = qobject_cast<AIPlayer*>(players.at(2));
        if (ai2 != 0) {
            connect(_playersWidget->skill_2, SIGNAL(valueChanged(int)), ai2, SLOT(setSkill(int)), Qt::DirectConnection);
        }
    }
}

void MainWindow::showLastMove()
{
    if (_history && !_isDedicatedServer && !_history->moveList().isEmpty()) {
        _render->highlightPoint(_history->moveList().last().point());
    }
}

void MainWindow::showPastGameStatus(QModelIndex index)
{
    if (_isDedicatedServer || index == _actualViewIndex) {
        return;
    }
    QList<Point> pointList;
    QList<QColor> colorList;
    if (_actualViewIndex < index) {
        QList<Move> list = _history->moveList();
        for (int i = _actualViewIndex.row() + 1; i <= index.row(); i++) {
            pointList.append(list.at(i).point());
            int id = list.at(i).playerId();
            colorList.append(_playersInfo.at(id).color());
        }
    }
    else {
        QList<Move> list = _history->moveList();
        for (int i = index.row() + 1; i <= _actualViewIndex.row(); i++) {
            pointList.append(list.at(i).point());
        }
    }
    _render->uncheckedDraw(pointList, colorList);
    _actualViewIndex = index;
    if (index.row() == _history->moveList().size() - 1 && _playersInfo.at(_actualPlayerIndex).type() == "H") {
        _render->acceptMove(true);
    }
    else {
        _render->acceptMove(false);
    }
}

void MainWindow::undoTurn()
{
    int ply = _history->moveList().size();
    if (ply <= _playersInfo.size()) {
        return;
    }
    QModelIndex end = _history->index(ply - _playersInfo.size() - 1, 0);
    undoMoves(end);
}

void MainWindow::undoMoves(QModelIndex index)
{
    if (!_isStandAloneGame) {
        return;
    }
    if (_playersInfo.at(_actualPlayerIndex).type() != "H") {
        QMessageBox::information(this, tr("Forbidden"), tr("You can't go back when it's not your turn."), QMessageBox::Ok);
        return;
    }
    if (_loop->isRunning()) {
        QMessageBox::StandardButton result = QMessageBox::question(this, tr("Are you sure?"), tr("Do you really want to bring the game in a past situation?"), QMessageBox::Yes | QMessageBox::No);
        if (result == QMessageBox::No) {
            return;
        }

        QList<Move> list = _history->moveList();
        if (index.row() == list.size() - 1) {
            return;
        }

        Q_ASSERT(_isStandAloneGame);

        showPastGameStatus(index);

        GameMatrix *mat = GameMatrix::instance();
        QList<Point> lastRound;
        for (int i = index.row(); i > (index.row() - _playersInfo.size()) && i >= 0; i--) {
            lastRound.append(list.at(i).point());
        }
        while (lastRound.size() < _playersInfo.size()) {
            lastRound.append(Point());
        }
        mat->setLastRound(lastRound);
        for (int i = index.row() + 1; i < list.size(); i++) {
            mat->clear(list.at(i).point());
        }
        int ply = list.size() - index.row() - 1;
        _history->remove(ply);
        _loop->setTurn(ply);
        // wake up GameLoop
        SyncSharedCondition::instance()->lock();
        SyncSharedCondition::instance()->notifyMove(Point());
        SyncSharedCondition::instance()->unlock();
    }
}

void MainWindow::timerExpired()
{
    SyncSharedCondition::instance()->lock();
    SyncSharedCondition::instance()->notifyMove(Point());
    SyncSharedCondition::instance()->unlock();
}

void MainWindow::networkError(QString error)
{
    QMessageBox::critical(this, tr("Error"), tr("%1\nQuitting current game.").arg(error), QMessageBox::Ok);
    resetOnNewGame();
}

void MainWindow::saveGame()
{
    Q_ASSERT(_isStandAloneGame);
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Game"));
    if (fileName.isEmpty()) {
        return;
    }

    GUISettings set;

    QFile f(fileName);
    if (!f.open(QIODevice::WriteOnly)) {
        QMessageBox::critical(this, tr("Error"), tr("Cannot open selected file."), QMessageBox::Ok);
        return;
    }

    QXmlStreamWriter xml(&f);
    //xml.setAutoFormatting(true);
    xml.writeStartDocument();
    xml.writeDefaultNamespace("http://www.itworks.org/GoMoku3D/SavedGame");
    xml.writeStartElement("game");
    xml.writeStartElement("settings");
    xml.writeTextElement("difficultyOne", QString::number(GameMatrix::instance()->d1()));
    xml.writeTextElement("difficultyTwo", QString::number(GameMatrix::instance()->d2()));
    xml.writeStartElement("cubeColor");
    xml.writeTextElement("r", QString::number(set.defaultCubeColor().red()));
    xml.writeTextElement("g", QString::number(set.defaultCubeColor().green()));
    xml.writeTextElement("b", QString::number(set.defaultCubeColor().blue()));
    xml.writeTextElement("a", QString::number(set.defaultCubeColor().alpha()));
    xml.writeEndElement();
    xml.writeStartElement("backgroundColor");
    xml.writeTextElement("r", QString::number(set.backgroundColor().red()));
    xml.writeTextElement("g", QString::number(set.backgroundColor().green()));
    xml.writeTextElement("b", QString::number(set.backgroundColor().blue()));
    xml.writeTextElement("a", QString::number(set.backgroundColor().alpha()));
    xml.writeEndElement();
    xml.writeEndElement();
    xml.writeStartElement("players");
    for (int i = 0; i < _playersInfo.size(); i++) {
        xml.writeStartElement("player");
        xml.writeAttribute("id", QString::number(i));
        xml.writeTextElement("name", _playersInfo.at(i).name());
        xml.writeStartElement("color");
        xml.writeTextElement("r", QString::number(_playersInfo.at(i).color().red()));
        xml.writeTextElement("g", QString::number(_playersInfo.at(i).color().green()));
        xml.writeTextElement("b", QString::number(_playersInfo.at(i).color().blue()));
        xml.writeTextElement("a", QString::number(_playersInfo.at(i).color().alpha()));
        xml.writeEndElement();
        xml.writeTextElement("type", _playersInfo.at(i).type());
        xml.writeEndElement();
    }
    xml.writeEndElement();
    xml.writeStartElement("history");
    Move m;
    foreach (m, _history->moveList()) {
        xml.writeStartElement("move");
        xml.writeTextElement("player", QString::number(m.playerId()));
        xml.writeStartElement("point");
        xml.writeTextElement("x", QString::number(m.point().x()));
        xml.writeTextElement("y", QString::number(m.point().y()));
        xml.writeTextElement("z", QString::number(m.point().z()));
        xml.writeEndElement();
        xml.writeEndElement();
    }
    xml.writeEndElement();
    xml.writeEndElement();
    xml.writeEndDocument();
    f.close();
}

void MainWindow::loadGame()
{
    if (_loop != 0 && !askUserConfirmation()) {
        return;
    }

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Saved Game"));
    if (fileName.isEmpty()) {
        return;
    }

    QFile f(fileName);
    if (!f.open(QIODevice::ReadOnly)) {
        QMessageBox::critical(this, tr("Error"), tr("Cannot open selected file."), QMessageBox::Ok);
        return;
    }

    QXmlStreamReader xml(&f);
    bool success = xml.readNext() == QXmlStreamReader::StartDocument
                && xml.readNext() == QXmlStreamReader::StartElement
                && xml.name() == "game"
                && parseSettings(xml)
                && parsePlayers(xml)
                && parseHistory(xml)
                && xml.readNext() == QXmlStreamReader::EndElement
                && xml.readNext() == QXmlStreamReader::EndDocument;
    f.close();
    if (!success) {
        QMessageBox::critical(this, tr("Error"), tr("File is corrupted.\nCannot load game."), QMessageBox::Ok);
        qCritical() << "XML error while parsing file:" << xml.errorString() << "@" << xml.characterOffset();
        return;
    }

    int d1 = property("difficultyOne").toInt();
    int d2 = property("difficultyTwo").toInt();
    QColor cubeColor = property("cubeColor").value<QColor>();
    QColor backgroundColor = property("backgroundColor").value<QColor>();

    int numPlayers = property("players/size").toInt();
    QList<PlayerInfo> infos;
    for (int i = 0; i < numPlayers; i++) {
        QString name = property(qPrintable("player_" + QString::number(i) + "/name")).toString();
        QColor color = property(qPrintable("player_" + QString::number(i) + "/color")).value<QColor>();
        QString type = property(qPrintable("player_" + QString::number(i) + "/type")).toString();
        infos.append(PlayerInfo(name, color, type));
    }

    int historySize = property("history/size").toInt();

    QList<Move> history;

    for (int i = 0; i < historySize; i++) {
        history.append(property(qPrintable("move_" + QString::number(i))).value<Move>());
    }

    // consistency check
    bool b1 = d1 == 5 || d1 == 7 || d1 == 9;
    bool b2 = d2 > 0 && d2 < 4;
    bool b3 = numPlayers == 2 || numPlayers == 3;
    bool b4 = infos.size() == numPlayers;
    bool b5 = history.size() == historySize;
    bool b6 = true;
    GameMatrix::create(d1, d2, numPlayers);
    for (int i = 0; i < history.size() && b6; i++) {
        if (!history.at(i).point().isValid() || (history.at(i).playerId() != (i % numPlayers))) {
            b6 = false;
        }
    }
    bool b7 = true;
    QSet<Point> points;
    Move e;
    foreach (e, history) {
        Point p = e.point();
        if (points.find(p) != points.end()) {
            b7 = false;
        }
        points.insert(p);
    }

    resetOnNewGame();

    qobject_cast<QDockWidget*>(_timer->parent())->setVisible(false);
    qobject_cast<QDockWidget*>(_chat->parent())->setVisible(false);
    qobject_cast<QDockWidget*>(_playersWidget->parent())->setVisible(true);
    qobject_cast<QDockWidget*>(_historyView->parent())->setVisible(true);

    if(!(b1 && b2 && b3 && b4 && b5 && b6 && b7)) {
        delete GameMatrix::instance();
        QMessageBox::critical(this, tr("Error"), tr("Saved game file is corrupted.\nCannot load game."), QMessageBox::Ok);
        return;
    }
    QMessageBox::StandardButton result = QMessageBox::question(this,
            tr("Load Settings?"),
               tr("Load color settings from the saved game?"),
                  QMessageBox::Yes | QMessageBox::No);

    if (result == QMessageBox::Yes) {
        GUISettings set;
        set.setBackgroundColor(backgroundColor);
        set.setDefaultCubeColor(cubeColor);
    }
    else {
        LocalSettings set;
        QList<PlayerInfo> list = set.playersInfo();
        QList<PlayerInfo> support;
        for (int i = 0; i < infos.size(); i++) {
            support.append(PlayerInfo(infos.at(i).name(), list.at(i).color(), infos.at(i).type()));
        }
        infos = support;
    }

    _isStandAloneGame = true;

    // abilito alcune voci del menu
    actionLoad_game->setEnabled(true);
    actionSave_game->setEnabled(true);

    _render->init(d1 * d2);

    _playersInfo = infos;
    _playersWidget->set(&_playersInfo);

    QList<Player*> players;
    for (int i = 0; i < numPlayers; i++) {
        if (infos.at(i).type() == "H") {
            players.append(new HumanPlayer(i, _render));
        }
        if (infos.at(i).type() == "A") {
            players.append(new AIPlayer(i));
        }
    }
    connectAISkillSlider(players);

    _loop = new GameLoop(players);
    if (history.size() != 0) {
        Move m;
        GameMatrix *mat = GameMatrix::instance();
        QList<bool> win;
        for (int i = 0; i < _playersInfo.size(); i++) {
            win.append(false);
        }
        foreach (m, history) {
            win[m.playerId()] = mat->add(m);
            _render->drawMove(m.point(), _playersInfo.at(m.playerId()).color());
            _history->append(m);
        }
        _actualViewIndex = _history->index(history.size() - 1, 0);
        _loop->winStatus(win);
        _loop->onLoadSetTurn(history.size() % _playersInfo.size());
    }

    Player *p;
    foreach (p, players) {
        AIPlayer *ai = qobject_cast<AIPlayer*>(p);
        if (ai != 0) {
            ai->undoMove();
        }
        p->moveToThread(_loop);
    }

    connect(actionForce_Move, SIGNAL(triggered()), _loop, SLOT(forceMove()));
    connect(_loop, SIGNAL(turn(int)), this, SLOT(turn(int)));
    connect(_loop, SIGNAL(turn(int)), _playersWidget, SLOT(updateTurn(int)));
    connect(_loop, SIGNAL(moved(Move)), this, SLOT(drawMove(Move)));
    connect(_loop, SIGNAL(win(int)), this, SLOT(playerWin(int)));
    connect(_loop, SIGNAL(draw(QList<int>)), this, SLOT(playersDraw(QList<int>)));

    _loop->start();
}

bool MainWindow::parseSettings(QXmlStreamReader &xml)
{
    if (xml.atEnd()) {
        return false;
    }
    return xml.readNext() == QXmlStreamReader::StartElement
        && xml.name() == "settings"
        && parseInt(xml, "difficultyOne")
        && parseInt(xml, "difficultyTwo")
        && parseColor(xml, "cubeColor")
        && parseColor(xml, "backgroundColor")
        && xml.readNext() == QXmlStreamReader::EndElement;
}

bool MainWindow::parsePlayers(QXmlStreamReader &xml)
{
    if (xml.atEnd()) {
        return false;
    }
    if (xml.readNext() != QXmlStreamReader::StartElement || xml.name() != "players") {
        return false;
    }
    int i = 0;
    while (!xml.atEnd()) {
        QXmlStreamReader::TokenType token = xml.readNext();
        if (token == QXmlStreamReader::StartElement && xml.name() == "player") {
            if (!parsePlayer(xml, i)) {
                return false;
            }
            i++;
        } else if (token == QXmlStreamReader::EndElement && (i == 2 || i == 3)) {
            break;
        } else {
            return false;
        }
    }
    if (xml.atEnd()) {
        return false;
    }
    setProperty("players/size", QVariant(i));
    return true;
}

bool MainWindow::parsePlayer(QXmlStreamReader &xml, int n)
{
    if (xml.atEnd()) {
        return false;
    }
    bool success = parseString(xml, "name")
                && parseColor(xml, "color")
                && parseString(xml, "type")
                && xml.readNext() == QXmlStreamReader::EndElement;
    if (success) {
        setProperty(qPrintable("player_" + QString::number(n) + "/name"), property("name"));
        setProperty(qPrintable("player_" + QString::number(n) + "/color"), property("color"));
        setProperty(qPrintable("player_" + QString::number(n) + "/type"), property("type"));
    }
    return success;
}

bool MainWindow::parseHistory(QXmlStreamReader &xml)
{
    if (xml.atEnd()) {
        return false;
    }
    if (xml.readNext() != QXmlStreamReader::StartElement || xml.name() != "history") {
        return false;
    }
    int i = 0;
    while (!xml.atEnd()) {
        QXmlStreamReader::TokenType token = xml.readNext();
        if (token == QXmlStreamReader::StartElement && xml.name() == "move") {
            if (!parseMove(xml, i)) {
                return false;
            }
            i++;
        } else if (token == QXmlStreamReader::EndElement) {
            break;
        } else {
            return false;
        }
    }
    if (xml.atEnd()) {
        return false;
    }
    setProperty("history/size", QVariant(i));
    return true;
}

bool MainWindow::parseMove(QXmlStreamReader &xml, int n)
{
    if (xml.atEnd()) {
        return false;
    }
    bool success = parseInt(xml, "player")
                && parsePoint(xml)
                && xml.readNext() == QXmlStreamReader::EndElement;
    if (success) {
        Move m(property("player").toInt(), property("point").value<Point>());
        setProperty(qPrintable("move_" + QString::number(n)), QVariant::fromValue(m));
    }
    return success;
}

bool MainWindow::parsePoint(QXmlStreamReader &xml)
{
    if (xml.atEnd()) {
        return false;
    }
    bool success = xml.readNext() == QXmlStreamReader::StartElement
                && xml.name() == "point"
                && parseInt(xml, "x")
                && parseInt(xml, "y")
                && parseInt(xml, "z")
                && xml.readNext() == QXmlStreamReader::EndElement;
    if (success) {
        Point p(property("my_x").toInt(), property("my_y").toInt(), property("my_z").toInt());
        setProperty("point", QVariant::fromValue(p));
    }
    return success;
}

bool MainWindow::parseInt(QXmlStreamReader &xml, QString elementName)
{
    if (xml.atEnd()) {
        return false;
    }
    if (xml.readNext() != QXmlStreamReader::StartElement || xml.name() != elementName) {
        return false;
    }
    bool ok;
    int x = xml.readElementText().toInt(&ok);
    if (!ok) {
        return false;
    }
    if (elementName.size() == 1) {
        elementName = "my_" + elementName;
    }
    setProperty(qPrintable(elementName), QVariant(x));
    return (xml.tokenType() == QXmlStreamReader::EndElement);
}

bool MainWindow::parseString(QXmlStreamReader &xml, QString elementName)
{
    if (xml.atEnd()) {
        return false;
    }
    if (xml.readNext() != QXmlStreamReader::StartElement || xml.name() != elementName) {
        return false;
    }
    setProperty(qPrintable(elementName), QVariant(xml.readElementText()));
    return (xml.tokenType() == QXmlStreamReader::EndElement);
}

bool MainWindow::parseColor(QXmlStreamReader &xml, QString elementName)
{
    if (xml.atEnd()) {
        return false;
    }
    bool success = xml.readNext() == QXmlStreamReader::StartElement
                && xml.name() == elementName
                && parseInt(xml, "r")
                && parseInt(xml, "g")
                && parseInt(xml, "b")
                && parseInt(xml, "a")
                && xml.readNext() == QXmlStreamReader::EndElement;
    if (success) {
        QColor c(property("my_r").toInt(), property("my_g").toInt(), property("my_b").toInt(), property("my_a").toInt());
        setProperty(qPrintable(elementName), QVariant(c));
    }
    return success;
}

void MainWindow::showAbout()
{
    AboutDialog dialog(this);
    dialog.exec();
}

bool MainWindow::areDifferentColors(QColor c1, QColor c2)
{
    double bound = 150.0;
    int dRed = c1.red() - c2.red();
    int dGreen = c1.green() - c2.green();
    int dBlue = c1.blue() - c2.blue();
    double diff = sqrt((float)(dRed * dRed + dGreen * dGreen + dBlue * dBlue));
    bool ok = false;
    if (diff > bound) {
        ok = true;
    }
    return ok;
}
