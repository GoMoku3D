/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "HistoryModel.h"

HistoryModel::HistoryModel(QObject *parent, QList<PlayerInfo> *info) :
        QAbstractTableModel(parent)
{
    _info = info;
}

void HistoryModel::reset()
{
    _history.clear();
    QAbstractTableModel::reset();
}

int HistoryModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 2;
}

int HistoryModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return _history.size();
}

QVariant HistoryModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole && index.isValid() && index.row() < _history.size()) {
        if (index.column() == 0) {
            return QVariant(_info->at(_history.at(index.row()).playerId()).name());
        }
        if (index.column() == 1) {
            return QVariant(_history.at(index.row()).point().toString());
        }
    }
    return QVariant();
}

QVariant HistoryModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole && section == 0) {
            return QVariant(tr("Player"));
        }
        if (role == Qt::DisplayRole && section == 1) {
            return  QVariant(tr("Move"));
        }
    } else {
        if (role == Qt::DisplayRole) {
            return QVariant(QString::number(section + 1));
        }
    }
    return QVariant();
}

Qt::ItemFlags HistoryModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index)
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

bool HistoryModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginInsertRows(QModelIndex(), row, row + count - 1);
    endInsertRows();
    return true;
}

bool HistoryModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginRemoveRows(QModelIndex(), row, row + count - 1);
    endRemoveRows();
    return true;
}

void HistoryModel::append(Move move)
{
    insertRows(rowCount(), 1);
    _history.append(move);
    emit dataChanged(createIndex(rowCount() - 1, 0), createIndex(rowCount() - 1, 1));
}

QList<Move> HistoryModel::moveList() const
{
    return _history;
}

void HistoryModel::remove(int count)
{
    removeRows(rowCount() - count, count);
    for (int i = 0; i < count; i++) {
        _history.removeLast();
    }
    emit dataChanged(createIndex(rowCount(), 0), createIndex(rowCount() + count - 1, 1));
}
