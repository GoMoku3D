/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QPalette>

#include "PlayersWidget.h"
#include "MainWindow.h"

PlayersWidget::PlayersWidget(QWidget *parent) :
        QWidget(parent), Ui_playerswidget(*this)
{
    setupUi(this);
    reset();
    color_0->setClickable(false);
    color_1->setClickable(false);
    color_2->setClickable(false);
}

void PlayersWidget::updateTurn(int playerId)
{
    arrow_0->setEnabled(false);
    arrow_1->setEnabled(false);
    arrow_2->setEnabled(false);
    switch(playerId) {
        case 0 :
            arrow_0->setEnabled(true);
            break;
        case 1 :
            arrow_1->setEnabled(true);
            break;
        case 2 :
            arrow_2->setEnabled(true);
            break;
    }
}

void PlayersWidget::reset()
{
    arrow_0->setEnabled(false);
    arrow_1->setEnabled(false);
    arrow_2->setEnabled(false);
    skill_0->setVisible(false);
    skill_0->setValue(2);
    skill_1->setVisible(false);
    skill_1->setValue(2);
    skill_2->setVisible(false);
    skill_2->setValue(2);
    color_0->setColor(QPalette().color(QPalette::Window));
    color_1->setColor(QPalette().color(QPalette::Window));
    color_2->setColor(QPalette().color(QPalette::Window));
    namelabel_0->setText(tr("Player 0"));
    namelabel_1->setText(tr("Player 1"));
    namelabel_2->setText(tr("Player 2"));
    naturelabel_0->setText("H");
    naturelabel_1->setText("H");
    naturelabel_2->setText("H");
    frame_2->setVisible(true);
}

void PlayersWidget::set(QList<PlayerInfo> *info)
{
    color_0->setColor(info->at(0).color());
    color_1->setColor(info->at(1).color());
    namelabel_0->setText(info->at(0).name());
    namelabel_1->setText(info->at(1).name());
    naturelabel_0->setText(info->at(0).type());
    if (naturelabel_0->text() == "A" && !qobject_cast<MainWindow*>(parent()->parent())->isOnlineGame()) {
        skill_0->setVisible(true);
    }
    else {
        skill_0->setVisible(false);
    }

    naturelabel_1->setText(info->at(1).type());
    if (naturelabel_1->text() == "A" && !qobject_cast<MainWindow*>(parent()->parent())->isOnlineGame()) {
        skill_1->setVisible(true);
    }
    else {
        skill_1->setVisible(false);
    }

    if (info->size() == 3) {
        frame_2->setVisible(true);
        color_2->setColor(info->at(2).color());
        namelabel_2->setText(info->at(2).name());
        naturelabel_2->setText(info->at(2).type());
        if (naturelabel_2->text() == "A" && !qobject_cast<MainWindow*>(parent()->parent())->isOnlineGame()) {
            skill_2->setVisible(true);
        }
        else {
            skill_2->setVisible(false);
        }
    }
    else {
        frame_2->setVisible(false);
    }
}
