/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef ONLINEDIALOG_H
#define ONLINEDIALOG_H

#include <QDialog>

#include "ui_OnlineDialog.h"

class OnlineDialog : public QDialog, private Ui_OnlineDialog
{
    Q_OBJECT

    friend class MainWindow;

    public:
        OnlineDialog(QWidget *parent);
        QList<QColor> colorList() const;

    private:
        QVector<QString> _typeList;
        bool _alreadyJoined;

    public slots:
        void join();
        void setStatus(QString status);
        void addPlayer(int id, QString name, QString type);
        void removePlayer(int id);
        void displaySettings(int d1, int d2, int numPlayers, int timer, bool playing);
        void acceptedJoinRequest(int id);
        void refusedJoinRequest(QString error);
        void gameStarted();
        void networkError(QString error);

    private slots:
        void connectToServer();
        void enableHelp();

    signals:
        void sendJoin(QString mode, QString name);
};

#endif

