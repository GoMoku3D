/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QColorDialog>
#include <QWhatsThis>
#include <QMessageBox>

#include "Preferences.h"
#include "GUISettings.h"
#include "MainWindow.h"

Preferences::Preferences(QWidget *parent) : QDialog(parent), Ui_preferences(*this)
{
    setupUi(this);

    GUISettings set;
    int index = 0;
    if (set.language() == "it") {
        index = 1;
    }

    //inizializzo con i valori salvati nei settings
    languageBox->setCurrentIndex(index);
    _bgColor = set.backgroundColor();
    _dcColor = set.defaultCubeColor();
    backgroundColor->setColor(set.backgroundColor());
    cubeColor->setColor(set.defaultCubeColor());
    aiDelaySpinBox->setValue(set.aiDelay());

    connect(helpButton, SIGNAL(clicked()), this, SLOT(enableHelp()));
}

void Preferences::accept()
{
    GUISettings settings;
    bool ok = MainWindow::areDifferentColors(_bgColor, _dcColor);

    _bgColor = backgroundColor->color();
    _dcColor = cubeColor->color();

    if (!ok) {
        QMessageBox::warning(this, tr("Warning"), tr("Background color and cube color are too similar."), QMessageBox::Ok);
        return;
    }
    // si suppone:
    //   0: english
    //   1: italian
    QString lang;
    if (languageBox->currentIndex() == 0) {
        lang = "en";
    } else if (languageBox->currentIndex() == 1) {
        lang = "it";
    }

    settings.setLanguage(lang);
    settings.setBackgroundColor(backgroundColor->color());
    settings.setDefaultCubeColor(cubeColor->color());
    settings.setAIDelay(aiDelaySpinBox->value());

    QDialog::accept();
}

void Preferences::enableHelp()
{
    QWhatsThis::enterWhatsThisMode();
}
