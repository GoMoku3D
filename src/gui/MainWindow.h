/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QModelIndex>
#include <QList>
#include <QTableView>
#include <QDockWidget>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "ui_MainWindow.h"
#include "Move.h"
#include "PlayerInfo.h"
#include "Player.h"

class GameLoop;
class Network;
class SettingsManager;
class Suggester;
class RenderWidget;
class ChatWidget;
class TimerWidget;
class HistoryModel;
class PlayersWidget;

class MainWindow : public QMainWindow, private Ui_MainWindow
{
    Q_OBJECT

    friend class OnlineDialog;
    friend class ServerSettingsDialog;

public:
    MainWindow();
    ~MainWindow();
    QModelIndex actualViewIndex() const;
    bool isOnlineGame() const;
    static bool areDifferentColors(QColor c1, QColor c2);

public slots:
    void loadGame();
    void saveGame();
    void turn(int playerId);
    void drawMove(Move move);
    void undoMoves(QModelIndex index);
    void showPastGameStatus(QModelIndex index);
    void newStandAloneGame();
    void newOnlineGame();
    void newServerGame();
    void playerWin(int playerId);
    void playersDraw(QList<int> playersIds);
    void suggestMove();
    void undoTurn();
    void updateHistory(QList<Move> list);

private slots:
    void showPreferences();
    void showAbout();
    void showLastMove();
    void timerExpired();
    void networkError(QString error);

private:
    GameLoop *_loop;
    Network *_net;
    QList<PlayerInfo> _playersInfo;
    Suggester *_suggester;
    RenderWidget *_render;
    ChatWidget *_chat;
    HistoryModel *_history;
    QTableView *_historyView;
    TimerWidget *_timer;
    PlayersWidget *_playersWidget;
    QModelIndex _actualViewIndex;
    QList<Move> _buffer;
    int _actualPlayerIndex;
    int _timerDuration;
    bool _isStandAloneGame;
    bool _isOnlineGame;
    bool _isServerGame;
    bool _isDedicatedServer;

    bool askUserConfirmation();
    void disableOnEndGame();
    void resetOnNewGame();
    void connectAISkillSlider(const QList<Player*> &players);
    bool parseSettings(QXmlStreamReader &xml);
    bool parsePlayers(QXmlStreamReader &xml);
    bool parsePlayer(QXmlStreamReader &xml, int n);
    bool parseHistory(QXmlStreamReader &xml);
    bool parseMove(QXmlStreamReader &xml, int n);
    bool parsePoint(QXmlStreamReader &xml);
    bool parseInt(QXmlStreamReader &xml, QString elementName);
    bool parseString(QXmlStreamReader &xml, QString elementName);
    bool parseColor(QXmlStreamReader &xml, QString elementName);

signals:
    void forceMove();
};

#endif
