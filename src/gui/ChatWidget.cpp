/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "ChatWidget.h"

ChatWidget::ChatWidget(QWidget *parent) :
        QWidget(parent), Ui_chatwidget(*this)
{
    setupUi(this);
    enableEditing(true);

    setWhatsThis(tr("Chat.\n"
                    "Here you can send/receive messages to/from all players in this game.\n"
                    "To send a new message, write the text, then press ENTER."));

    QObject::connect(lineEdit, SIGNAL(returnPressed()), this, SLOT(returnEntered()));
}

void ChatWidget::enableEditing(bool state)
{
    lineEdit->setEnabled(state);
}

void ChatWidget::reset()
{
    textBrowser->clear();
    lineEdit->clear();
}

void ChatWidget::displayMessage(QString sender, QString message)
{
    textBrowser->append("<" + sender + "> " + message);
}

void ChatWidget::returnEntered()
{
    QString text = lineEdit->text();
    lineEdit->clear();
    if (!text.isEmpty()) {
        emit textEntered(QString(), text);
    }
}
