/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef SERVERSETTINGSDIALOG_H
#define SERVERSETTINGSDIALOG_H

#include <QDialog>

#include "ui_ServerSettingsDialog.h"

class ServerSettingsDialog : public QDialog, private Ui_ServerSettingsDialog
{
    Q_OBJECT

    friend class MainWindow;

    public:
        ServerSettingsDialog(QWidget *parent);
        QList<QColor> colorList() const;

    private:
        bool _alreadyJoined;
        void loadSettings();

    public slots:
        virtual void accept();
        void addPlayer(int id, QString name, QString type);
        void removePlayer(int id);
        void gameStarted();
        void networkError(QString error);

    private slots:
        void playersChanged(int number);
        void enableHelp();
};

#endif
