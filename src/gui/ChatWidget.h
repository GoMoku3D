/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef CHATWIDGET_H
#define CHATWIDGET_H

#include <QWidget>
#include <QString>

#include "ui_ChatWidget.h"

class ChatWidget : public QWidget, private Ui_chatwidget
{
    Q_OBJECT

    public:
        ChatWidget(QWidget *parent);
        void enableEditing(bool state);
        void reset();

    public slots:
        void displayMessage(QString sender, QString message);

    private slots:
        void returnEntered();

    signals:
        void textEntered(QString self, QString text);
};

#endif
