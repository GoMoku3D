/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef HISTORYMODEL_H
#define HISTORYMODEL_H

#include <QAbstractTableModel>
#include <QList>

#include "Move.h"
#include "PlayerInfo.h"

class HistoryModel : public QAbstractTableModel
{
    Q_OBJECT

    public:
        HistoryModel(QObject *parent, QList<PlayerInfo> *info);
        virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
        virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
        virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
        virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
        virtual Qt::ItemFlags flags(const QModelIndex &index) const;
        virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
        virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
        void append(Move move);
        QList<Move> moveList() const;
        void remove(int count);
        void reset();

    private:
        QList<Move> _history;
        QList<PlayerInfo> *_info;

};

#endif
