/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "ColorLabel.h"

ColorLabel::ColorLabel(QWidget *parent) : QLabel(parent)
{
    _clickable = true;
}

void ColorLabel::setClickable(bool b)
{
    _clickable = b;
}

void ColorLabel::setColor(QColor color)
{
    _color = color;
    QPixmap pix(1, 1);
    pix.fill(color);
    setPixmap(pix);
    setScaledContents(true);
}

QColor ColorLabel::color() const
{
    return _color;
}

void ColorLabel::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
}

void ColorLabel::mousePressEvent(QMouseEvent *event)
{
    if (event->button() != Qt::LeftButton || !_clickable) {
        return;
    }

    bool ok;
    QRgb chosenColor = QColorDialog::getRgba(_color.rgba(), &ok, this);
    if (ok) {
        setColor(QColor::fromRgba(chosenColor));
    }
}
