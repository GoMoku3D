/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QColorDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QWhatsThis>

#include "ServerSettingsDialog.h"
#include "ServerSettings.h"
#include "MainWindow.h"
#include "GameServer.h"

ServerSettingsDialog::ServerSettingsDialog(QWidget *parent)
    : QDialog(parent)
    , Ui_ServerSettingsDialog(*this)
{
    setupUi(this);

    loadSettings();
    connect(numPlayersBox, SIGNAL(currentIndexChanged(int)), this, SLOT(playersChanged(int)));
    connect(helpButton, SIGNAL(clicked()), this, SLOT(enableHelp()));
}

void ServerSettingsDialog::loadSettings()
{
    ServerSettings set;

    _alreadyJoined = false;

    d2Box->setCurrentIndex(set.difficulty2() - 1);
    d1Box->setCurrentIndex((set.difficulty1() - 5) / 2);

    QList<PlayerInfo> info = set.playersInfo();
    numPlayersBox->setCurrentIndex(set.numberOfPlayers() - 2);

    playername_0->setText(info[0].name());
    playername_1->setText(info[1].name());

    color_0->setColor(info[0].color());
    color_1->setColor(info[1].color());
    color_0->setClickable(true);
    color_1->setClickable(true);

    if (info[0].type() == "H") {
        human_0->click();
    }
    else {
        if (info[0].type() == "A") {
            ai_0->click();
        }
        else {
            remote_0->click();
        }
    }

    if (info[1].type() == "H") {
        human_1->click();
    }
    else {
        if (info[1].type() == "A") {
            ai_1->click();
        }
        else {
            remote_1->click();
        }
    }


    if (set.numberOfPlayers() == 3) {
        frame_3->setEnabled(true);
        playername_2->setText(info[2].name());
        color_2->setColor(info[2].color());
        color_2->setEnabled(true);
        color_2->setClickable(true);
        if (info[2].type() == "H") {
            human_2->click();
        }
        else {
            if (info[2].type() == "A") {
                ai_2->click();
            }
            else {
                remote_2->click();
            }
        }
    }
    else {
        frame_3->setEnabled(false);
    }

    if (set.timerDuration() != 0) {
        timerCheckBox->click();
        timerSpinBox->setValue(set.timerDuration());
    }

    myname->setText(set.myName());
    serverPortLineEdit->setText(QString::number(set.serverPort()));
}

QList<QColor> ServerSettingsDialog::colorList() const
{
    QList<QColor> list;
    list.append(color_0->color());
    list.append(color_1->color());
    list.append(color_2->color());
    return list;
}

void ServerSettingsDialog::accept()
{
    quint16 serverPort = serverPortLineEdit->text().toUShort();
    if (serverPort <= 1024) {
        QMessageBox::warning(this, tr("Warning"), tr("Invalid port number."), QMessageBox::Ok);
        return;
    }

    QString p0Name = playername_0->text();
    QString p1Name = playername_1->text();
    QString p2Name = playername_2->text();
    bool b0 = p0Name == "" && !remote_0->isChecked();
    bool b1 = p1Name == "" && !remote_1->isChecked();
    bool b2 = p2Name == "" && !remote_2->isChecked();
    if (b0 || b1 || (numPlayersBox->currentText().toInt() == 3 && b2)) {
        QMessageBox::warning(this, tr("Warning"), tr("Human and AI players cannot have empty name"), QMessageBox::Ok);
        return;
    }
    if (myname->isEnabled() && myname->text() == "") {
        QMessageBox::warning(this, tr("Warning"), tr("You cannot have an empty name"), QMessageBox::Ok);
        return;
    }

    ServerSettings set;

    QList<QColor> colors;
    colors.append(set.backgroundColor());
    colors.append(set.defaultCubeColor());
    colors.append(color_0->color());
    colors.append(color_1->color());
    if (numPlayersBox->currentText().toInt() == 3) {
        colors.append(color_2->color());
    }
    for (int i = 0; i < colors.size(); i++) {
        for (int j = 0; j < i; j++) {
            bool ok = MainWindow::areDifferentColors(colors.at(j), colors.at(i));
            if (!ok) {
                if (j == 0 || j == 1) {
                    QMessageBox::warning(this, tr("Warning"), tr("Players' colors are too similar\nto background color or empty cube color."), QMessageBox::Ok);
                }
                else {
                    QMessageBox::warning(this, tr("Warning"), tr("Players' colors are too similar."), QMessageBox::Ok);
                }
                return;
            }
        }
    }

    set.setDifficulty1(d1Box->currentText().toInt());
    set.setDifficulty2(d2Box->currentText().toInt());

    QString p0Type;
    QString p1Type;

    if (human_0->isChecked()) {
        p0Type = "H";
    }
    else {
        if (ai_0->isChecked()) {
            p0Type = "A";
        }
        else {
            p0Type = "R";
            playername_0->setText("");
        }
    }
    if (human_1->isChecked()) {
        p1Type = "H";
    }
    else {
        if (ai_1->isChecked()) {
            p1Type = "A";
        }
        else {
            p1Type = "R";
            playername_1->setText("");
        }
    }
    QList<PlayerInfo> info;
    info.append(PlayerInfo(p0Name, color_0->color(), p0Type));
    info.append(PlayerInfo(p1Name, color_1->color(), p1Type));
    if (numPlayersBox->currentText().toInt() == 3) {
        QString p2Type;
        if (human_2->isChecked()) {
            p2Type = "H";
        }
        else {
            if (ai_2->isChecked()) {
                p2Type = "A";
            }
            else {
                p2Type = "R";
                playername_2->setText("");
            }
        }
        info.append(PlayerInfo(p2Name, color_2->color(), p2Type));
    }
    set.setPlayersInfo(info);
    set.setNumberOfPlayers(info.size());
    if (timerCheckBox->isChecked()) {
        set.setTimerDuration(timerSpinBox->value());
    }
    else {
        set.setTimerDuration(0);
    }
    uint port = serverPortLineEdit->text().toUInt();
    if (port != 0) {
        set.setServerPort(port);
    }
    if (spectator->isEnabled() && spectator->isChecked()) {
        set.setMyName(myname->text());
    }

    //disabilito più o meno tutto
    color_0->setClickable(false);
    color_1->setClickable(false);
    color_2->setClickable(false);
    playername_0->setEnabled(false);
    playername_1->setEnabled(false);
    playername_2->setEnabled(false);
    human_0->setEnabled(false);
    human_1->setEnabled(false);
    human_2->setEnabled(false);
    remote_0->setEnabled(false);
    remote_1->setEnabled(false);
    remote_2->setEnabled(false);
    ai_0->setEnabled(false);
    ai_1->setEnabled(false);
    ai_2->setEnabled(false);
    numPlayersBox->setEnabled(false);
    d1Box->setEnabled(false);
    d2Box->setEnabled(false);
    timerCheckBox->setEnabled(false);
    timerSpinBox->setEnabled(false);
    spectator->setEnabled(false);
    myname->setEnabled(false);
    dedicatedserver->setEnabled(false);
    serverPortLineEdit->setEnabled(false);
    buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    waitingLabel->setText(RED_TEXT(tr("Waiting for players to join ...")));

    MainWindow *mainwin = qobject_cast<MainWindow*>(parent());
    if (mainwin->_net) {
        mainwin->_net->deleteLater();
        QApplication::processEvents();
        mainwin->_net = 0;
    }
    mainwin->_net = new GameServer(this, mainwin->_history);
}

void ServerSettingsDialog::playersChanged(int number)
{
    remote_2->click();
    if (number == 0) {
        frame_3->setEnabled(false);
    }
    else {
        ServerSettings set;
        QList<PlayerInfo> info = set.playersInfo();
        frame_3->setEnabled(true);
        playername_2->setText(info[2].name());
        color_2->setColor(info[2].color());
        color_2->setClickable(true);
        if (info[2].type() == "H") {
            remote_2->click();
        }
        if (info[2].type() == "A") {
            ai_2->click();
        }
    }
}

void ServerSettingsDialog::removePlayer(int id)
{
    if (id == 0) {
        playername_0->clear();
    }
    if (id == 1) {
        playername_1->clear();
    }
    if (id == 2) {
        playername_2->clear();
    }
}

void ServerSettingsDialog::addPlayer(int id, QString name, QString type)
{
    if (id == 0) {
        playername_0->setText(name);
        if (type == "A") {
            remote_0->setChecked(false);
            ai_0->setChecked(true);
        }
        else {
            remote_0->setChecked(true);
            ai_0->setChecked(false);
        }
    }
    if (id == 1) {
        playername_1->setText(name);
        if (type == "A") {
            remote_1->setChecked(false);
            ai_1->setChecked(true);
        }
        else {
            remote_1->setChecked(true);
            ai_1->setChecked(false);
        }
    }
    if (id == 2) {
        playername_2->setText(name);
        if (type == "A") {
            remote_2->setChecked(false);
            ai_2->setChecked(true);
        }
        else {
            remote_2->setChecked(true);
            ai_2->setChecked(false);
        }
    }
}

void ServerSettingsDialog::gameStarted()
{
    QDialog::accept();
}

void ServerSettingsDialog::networkError(QString error)
{
    MainWindow *mainwin = qobject_cast<MainWindow*>(parent());
    QMessageBox::critical(this, tr("Error"), error, QMessageBox::Ok);
    if (mainwin->_net) {
        mainwin->_net->deleteLater();
        QApplication::processEvents();
        mainwin->_net = 0;
    }

    color_0->setClickable(true);
    color_1->setClickable(true);
    color_2->setClickable(true);
    playername_0->setEnabled(true);
    playername_1->setEnabled(true);
    playername_2->setEnabled(true);
    human_0->setEnabled(true);
    human_1->setEnabled(true);
    human_2->setEnabled(true);
    remote_0->setEnabled(true);
    remote_1->setEnabled(true);
    remote_2->setEnabled(true);
    ai_0->setEnabled(true);
    ai_1->setEnabled(true);
    ai_2->setEnabled(true);
    numPlayersBox->setEnabled(true);
    d1Box->setEnabled(true);
    d2Box->setEnabled(true);
    timerCheckBox->setEnabled(true);
    timerCheckBox->setChecked(false);
    timerSpinBox->setEnabled(true);
    spectator->setEnabled(false);
    myname->setEnabled(false);
    dedicatedserver->setEnabled(false);
    serverPortLineEdit->setEnabled(true);
    buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);
    waitingLabel->setText("");

    loadSettings();
}

void ServerSettingsDialog::enableHelp()
{
    QWhatsThis::enterWhatsThisMode();
}
