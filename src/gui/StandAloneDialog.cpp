/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QList>
#include <QColorDialog>
#include <QMessageBox>
#include <QWhatsThis>

#include "StandAloneDialog.h"
#include "LocalSettings.h"
#include "PlayerInfo.h"
#include "MainWindow.h"

StandAloneDialog::StandAloneDialog(QWidget *parent) :
        QDialog(parent), Ui_StandAloneDialog(*this)
{
    setupUi(this);

    LocalSettings set;
    d2Box->setCurrentIndex(set.difficulty2() - 1);
    d1Box->setCurrentIndex((set.difficulty1() - 5) / 2);

    QList<PlayerInfo> info = set.playersInfo();
    numPlayersBox->setCurrentIndex(set.numberOfPlayers() - 2);

    playername_0->setText(info.at(0).name());
    playername_1->setText(info.at(1).name());

    color_0->setColor(info.at(0).color());
    color_1->setColor(info.at(1).color());

    if (info.at(0).type() == "H") {
        human_0->click();
    }
    else {
        ai_0->click();
    }
    if (info.at(1).type() == "H") {
        human_1->click();
    }
    else {
        ai_1->click();
    }

    if (set.numberOfPlayers() == 3) {
        frame_3->setEnabled(true);
        human_2->setEnabled(true);
        playername_2->setEnabled(true);
        playername_2->setText(info.at(2).name());
        color_2->setEnabled(true);
        color_2->setColor(info.at(2).color());
        if (info.at(2).type() == "H") {
            human_2->click();
        }
        else {
            ai_2->click();
        }
    }

    connect(numPlayersBox, SIGNAL(currentIndexChanged(int)), this, SLOT(playersChanged(int)));
    connect(helpButton, SIGNAL(clicked()), this, SLOT(enableHelp()));
}

void StandAloneDialog::accept()
{
    QString p0Name = playername_0->text();
    QString p1Name = playername_1->text();
    QString p2Name = playername_2->text();
    if (p0Name == "" || p1Name == "" || (frame_3->isEnabled() && p2Name == "")) {
        QMessageBox::warning(this, tr("Warning"), tr("Players cannot have empty name"), QMessageBox::Ok);
        return;
    }

    LocalSettings set;
    QList<QColor> colors;
    colors.append(set.backgroundColor());
    colors.append(set.defaultCubeColor());
    colors.append(color_0->color());
    colors.append(color_1->color());
    if (numPlayersBox->currentText().toInt() == 3) {
        colors.append(color_2->color());
    }
    for (int i = 0; i < colors.size(); i++) {
        for (int j = 0; j < i; j++) {
            bool ok = MainWindow::areDifferentColors(colors.at(j), colors.at(i));
            if (!ok) {
                if (j == 0 || j == 1) {
                    QMessageBox::warning(this, tr("Warning"), tr("Players' colors are too similar\nto background color or empty cube color."), QMessageBox::Ok);
                }
                else {
                    QMessageBox::warning(this, tr("Warning"), tr("Players' colors are too similar."), QMessageBox::Ok);
                }
                return;
            }
        }
    }

    set.setDifficulty1(d1Box->currentText().toInt());
    set.setDifficulty2(d2Box->currentText().toInt());

    QString p0Type;
    QString p1Type;
    if (human_0->isChecked()) {
        p0Type = "H";
    }
    else {
        p0Type = "A";
    }
    if (human_1->isChecked()) {
        p1Type = "H";
    }
    else {
        p1Type = "A";
    }

    QList<PlayerInfo> info;
    info.append(PlayerInfo(p0Name, color_0->color(), p0Type));
    info.append(PlayerInfo(p1Name, color_1->color(), p1Type));

    if (numPlayersBox->currentText().toInt() == 3) {
        QString p2Type;
        if (human_2->isChecked()) {
            p2Type = "H";
        }
        else {
            p2Type = "A";
        }
        info.append(PlayerInfo(p2Name, color_2->color(), p2Type));
    }
    set.setPlayersInfo(info);
    set.setNumberOfPlayers(info.size());

    QDialog::accept();
}

void StandAloneDialog::playersChanged(int number)
{
    ai_2->click();
    if (number == 0) {
        frame_3->setEnabled(false);
    }
    else {
        LocalSettings set;
        QList<PlayerInfo> info = set.playersInfo();
        frame_3->setEnabled(true);
        human_2->setEnabled(true);
        playername_2->setEnabled(true);
        playername_2->setText(info.at(2).name());
        color_2->setEnabled(true);
        color_2->setColor(info.at(2).color());
        if (info.at(2).type() == "H") {
            human_2->click();
        }
    }
}

QList<QColor> StandAloneDialog::colorList() const
{
    QList<QColor> list;
    list.append(color_0->color());
    list.append(color_1->color());
    list.append(color_2->color());
    return list;
}

void StandAloneDialog::enableHelp()
{
    QWhatsThis::enterWhatsThisMode();
}
