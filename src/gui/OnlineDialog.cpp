/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QColorDialog>
#include <QMessageBox>
#include <QHostAddress>
#include <QWhatsThis>

#include "OnlineDialog.h"
#include "ServerSettings.h"
#include "LocalSettings.h"
#include "MainWindow.h"
#include "GameClient.h"

OnlineDialog::OnlineDialog(QWidget *parent)
    : QDialog(parent)
    , Ui_OnlineDialog(*this)
    , _typeList(3, "")
{
    setupUi(this);
    _alreadyJoined = false;
    ServerSettings set;
    portLineEdit->setText(QString::number(set.serverPort()));

    connect(connectButton, SIGNAL(clicked()), this, SLOT(connectToServer()));
    connect(playerJoinButton, SIGNAL(clicked()), this, SLOT(join()));
    connect(spectatorJoinButton, SIGNAL(clicked()), this, SLOT(join()));
    connect(helpButton, SIGNAL(clicked()), this, SLOT(enableHelp()));
}

void OnlineDialog::join()
{
    QObject *s = sender();
    if (!s) {
        return;
    }

    if (myNameLineEdit->text() == "") {
        QMessageBox::warning(this, tr("Warning"), tr("You cannot have an empty name"), QMessageBox::Ok);
        return;
    }

    ServerSettings set;

    QList<QColor> colors;
    colors.append(set.backgroundColor());
    colors.append(set.defaultCubeColor());
    if (s == playerJoinButton) {
        colors.append(myColor->color());
    }
    colors.append(color_0->color());
    colors.append(color_1->color());
    if (numPlayersLabel->text().toInt() == 3) {
        colors.append(color_2->color());
    }
    for (int i = 0; i < colors.size(); i++) {
        for (int j = 0; j < i; j++) {
            bool ok = MainWindow::areDifferentColors(colors.at(j), colors.at(i));
            if (!ok) {
                if (j == 0 || j == 1) {
                    QMessageBox::warning(this, tr("Warning"), tr("Players' colors are too similar\nto background color or empty cube color."), QMessageBox::Ok);
                }
                else {
                    QMessageBox::warning(this, tr("Warning"), tr("Players' colors are too similar."), QMessageBox::Ok);
                }
                return;
            }
        }
    }

    _alreadyJoined = true;
    myNameLineEdit->setEnabled(false);
    myColor->setClickable(false);
    color_0->setClickable(false);
    color_1->setClickable(false);
    color_2->setClickable(false);
    playerJoinButton->setEnabled(false);
    spectatorJoinButton->setEnabled(false);

    set.setMyName(myNameLineEdit->text());
    set.setMyColor(myColor->color());

    if (s == playerJoinButton) {
        emit sendJoin("player", myNameLineEdit->text());
    }

    if (s == spectatorJoinButton) {
        emit sendJoin("spectator", myNameLineEdit->text());
    }
}

void OnlineDialog::acceptedJoinRequest(int id)
{
    if (id == 0) {
        name_0->setText(myNameLineEdit->text());
        color_0->setColor(myColor->color());
        type_0->setText(tr("Human"));
        _typeList[0] = "H";
    }
    if (id == 1) {
        name_1->setText(myNameLineEdit->text());
        color_1->setColor(myColor->color());
        type_1->setText(tr("Human"));
        _typeList[1] = "H";
    }
    if (id == 2) {
        name_2->setText(myNameLineEdit->text());
        color_2->setColor(myColor->color());
        type_2->setText(tr("Human"));
        _typeList[2] = "H";
    }
}

void OnlineDialog::refusedJoinRequest(QString error)
{
    QMessageBox::warning(this, tr("Warning"), error, QMessageBox::Ok);
    _alreadyJoined = false;
    myNameLineEdit->setEnabled(true);
    myColor->setClickable(true);
    color_0->setClickable(true);
    color_1->setClickable(true);
    color_2->setClickable(true);
    playerJoinButton->setEnabled(true);
    spectatorJoinButton->setEnabled(true);

    bool b0 = frame_0->isEnabled() && name_0->text() == "";
    bool b1 = frame_1->isEnabled() && name_1->text() == "";
    bool b2 = frame_2->isEnabled() && name_2->text() == "";
    if (!b0 && !b1 && !b2) {
        playerJoinButton->setEnabled(false);
        myColor->setEnabled(false);
    }
}

void OnlineDialog::addPlayer(int id, QString name, QString type)
{
    _typeList[id] = type;
    if (id == 0) {
        name_0->setText(name);
        if (type == "A") {
            type_0->setText(tr("AI"));
        }
        else {
            type_0->setText(tr("Remote"));
        }
    }
    if (id == 1) {
        name_1->setText(name);
        if (type == "A") {
            type_1->setText(tr("AI"));
        }
        else {
            type_1->setText(tr("Remote"));
        }
    }
    if (id == 2) {
        name_2->setText(name);
        if (type == "A") {
            type_2->setText(tr("AI"));
        }
        else {
            type_2->setText(tr("Remote"));
        }
    }

    bool b0 = frame_0->isEnabled() && name_0->text() == "";
    bool b1 = frame_1->isEnabled() && name_1->text() == "";
    bool b2 = frame_2->isEnabled() && name_2->text() == "";
    if (!b0 && !b1 && !b2) {
        playerJoinButton->setEnabled(false);
    }
}

void OnlineDialog::removePlayer(int id)
{
    _typeList[id] = "";
    playerJoinButton->setEnabled(!_alreadyJoined);
    if (id == 0) {
        name_0->setText("");
        type_0->setText("");
    }
    if (id == 1) {
        name_1->setText("");
        type_1->setText("");
    }
    if (id == 2) {
        name_2->setText("");
        type_2->setText("");
    }
}

void OnlineDialog::displaySettings(int d1, int d2, int numPlayers, int timer, bool playing)
{
    ServerSettings setS;
    myNameLineEdit->setText(setS.myName());
    myColor->setColor(setS.myColor());

    LocalSettings set;
    QList<PlayerInfo> info = set.playersInfo();
    numPlayersLabel->setText(QString::number(numPlayers));
    frame_0->setEnabled(true);
    color_0->setColor(info[0].color());
    color_0->setClickable(true);
    frame_1->setEnabled(true);
    color_1->setColor(info[1].color());
    color_1->setClickable(true);
    if (numPlayers == 3) {
        frame_2->setEnabled(true);
        color_2->setColor(info[2].color());
        color_2->setClickable(true);
    }
    else {
        frame_2->setEnabled(false);
    }
    d1Label->setText(QString::number(d1));
    d2Label->setText(QString::number(d2));
    timerLabel->setText(timer > 0 ? QString::number(timer) : "--");

    mySettingsFrame->setEnabled(true);
    label_10->setEnabled(true);
    myNameLineEdit->setEnabled(true);
    myColor->setEnabled(!playing);
    myColor->setClickable(!playing);
    spectatorJoinButton->setEnabled(true);
    playerJoinButton->setEnabled(!playing);
}

void OnlineDialog::setStatus(QString status)
{
    statusLabel->setText(status);
}

void OnlineDialog::gameStarted()
{
    QDialog::accept();
}

void OnlineDialog::connectToServer()
{
    bool ok;
    quint16 serverPort = portLineEdit->text().toUShort(&ok);
    if (!ok) {
        QMessageBox::critical(this, tr("Error"), tr("Cannot connect,\n'%1' is not a valid port number.").arg(portLineEdit->text()), QMessageBox::Ok);
        return;
    } else if (serverPort <= 1024) {
        QMessageBox::critical(this, tr("Error"), tr("Cannot connect,\nport number must be greater than 1024."), QMessageBox::Ok);
        return;
    }

    connectButton->setEnabled(false);
    ipLineEdit->setEnabled(false);
    portLineEdit->setEnabled(false);

    MainWindow *mainwin = qobject_cast<MainWindow*>(parent());
    if (mainwin->_net) {
        mainwin->_net->deleteLater();
        QApplication::processEvents();
        mainwin->_net = 0;
    }
    mainwin->_net = new GameClient(this, ipLineEdit->text(), serverPort);

    PlSettings->setEnabled(true);
}

void OnlineDialog::networkError(QString error)
{
    MainWindow *mainwin = qobject_cast<MainWindow*>(parent());
    QMessageBox::critical(this, tr("Error"), tr("Network error.\n") + error + ".", QMessageBox::Ok);
    if (mainwin->_net) {
        mainwin->_net->deleteLater();
        QApplication::processEvents();
        mainwin->_net = 0;
    }

    _alreadyJoined = false;
    _typeList = QVector<QString>(3, "");
    ipLineEdit->setEnabled(true);
    portLineEdit->setEnabled(true);
    connectButton->setEnabled(true);
    statusLabel->setText(RED_TEXT(tr("not connected")));
    numPlayersLabel->setText("?");
    d1Label->setText("?");
    d2Label->setText("?");
    timerLabel->setText("?");

    mySettingsFrame->setEnabled(false);
    label_10->setEnabled(false);
    myNameLineEdit->setEnabled(false);
    myColor->setEnabled(false);
    spectatorJoinButton->setEnabled(false);
    playerJoinButton->setEnabled(false);
    name_0->setText("");
    name_1->setText("");
    name_2->setText("");
    type_0->setText("");
    type_1->setText("");
    type_2->setText("");
    PlSettings->setEnabled(false);
    myNameLineEdit->setText("");
}

QList<QColor> OnlineDialog::colorList() const
{
    QList<QColor> list;
    list.append(color_0->color());
    list.append(color_1->color());
    list.append(color_2->color());
    return list;
}

void OnlineDialog::enableHelp()
{
    QWhatsThis::enterWhatsThisMode();
}
