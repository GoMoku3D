/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef PLAYERINFO_H
#define PLAYERINFO_H

#include <QColor>
#include <QString>

class PlayerInfo
{
    public:
        PlayerInfo();
        PlayerInfo(QString name, QColor color, QString type);
        QString name() const;
        QColor color() const;
        QString type() const;
    private:
        QString _name;
        QColor _color;
        QString _type;
};

#endif
