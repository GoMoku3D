/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "CCThreatTest.h"

void CCThreatTest::initTestCase()
{
    _mat = GameMatrix::create(5, 2, 2);
    _ai = new CCThreatSearchAI(0);
    GameMatrix::destroy();
}

void CCThreatTest::cleanupTestCase()
{
    if (_ai != 0) {
        delete _ai;
    }
}

void CCThreatTest::cleanup()
{
    if (_threat != 0) {
        delete _threat;
        _threat = 0;
    }
    GameMatrix::destroy();
    _mat = 0;
}

void CCThreatTest::ccThreat()
{
    //ambiente
    QFETCH(int, iD1);
    QFETCH(int, iPlayers);
    QFETCH(int, oPlayers);

    _mat = GameMatrix::create(iD1, 3, iPlayers);
    _threat = new CCThreat(0);

    //postcondizioni
    QCOMPARE(_threat->_opponentCC.size(), oPlayers);
}

void CCThreatTest::ccThreat_data()
{
    QTest::addColumn<int>("iD1");
    QTest::addColumn<int>("iPlayers");
    QTest::addColumn<int>("oPlayers");

    QTest::newRow("(5, 3, 2)") << 5 << 2 << 2;
    QTest::newRow("(5, 3, 3)") << 5 << 3 << 3;
    QTest::newRow("(7, 3, 2)") << 7 << 2 << 2;
    QTest::newRow("(7, 3, 3)") << 7 << 3 << 3;
    QTest::newRow("(9, 3, 2)") << 9 << 2 << 2;
    QTest::newRow("(9, 3, 3)") << 9 << 3 << 3;
}

void CCThreatTest::insertHook()
{
    //ambiente
    _mat = GameMatrix::create(5, 3, 3);
    _threat = new CCThreat(_ai);
    _mat->add(Move(1,Point(0, 0, 0)));
    _threat->insertHook(Point(0, 0, 0));
    _mat->add(Move(0,Point(3, 0, 0)));
    _threat->insertHook(Point(3, 0, 0));
    _mat->add(Move(1,Point(1, 0, 0)));
    _threat->insertHook(Point(1, 0, 0));

    //postcondizioni
    QVERIFY(_threat->_opponentCC[0].isEmpty());
    QVERIFY(_threat->_opponentCC[2].isEmpty());
    QCOMPARE(_threat->_opponentCC[1].size(), 1);
    QVERIFY(_threat->_opponentCC[1].first().nodes.find(Point(0, 0, 0)) != _threat->_opponentCC[1].first().nodes.end());
    QVERIFY(_threat->_opponentCC[1].first().nodes.find(Point(1, 0, 0)) != _threat->_opponentCC[1].first().nodes.end());
    QCOMPARE(_threat->_opponentCC[1].first().min_x, 0);
    QCOMPARE(_threat->_opponentCC[1].first().min_y, 0);
    QCOMPARE(_threat->_opponentCC[1].first().min_z, 0);
    QCOMPARE(_threat->_opponentCC[1].first().max_x, 1);
    QCOMPARE(_threat->_opponentCC[1].first().max_y, 0);
    QCOMPARE(_threat->_opponentCC[1].first().max_z, 0);
}

void CCThreatTest::scanFromMatrixHook()
{
    //ambiente
    _mat = GameMatrix::create(5, 3, 3);
    _threat = new CCThreat(_ai);
    _mat->add(Move(1,Point(0, 0, 0)));
    _mat->add(Move(0,Point(3, 0, 0)));
    _mat->add(Move(1,Point(1, 0, 0)));
    _threat->scanFromMatrixHook();

    //postcondizioni
    QVERIFY(_threat->_opponentCC[0].isEmpty());
    QVERIFY(_threat->_opponentCC[2].isEmpty());
    QCOMPARE(_threat->_opponentCC[1].size(), 1);
    QVERIFY(_threat->_opponentCC[1].first().nodes.find(Point(0, 0, 0)) != _threat->_opponentCC[1].first().nodes.end());
    QVERIFY(_threat->_opponentCC[1].first().nodes.find(Point(1, 0, 0)) != _threat->_opponentCC[1].first().nodes.end());
    QCOMPARE(_threat->_opponentCC[1].first().min_x, 0);
    QCOMPARE(_threat->_opponentCC[1].first().min_y, 0);
    QCOMPARE(_threat->_opponentCC[1].first().min_z, 0);
    QCOMPARE(_threat->_opponentCC[1].first().max_x, 1);
    QCOMPARE(_threat->_opponentCC[1].first().max_y, 0);
    QCOMPARE(_threat->_opponentCC[1].first().max_z, 0);
}

QTEST_MAIN(CCThreatTest)
