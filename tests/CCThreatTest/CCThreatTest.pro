TEMPLATE = app
TARGET = CCThreatTest
QT -= gui

include(../tests.pri)

HEADERS += CCThreatTest.h
SOURCES += CCThreatTest.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o \
           ../../build/Threat.o \
           ../../build/ThreatSearchAI.o \
           ../../build/AI.o \
           ../../build/CCThreat.o \
           ../../build/CCThreatSearchAI.o
