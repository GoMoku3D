TEMPLATE = app
TARGET = ThreatTest
QT -= gui

include(../tests.pri)

HEADERS += ThreatTest.h
SOURCES += ThreatTest.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o \
           ../../build/Threat.o \
           ../../build/ThreatSearchAI.o \
           ../../build/AI.o
