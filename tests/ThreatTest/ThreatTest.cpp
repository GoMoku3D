/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "ThreatTest.h"

void ThreatTest::initTestCase()
{
    _mat = GameMatrix::create(5, 2, 2);
    _ai = new ThreatSearchAI(0);
    GameMatrix::destroy();
}

void ThreatTest::cleanupTestCase()
{
    if (_ai != 0) {
        delete _ai;
    }
}

void ThreatTest::cleanup()
{
    if (_threat != 0) {
        delete _threat;
        _threat = 0;
    }
    GameMatrix::destroy();
    _mat = 0;
}

void ThreatTest::threat()
{
    //ambiente
    QFETCH(int, iD1);
    QFETCH(int, iD2);
    QFETCH(int, iPlayers);
    QFETCH(int, oD1);
    QFETCH(int, oD2);

    _mat = GameMatrix::create(iD1, iD2, iPlayers);
    _threat = new Threat(_ai);

    //postcondizioni
    QVERIFY(_threat->_ai == _ai);
    QVERIFY(_threat->_mat == _mat);
    QCOMPARE(_threat->_d1, oD1);
    QCOMPARE(_threat->_d2, oD2);
    QVERIFY(_threat->_priority != 0);
}

void ThreatTest::threat_data()
{
    QTest::addColumn<int>("iD1");
    QTest::addColumn<int>("iD2");
    QTest::addColumn<int>("iPlayers");
    QTest::addColumn<int>("oD1");
    QTest::addColumn<int>("oD2");

    QTest::newRow("(5, 1, 2)") << 5 << 1 << 2 << 5 << 1;
    QTest::newRow("(5, 2, 2)") << 5 << 2 << 2 << 5 << 2;
    QTest::newRow("(5, 3, 2)") << 5 << 3 << 2 << 5 << 3;
    QTest::newRow("(5, 1, 3)") << 5 << 1 << 3 << 5 << 1;
    QTest::newRow("(5, 2, 3)") << 5 << 2 << 3 << 5 << 2;
    QTest::newRow("(5, 3, 3)") << 5 << 3 << 3 << 5 << 3;
    QTest::newRow("(7, 1, 2)") << 7 << 1 << 2 << 7 << 1;
    QTest::newRow("(7, 2, 2)") << 7 << 2 << 2 << 7 << 2;
    QTest::newRow("(7, 3, 2)") << 7 << 3 << 2 << 7 << 3;
    QTest::newRow("(7, 1, 3)") << 7 << 1 << 3 << 7 << 1;
    QTest::newRow("(7, 2, 3)") << 7 << 2 << 3 << 7 << 2;
    QTest::newRow("(7, 3, 3)") << 7 << 3 << 3 << 7 << 3;
    QTest::newRow("(9, 1, 2)") << 9 << 1 << 2 << 9 << 1;
    QTest::newRow("(9, 2, 2)") << 9 << 2 << 2 << 9 << 2;
    QTest::newRow("(9, 3, 2)") << 9 << 3 << 2 << 9 << 3;
    QTest::newRow("(9, 1, 3)") << 9 << 1 << 3 << 9 << 1;
    QTest::newRow("(9, 2, 3)") << 9 << 2 << 3 << 9 << 2;
    QTest::newRow("(9, 3, 3)") << 9 << 3 << 3 << 9 << 3;
}

void ThreatTest::insert()
{
    //ambiente
    _mat = GameMatrix::create(5, 2, 2);
    _threat = new Threat(_ai);
    _mat->add(Move(0,Point(0, 0, 0)));
    _threat->insert(Point(0, 0, 0));
    _mat->add(Move(1,Point(1, 0, 0)));
    _threat->insert(Point(1, 0, 0));

    //postcondizioni
    //QCOMPARE(_threat->_x.size(), 0);
    QCOMPARE(_threat->_y.size(), 1);
    QCOMPARE(_threat->_z.size(), 1);
    QVERIFY(_threat->_y.find(Point(0, 0, 0)) != _threat->_y.end());
    QVERIFY(_threat->_z.find(Point(0, 0, 0)) != _threat->_z.end());
    QVERIFY(_threat->_priority[0] != 0);
    QVERIFY(_threat->_priority[1] == 0);
    QVERIFY(_threat->_priority[2] == 0);
    QVERIFY(_threat->_priority[3] == 0);
    QCOMPARE(_threat->_priority[0]->point, Point(0, 0, 0));
    QVERIFY(_threat->_priority[0]->next != 0);
    QCOMPARE(_threat->_priority[0]->next->point, Point(0, 0, 0));
    QVERIFY(_threat->_priority[0]->next->next == 0);
}

void ThreatTest::scanFromMatrix()
{
    //ambiente
    _mat = GameMatrix::create(9, 3, 3);
    _threat = new Threat(_ai);
    _mat->add(Move(0, Point(0, 0, 0)));
    _mat->add(Move(1, Point(1, 0, 0)));
    _mat->add(Move(2, Point(2, 0, 0)));
    _threat->scanFromMatrix();

    //postcondizioni
    QCOMPARE(_threat->_x.size(), 0);
    QCOMPARE(_threat->_y.size(), 1);
    QCOMPARE(_threat->_z.size(), 1);
    QVERIFY(_threat->_y.find(Point(0, 0, 0)) != _threat->_y.end());
    QVERIFY(_threat->_z.find(Point(0, 0, 0)) != _threat->_z.end());
    QVERIFY(_threat->_priority[0] != 0);
    QVERIFY(_threat->_priority[1] == 0);
    QVERIFY(_threat->_priority[2] == 0);
    QVERIFY(_threat->_priority[3] == 0);
    QVERIFY(_threat->_priority[4] == 0);
    QVERIFY(_threat->_priority[5] == 0);
    QVERIFY(_threat->_priority[6] == 0);
    QVERIFY(_threat->_priority[7] == 0);
    QCOMPARE(_threat->_priority[0]->point, Point(0, 0, 0));
    QVERIFY(_threat->_priority[0]->next != 0);
    QCOMPARE(_threat->_priority[0]->next->point, Point(0, 0, 0));
    QVERIFY(_threat->_priority[0]->next->next == 0);
}

QTEST_MAIN(ThreatTest)
