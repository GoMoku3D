TEMPLATE = app
TARGET = StreamSocketTest
QT -= gui
QT += network xml

include(../tests.pri)

HEADERS += StreamSocketTest.h
SOURCES += StreamSocketTest.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o \
           ../../build/StreamSocket.o \
           ../../build/moc_StreamSocket.o
