/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef STREAMSOCKETTEST_H
#define STREAMSOCKETTEST_H

#include <QObject>
#include <QSignalSpy>
#include <QTcpServer>
#include <QTcpSocket>

#include "StreamSocket.h"

class StreamSocketTest : public QObject
{
    Q_OBJECT

    private:
        QTcpServer *_listener;
        QTcpSocket *_serverStub;
        StreamSocket *_testedObject;
        QSignalSpy *_receivedChatMessageSpy;
        QSignalSpy *_receivedMoveSpy;
        QSignalSpy *_startGameSpy;

    private slots:
        void initTestCase();
        void cleanupTestCase();
        void init();
        void cleanup();
        void changeState();
        void sendChatMessage();
        void sendChatMessage_data();
        void sendMove();
        void sendMove_data();
        void openStream();
        void receivedChatMessage();
        void receivedMove();
        void startGame();
};

#endif
