/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QHostAddress>
#include <QTest>
#include <QVariant>

#include "StreamSocketTest.h"

void StreamSocketTest::initTestCase()
{
    qRegisterMetaType<Move>();

    _listener = new QTcpServer(this);
    QVERIFY(_listener->listen(QHostAddress::LocalHost, 42000));
    QVERIFY(!_listener->hasPendingConnections());
}

void StreamSocketTest::cleanupTestCase()
{
    _listener->close();
    delete _listener;
}

void StreamSocketTest::init()
{
    // inizializzazioni
    QTcpSocket *s = new QTcpSocket();
    _testedObject = new StreamSocket(s);
    s->connectToHost(QHostAddress::LocalHost, 42000);
    QVERIFY(_listener->waitForNewConnection(10000));
    QVERIFY(_listener->hasPendingConnections());
    _serverStub = _listener->nextPendingConnection();
    _receivedChatMessageSpy = new QSignalSpy(_testedObject, SIGNAL(receivedChatMessage(QString)));
    _receivedMoveSpy = new QSignalSpy(_testedObject, SIGNAL(receivedMove(Move)));
    _startGameSpy = new QSignalSpy(_testedObject, SIGNAL(startGame()));

    // postcondizioni
    QCOMPARE(_listener->hasPendingConnections(), false);
    QCOMPARE(_serverStub->state(), QAbstractSocket::ConnectedState);
    QCOMPARE(_testedObject->_socket, s);
    QCOMPARE(_testedObject->state(), StreamSocket::Unconnected);
    QCOMPARE(_testedObject->_buffer.isEmpty(), true);
    QCOMPARE(_receivedChatMessageSpy->isValid(), true);
    QCOMPARE(_receivedChatMessageSpy->count(), 0);
    QCOMPARE(_receivedMoveSpy->isValid(), true);
    QCOMPARE(_receivedMoveSpy->count(), 0);
    QCOMPARE(_startGameSpy->isValid(), true);
    QCOMPARE(_startGameSpy->count(), 0);
}

void StreamSocketTest::cleanup()
{
    delete _receivedChatMessageSpy;
    delete _receivedMoveSpy;
    delete _startGameSpy;
    delete _testedObject;
    delete _serverStub;
}

void StreamSocketTest::changeState()
{
    // ambiente
    _testedObject->changeState(StreamSocket::Idle);

    // postcondizioni
    QCOMPARE(_testedObject->_state, StreamSocket::Idle);
    QCOMPARE(_testedObject->state(), StreamSocket::Idle);
}

void StreamSocketTest::sendChatMessage()
{
    QFETCH(QString, msg);
    QFETCH(QString, output);

    // ambiente
    _testedObject->sendChatMessage("sender", msg);
    _testedObject->_socket->waitForBytesWritten(10000);

    // postcondizioni
    QCOMPARE(_serverStub->waitForReadyRead(10000), true);
    QCOMPARE(QString(_serverStub->readAll()), output);
}

void StreamSocketTest::sendChatMessage_data()
{
    QTest::addColumn<QString>("msg");
    QTest::addColumn<QString>("output");

    QTest::newRow("Chat1") << "1st message" << "<chatMessage from=\"sender\">1st message</chatMessage>";
    QTest::newRow("Chat2") << "Second Message" << "<chatMessage from=\"sender\">Second Message</chatMessage>";
    QTest::newRow("Chat3") << " " << "<chatMessage from=\"sender\"> </chatMessage>";
}

void StreamSocketTest::sendMove()
{
    QFETCH(int, id);
    QFETCH(int, x);
    QFETCH(int, y);
    QFETCH(int, z);
    QFETCH(QString, output);

    // ambiente
    Move m(id, Point(x, y, z));
    _testedObject->sendMove(m);
    _testedObject->_socket->waitForBytesWritten(10000);

    // postcondizioni
    QCOMPARE(_serverStub->waitForReadyRead(10000), true);
    QCOMPARE(QString(_serverStub->readAll()), output);
}

void StreamSocketTest::sendMove_data()
{
    QTest::addColumn<int>("id");
    QTest::addColumn<int>("x");
    QTest::addColumn<int>("y");
    QTest::addColumn<int>("z");
    QTest::addColumn<QString>("output");

    QTest::newRow("Move1") << 0 << 2 << 3 << 6 << "<move><player>0</player><point><x>2</x><y>3</y><z>6</z></point></move>";
    QTest::newRow("Move2") << 1 << 0 << 5 << 1 << "<move><player>1</player><point><x>0</x><y>5</y><z>1</z></point></move>";
    QTest::newRow("Move3") << 2 << 10 << 4 << 25 << "<move><player>2</player><point><x>10</x><y>4</y><z>25</z></point></move>";
}

void StreamSocketTest::openStream()
{
    // ambiente
    QString output("<?xml version=\"1.0\" encoding=\"UTF-8\"?><stream xmlns=\"http://www.itworks.org/GoMoku3D/NetworkProtocol\" version=\"1.0\">");
    _testedObject->openStream();
    _testedObject->_socket->waitForBytesWritten(10000);

    // postcondizioni
    QCOMPARE(_serverStub->waitForReadyRead(10000), true);
    QCOMPARE(QString(_serverStub->read(output.size())), output);
}

void StreamSocketTest::receivedChatMessage()
{
    // ambiente
    QByteArray input("<chatMessage from=\"Tizio\">Salve, Caio!</chatMessage>");
    QString msg("<Tizio> Salve, Caio!");
    _testedObject->changeState(StreamSocket::Playing);
    _serverStub->write(input);
    QTest::qWait(100);

    // postcondizioni
    QCOMPARE(_receivedChatMessageSpy->count(), 1);
    QCOMPARE(_receivedChatMessageSpy->at(0).at(0).type(), QVariant::String);
    QCOMPARE(_receivedChatMessageSpy->at(0).at(0).toString(), msg);
}

void StreamSocketTest::receivedMove()
{
    // ambiente
    QByteArray input("<move><player>0</player><point><x>1</x><y>2</y><z>3</z></point></move>");
    int id = 0;
    int x = 1;
    int y = 2;
    int z = 3;
    _testedObject->changeState(StreamSocket::AwaitingMove);
    _serverStub->write(input);
    QTest::qWait(100);

    // postcondizioni
    QCOMPARE(_receivedMoveSpy->count(), 1);
    QCOMPARE(_receivedMoveSpy->at(0).at(0).value<Move>(), Move(id, Point(x, y, z)));
}

void StreamSocketTest::startGame()
{
    // ambiente
    QByteArray input("<startGame/>");
    _testedObject->changeState(StreamSocket::AwaitingGameStart);
    _serverStub->write(input);
    QTest::qWait(100);

    // postcondizioni
    QCOMPARE(_startGameSpy->count(), 1);
    QCOMPARE(_startGameSpy->at(0).isEmpty(), true);
    QCOMPARE(_testedObject->state(), StreamSocket::Playing);
}

QTEST_MAIN(StreamSocketTest)
