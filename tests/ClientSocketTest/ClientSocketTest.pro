TEMPLATE = app
TARGET = ClientSocketTest
QT -= gui
QT += network xml

include(../tests.pri)

HEADERS += ClientSocketTest.h
SOURCES += ClientSocketTest.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o \
           ../../build/StreamSocket.o \
           ../../build/moc_StreamSocket.o \
           ../../build/ClientSocket.o \
           ../../build/moc_ClientSocket.o
