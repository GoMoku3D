/********************************************************************
 *
 * Copyright (C) 2008  Davide Pesavento
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <QTest>
#include <QVariant>

#include "ClientSocketTest.h"

void ClientSocketTest::initTestCase()
{
    qRegisterMetaType< QList<Move> >();

    _listener = new QTcpServer(this);
    QVERIFY(_listener->listen(QHostAddress::LocalHost, 42000));
    QVERIFY(!_listener->hasPendingConnections());
}

void ClientSocketTest::cleanupTestCase()
{
    _listener->close();
    delete _listener;
}

void ClientSocketTest::init()
{
    // inizializzazioni
    QTcpSocket *s = new QTcpSocket();
    _testedObject = new ClientSocket(s);
    s->connectToHost(QHostAddress::LocalHost, 42000);
    QVERIFY(_listener->waitForNewConnection(10000));
    QVERIFY(_listener->hasPendingConnections());
    _serverStub = _listener->nextPendingConnection();
    _joinAcceptedSpy = new QSignalSpy(_testedObject, SIGNAL(joinAccepted(int)));
    _joinRefusedSpy = new QSignalSpy(_testedObject, SIGNAL(joinRefused(QString)));
    _receivedGameSettingsSpy = new QSignalSpy(_testedObject, SIGNAL(receivedGameSettings(int, int, int, int, bool)));
    _receivedHistorySpy = new QSignalSpy(_testedObject, SIGNAL(receivedHistory(QList<Move>)));

    // postcondizioni
    QCOMPARE(_listener->hasPendingConnections(), false);
    QCOMPARE(_serverStub->state(), QAbstractSocket::ConnectedState);
    QCOMPARE(_testedObject->_socket, s);
    QCOMPARE(_testedObject->_buffer.isEmpty(), true);
    QCOMPARE(_joinAcceptedSpy->isValid(), true);
    QCOMPARE(_joinAcceptedSpy->count(), 0);
    QCOMPARE(_joinRefusedSpy->isValid(), true);
    QCOMPARE(_joinRefusedSpy->count(), 0);
    QCOMPARE(_receivedGameSettingsSpy->isValid(), true);
    QCOMPARE(_receivedGameSettingsSpy->count(), 0);
    QCOMPARE(_receivedHistorySpy->isValid(), true);
    QCOMPARE(_receivedHistorySpy->count(), 0);
}

void ClientSocketTest::cleanup()
{
    delete _joinAcceptedSpy;
    delete _joinRefusedSpy;
    delete _receivedGameSettingsSpy;
    delete _receivedHistorySpy;
    delete _testedObject;
    delete _serverStub;
}

void ClientSocketTest::joinGame()
{
    QFETCH(QString, mode);
    QFETCH(QString, name);
    QFETCH(QString, output);

    // ambiente
    _testedObject->changeState(StreamSocket::Idle);
    _testedObject->joinGame(mode, name);
    _testedObject->_socket->waitForBytesWritten(10000);

    // postcondizioni
    QCOMPARE(_serverStub->waitForReadyRead(10000), true);
    QCOMPARE(QString(_serverStub->read(output.size())), output);
}

void ClientSocketTest::joinGame_data()
{
    QTest::addColumn<QString>("mode");
    QTest::addColumn<QString>("name");
    QTest::addColumn<QString>("output");

    QTest::newRow("Join1") << "player" << "Pippo" << "<joinRequest gameMode=\"player\">Pippo</joinRequest>";
    QTest::newRow("Join2") << "spectator" << "Pluto" << "<joinRequest gameMode=\"spectator\">Pluto</joinRequest>";
}

void ClientSocketTest::cancelJoin()
{
    // ambiente
    QString output("<playerLeft>-1</playerLeft>");
    _testedObject->changeState(StreamSocket::AwaitingGameStart);
    _testedObject->cancelJoin();
    _testedObject->_socket->waitForBytesWritten(10000);

    // postcondizioni
    QCOMPARE(_serverStub->waitForReadyRead(10000), true);
    QCOMPARE(QString(_serverStub->read(output.size())), output);
}

void ClientSocketTest::joinAccepted()
{
    // ambiente
    QByteArray input("<joinACK>1</joinACK>");
    int id = 1;
    _testedObject->changeState(StreamSocket::AwaitingJoinAnswer);
    _serverStub->write(input);
    QTest::qWait(100);

    // postcondizioni
    QCOMPARE(_testedObject->state(), StreamSocket::AwaitingGameStart);
    QCOMPARE(_joinAcceptedSpy->count(), 1);
    QCOMPARE(_joinAcceptedSpy->at(0).at(0).type(), QVariant::Int);
    QCOMPARE(_joinAcceptedSpy->at(0).at(0).toInt(), id);
}

void ClientSocketTest::joinRefused()
{
    // ambiente
    QByteArray input("<joinNAK>2</joinNAK>");
    QString errorMessage("The name you have chosen is already\nbeing used by someone else.");
    _testedObject->changeState(StreamSocket::AwaitingJoinAnswer);
    _serverStub->write(input);
    QTest::qWait(100);

    // postcondizioni
    QCOMPARE(_testedObject->state(), StreamSocket::Idle);
    QCOMPARE(_joinRefusedSpy->count(), 1);
    QCOMPARE(_joinRefusedSpy->at(0).at(0).type(), QVariant::String);
    QCOMPARE(_joinRefusedSpy->at(0).at(0).toString(), errorMessage);
}

void ClientSocketTest::receivedGameSettings()
{
    // ambiente
    QByteArray input("<settings playing=\"false\"><difficultyOne>5</difficultyOne><difficultyTwo>2</difficultyTwo><numberOfPlayers>3</numberOfPlayers><timerDuration>60</timerDuration></settings>");
    _testedObject->changeState(StreamSocket::FullyOpened);
    _serverStub->write(input);
    QTest::qWait(100);

    // postcondizioni
    QCOMPARE(_testedObject->state(), StreamSocket::Idle);
    QCOMPARE(_receivedGameSettingsSpy->count(), 1);
    QCOMPARE(_receivedGameSettingsSpy->at(0).at(0).type(), QVariant::Int);
    QCOMPARE(_receivedGameSettingsSpy->at(0).at(0).toInt(), 5);
    QCOMPARE(_receivedGameSettingsSpy->at(0).at(1).type(), QVariant::Int);
    QCOMPARE(_receivedGameSettingsSpy->at(0).at(1).toInt(), 2);
    QCOMPARE(_receivedGameSettingsSpy->at(0).at(2).type(), QVariant::Int);
    QCOMPARE(_receivedGameSettingsSpy->at(0).at(2).toInt(), 3);
    QCOMPARE(_receivedGameSettingsSpy->at(0).at(3).type(), QVariant::Int);
    QCOMPARE(_receivedGameSettingsSpy->at(0).at(3).toInt(), 60);
    QCOMPARE(_receivedGameSettingsSpy->at(0).at(4).type(), QVariant::Bool);
    QCOMPARE(_receivedGameSettingsSpy->at(0).at(4).toBool(), false);
}

void ClientSocketTest::receivedHistory()
{
    // ambiente
    QByteArray input("<history><move><player>0</player><point><x>2</x><y>3</y><z>6</z></point></move><move><player>1</player><point><x>0</x><y>5</y><z>1</z></point></move></history>");
    Move m0(0, Point(2, 3, 6));
    Move m1(1, Point(0, 5, 1));
    _testedObject->changeState(StreamSocket::AwaitingGameStart);
    _serverStub->write(input);
    QTest::qWait(100);

    // postcondizioni
    QCOMPARE(_receivedHistorySpy->count(), 1);
    QList<Move> moves = _receivedHistorySpy->at(0).at(0).value< QList<Move> >();
    QCOMPARE(moves.at(0), m0);
    QCOMPARE(moves.at(1), m1);
}

QTEST_MAIN(ClientSocketTest)
