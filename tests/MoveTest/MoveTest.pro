TEMPLATE = app
TARGET = MoveTest
QT -= gui

include(../tests.pri)

HEADERS += MoveTest.h
SOURCES += MoveTest.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o
