/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "MoveTest.h"

void MoveTest::initTestCase()
{
    GameMatrix::create(5, 1, 2);
}

void MoveTest::cleanupTestCase()
{
    GameMatrix::destroy();
}

void MoveTest::init()
{
    _m1 = Move(2, Point(0, 0, 0));
    _m2 = Move(0, Point(5, 3, 0));
    _m3 = Move(0, Point(4, 3, 0));
}

void MoveTest::move()
{
    //postcondizioni
    QCOMPARE(_m1._playerId, 2);
    QCOMPARE(_m2._playerId, 0);
    QCOMPARE(_m3._playerId, 0);
    QCOMPARE(_m1._coord, Point(0, 0, 0));
    QCOMPARE(_m2._coord, Point(5, 3, 0));
    QCOMPARE(_m3._coord, Point(4, 3, 0));
}

void MoveTest::isValid()
{
    QVERIFY(_m1.isValid() == false);
    QVERIFY(_m2.isValid() == false);
    QVERIFY(_m3.isValid() == true);
}

QTEST_MAIN(MoveTest)
