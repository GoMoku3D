TEMPLATE = app
TARGET = PointTest
QT -= gui

include(../tests.pri)

HEADERS += PointTest.h
SOURCES += PointTest.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o
