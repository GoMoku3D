/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "PointTest.h"

void PointTest::initTestCase()
{
    GameMatrix::create(5, 1, 2);
}

void PointTest::cleanupTestCase()
{
    GameMatrix::destroy();
}

void PointTest::init()
{
    _default = Point();
    _p1 = Point(0, 0, 0);
    _p2 = Point(5, 3, 0);
}

void PointTest::point()
{
    //postcondizioni
    QCOMPARE(_default._x, -1);
    QCOMPARE(_default._y, -1);
    QCOMPARE(_default._z, -1);
    QCOMPARE(_p1._x, 0);
    QCOMPARE(_p1._y, 0);
    QCOMPARE(_p1._z, 0);
    QCOMPARE(_p2._x, 5);
    QCOMPARE(_p2._y, 3);
    QCOMPARE(_p2._z, 0);
}

void PointTest::isValid()
{
    //postcondizioni
    QVERIFY(_default.isValid() == false);
    QVERIFY(_p1.isValid() == true);
    QVERIFY(_p2.isValid() == false);
}

void PointTest::isNull()
{
    //postcondizioni
    QVERIFY(_default.isNull() == true);
    QVERIFY(_p1.isNull() == false);
    QVERIFY(_p2.isNull() == false);
}

QTEST_MAIN(PointTest)
