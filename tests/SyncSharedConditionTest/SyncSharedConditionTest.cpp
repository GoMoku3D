/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "SyncSharedConditionTest.h"

void SyncSharedConditionTest::initTestCase()
{
    _thread = new TestThread();
    _sync = SyncSharedCondition::instance();
    _thread->_sync = _sync;
}

void SyncSharedConditionTest::cleanupTestCase()
{
    _thread->deleteLater();
    delete _sync;
}

void SyncSharedConditionTest::TestThread::run()
{
    forever {
        _sync->lock();
        _sync->notifyMove(Point(0, 0, 0));
        _sync->unlock();
        sleep(1);
    }
}

void SyncSharedConditionTest::waitNotify()
{
    //ambiente
    _thread->start();
    _sync->lock();
    Point result = _sync->waitForMove();
    _sync->unlock();

    //postcondizioni
    QCOMPARE(result, Point(0, 0, 0));
}

QTEST_MAIN(SyncSharedConditionTest)
