TEMPLATE = app
TARGET = SyncSharedConditionTest
QT -= gui

include(../tests.pri)

HEADERS += SyncSharedConditionTest.h
SOURCES += SyncSharedConditionTest.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o \
           ../../build/SyncSharedCondition.o
