/********************************************************************
 *
 * Copyright (C) 2008  Tobia Zorzan
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include <Inventor/Qt/SoQt.h>

#include "GUITest.h"

GUITest *testInstance;

void SyncTestThread::run()
{
    SyncSharedCondition::instance()->lock();
    result = SyncSharedCondition::instance()->waitForMove();
    SyncSharedCondition::instance()->unlock();
}

void GUITest::msgHandler(QtMsgType msgType, const char *msg)
{
    //fprintf(stderr, "LOGGER: %s\n", msg);
    Q_UNUSED(msgType);
    QStringList log = QString(msg).split(":");
    if(log[0] == "Rotation") {
        testInstance->_frontside = (log[1].split(" "))[1].toInt();
        testInstance->_upside = (log[1].split(" "))[4].toInt();
        fprintf(stderr, "LOGGER: %s\n", msg);
    } else if (log[0] == "New Explosion ") {
        fprintf(stderr, "LOGGER: %s, cleaning data\n", msg);
        testInstance->_explodedCounter = 0;
        testInstance->_firstExplodedCube = Point();
        testInstance->_lastExplodedCube = Point();
    } else if (log[0] == "Explosion") {
        Point cube = Point((log[1].split(" "))[2].toInt(), (log[1].split(" "))[5].toInt(), (log[1].split(" "))[8].toInt());
        if(testInstance->_explodedCounter == 0) {
            testInstance->_firstExplodedCube = cube;
        }
        testInstance->_lastExplodedCube = cube;
        testInstance->_explodedCounter++;
    } else if (log[0] == "Draw") {
        //append to list the cube and its color
        QStringList data = log[1].split(" ", QString::SkipEmptyParts);
        testInstance->_drawedMoves.append(Point(data[0].toInt(), data[1].toInt(), data[2].toInt()));

        data = log[2].split(" ", QString::SkipEmptyParts);
        QColor color;
        color.setRgbF(data[0].toFloat(), data[1].toFloat(), data[2].toFloat(), 1.0);
        testInstance->_drawedColors.append(color);
        fprintf(stderr, "LOGGER: %s.\n", msg);
    }
}

void GUITest::initTestCase()
{
    //inizizalizza
    testInstance = this;
    QMainWindow * mainwin = new QMainWindow();
    qInstallMsgHandler(GUITest::msgHandler);
    SoQt::init(mainwin);
    GameMatrix::create(5, 2, 2);
    _widget = new RenderWidget(mainwin, 10);

    QWidget *eventWidget = _widget->renderarea()->getWidget();
    mainwin->setMouseTracking(true);
    _widget->setMouseTracking(true);
    eventWidget->setMouseTracking(true);

    _frontside=0;
    _upside=0;

    _explodedCounter = 0;
}

void GUITest::cleanupTestCase()
{
    delete _widget;
}

void GUITest::rotate_data()
{
    //Dati per il test di rotazione
    QTest::addColumn<int>("asse");
    QTest::addColumn<int>("frontside");
    QTest::addColumn<int>("upside");

    QTest::newRow("rot1") << 2 << 1 << 2;
    QTest::newRow("rot1") << 2 << 4 << 2;
    QTest::newRow("rot1") << 1 << 3 << 4;
    QTest::newRow("rot1") << 1 << 5 << 3;
}

void GUITest::rotate()
{
    //metodo di test della rotazione
    QFETCH(int, asse);
    QFETCH(int, frontside);
    QFETCH(int, upside);

    SbRotation rot;
    switch(asse) {
        case 0:
            rot = SbRotation(SbVec3f(1, 0, 0), M_PI/2);
            break;
        case 1:
            rot = SbRotation(SbVec3f(-1, 0, 0), M_PI/2);
            break;
        case 2:
            rot = SbRotation(SbVec3f(0, 1, 0), M_PI/2);
            break;
        case 3:
            rot = SbRotation(SbVec3f(0, -1, 0), M_PI/2);
            break;
        case 4:
            rot = SbRotation(SbVec3f(0, 0, 1), M_PI/2);
            break;
        case 5:
            rot = SbRotation(SbVec3f(0, 0, -1), M_PI/2);
            break;
    }
    _widget->renderarea()->scene()->applyRotationToCamera(rot);

    QCOMPARE(_frontside, frontside);
    QCOMPARE(_upside, upside);
}

void GUITest::explode_data()
{
    //dati per il test di esplosione
    QTest::addColumn<Point>("posizione");
    QTest::addColumn<int>("contatore");
    QTest::addColumn<Point>("primo");
    QTest::addColumn<Point>("ultimo");

    QTest::newRow("t1") << Point(9, 0, 8) << 100 << Point(0, 0, 9) << Point(9, 9, 9);
    QTest::newRow("t2") << Point(5, 0, 6) << 300 << Point(0, 0, 9) << Point(9, 9, 7);
    QTest::newRow("t3") << Point(6, 4, 4) << 500 << Point(0, 0, 9) << Point(9, 9, 5);
    QTest::newRow("t4") << Point(2, 7, 3) << 600 << Point(0, 0, 9) << Point(9, 9, 4);
    QTest::newRow("t5") << Point(7, 8, 0) << 900 << Point(0, 0, 9) << Point(9, 9, 1);
    QTest::newRow("t6") << Point(2, 1, 9) << 0 << Point(-1, -1, -1) << Point(-1, -1, -1);
}

void GUITest::explode()
{
    //metodo di test per l'esplosione
    QFETCH(Point, posizione);
    QFETCH(int, contatore);
    QFETCH(Point, primo);
    QFETCH(Point, ultimo);

    _widget->renderarea()->selectCube(posizione);

    QTest::keyPress(_widget->renderarea()->getNormalWidget(), Qt::Key_Space);

    QTest::qWait(600);

    QTest::keyPress(_widget->renderarea()->getNormalWidget(), Qt::Key_Space);

    QTest::qWait(300);

    _widget->renderarea()->scene()->setDirectionOfExplosion(CubeRenderArea::NO_ORIENTATION);

    QCOMPARE(_explodedCounter, contatore);
    QCOMPARE(_firstExplodedCube, primo);
    QCOMPARE(_lastExplodedCube, ultimo);
}

void GUITest::drawMove()
{
    //metodo di test per la rappresentazione di una mossa
    QList<Point> points;
    QList<QColor> colors;
    points.append(Point(3, 4, 6));
    colors.append(QColor(200, 0, 0));
    points.append(Point(9, 2, 4));
    colors.append(QColor(0, 200, 0));
    points.append(Point(0, 0, 1));
    colors.append(QColor(0, 0, 200));
    points.append(Point(9, 2, 4));
    colors.append(QColor(200, 0, 0));

    _widget->uncheckedDraw(points, colors);

    _widget->drawMove(Point(8, 4, 4), QColor(255, 0, 255));
    _widget->drawMove(Point(13, 0, 1), QColor(255, 255, 255));
    _widget->drawMove(Point(9, 2, 4), QColor(0, 0, 200));


    QList<Point> expectedPoints;
    QList<QColor> expectedColors;
    expectedPoints.append(Point(3, 4, 6));
    expectedColors.append(QColor(200, 0, 0));
    expectedPoints.append(Point(9, 2, 4));
    expectedColors.append(QColor(0, 200, 0));
    expectedPoints.append(Point(0, 0, 1));
    expectedColors.append(QColor(0, 0, 200));
    expectedPoints.append(Point(9, 2, 4));
    expectedColors.append(QColor(200, 0, 0));
    expectedPoints.append(Point(8, 4, 4));
    expectedColors.append(QColor(255, 0, 255));
    expectedPoints.append(Point(9, 2, 4));
    expectedColors.append(QColor(0, 0, 200));

    QCOMPARE(_drawedMoves, expectedPoints);
    QCOMPARE(_drawedColors, expectedColors);
}

void GUITest::doMove()
{
    //metodo di test per l'esecuzione di una mossa
    QFETCH(bool, accept);
    QFETCH(Point, point);
    QFETCH(Point, move);

    _thread = new SyncTestThread();
    _thread->start();

    _widget->acceptMove(accept);
    _widget->renderarea()->selectCube(point);

    QTest::keyPress(_widget->renderarea()->getNormalWidget(), Qt::Key_Return);
    QTest::qWait(800);

    QCOMPARE(_thread->result, move);

    delete _thread;
    _thread = 0;
}

void GUITest::doMove_data()
{
    //dati per il test doMove()
    QTest::addColumn<bool>("accept");
    QTest::addColumn<Point>("point");
    QTest::addColumn<Point>("move");

    QTest::newRow("t1") << true << Point(5, 2, 1) << Point(5, 2, 1);
    QTest::newRow("t2") << false << Point(0, 7, 2) << Point(-1, -1, -1);
    QTest::newRow("t3") << true << Point(3, 3, 3) << Point(3, 3, 3);
    QTest::newRow("t4") << true << Point(1, 4, 3) << Point(1, 4, 3);
    QTest::newRow("t5") << false << Point(2, 2, 2) << Point(-1, -1, -1);
}

QTEST_MAIN(GUITest)
