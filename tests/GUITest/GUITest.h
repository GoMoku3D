/********************************************************************
 *
 * Copyright (C) 2008  Tobia Zorzan
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef GUITEST_H
#define GUITEST_H

#include <QObject>
#include <QTest>
#include <QtGui>

#include "RenderWidget.h"
#include "CubeRenderArea.h"

class CubeRenderArea;

enum CubeRenderArea::ORIENTATION;

class SyncTestThread : public QThread
{
    public:
        void run();
        Point result;

};

class GUITest : public QObject
{
    Q_OBJECT

    private:
        RenderWidget* _widget;
        int _explodedCounter;
        Point _firstExplodedCube;
        Point _lastExplodedCube;
        QList<Point> _drawedMoves;
        QList<QColor> _drawedColors;
        //CubeRenderArea::Orientation _frontside;
        //CubeRenderArea::Orientation _upside;
        int _frontside;
        int _upside;
        SyncTestThread *_thread;

    public:
	    static void msgHandler(QtMsgType msgType, const char *msg);

    private slots:
        void initTestCase();
        void cleanupTestCase();        
        void rotate_data();
        void rotate();
        void explode_data();
        void explode();
        void drawMove();
        void doMove();
        void doMove_data();

};

#endif
