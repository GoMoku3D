TEMPLATE = app
TARGET = GUITest

unix:LIBS += -lSoQt
macx:LIBS += -Wl,-framework,SoQt -Wl,-framework,Inventor -Wl,-framework,OpenGL

include(../tests.pri)

HEADERS += GUITest.h
SOURCES += GUITest.cpp
OBJECTS += ../../build/RenderWidget.o \
           ../../build/moc_RenderWidget.o \
           ../../build/CubeRenderArea.o \
           ../../build/SceneGraph.o \
           ../../build/Point.o \
           ../../build/GameMatrix.o \
           ../../build/Move.o \
           ../../build/SyncSharedCondition.o \
           ../../build/GUISettings.o
