/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef GAMELOOPTEST_H
#define GAMELOOPTEST_H

#include <QObject>
#include <QList>
#include <QTest>
#include <QSignalSpy>

#include "GameLoop.h"
#include "Player.h"
#include "PlayerStub.h"

class GameLoopTest : public QObject
{
    Q_OBJECT

    private:
        GameLoop *_gameLoop;
        QList<Player*> _players;
        QSignalSpy *_movedSpy;
        QSignalSpy *_turnSpy;
        QSignalSpy *_winSpy;
        QSignalSpy *_drawSpy;

    private slots:
		void initTestCase();
        void init();
        void cleanup();
        void gameLoop();
        void setTurn();
        void runWin();
        void runDraw();
};

Q_DECLARE_METATYPE(QList<int>)

#endif
