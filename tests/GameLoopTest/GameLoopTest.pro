TEMPLATE = app
TARGET = GameLoopTest
QT -= gui

include(../tests.pri)

HEADERS += GameLoopTest.h \
           PlayerStub.h
SOURCES += GameLoopTest.cpp \
           PlayerStub.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o \
           ../../build/GameLoop.o \
           ../../build/SyncSharedCondition.o \
           ../../build/Player.o \
           ../../build/moc_Player.o \
           ../../build/moc_GameLoop.o \
           ../../build/AIPlayer.o \
           ../../build/moc_AIPlayer.o \
           ../../build/CCThreatSearchAI.o \
           ../../build/ThreatSearchAI.o \
           ../../build/Threat.o \
           ../../build/CCThreat.o \
           ../../build/AI.o
