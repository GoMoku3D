/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "GameLoopTest.h"
#include "GameMatrix.h"

void GameLoopTest::initTestCase()
{
	qRegisterMetaType<Move>();
	qRegisterMetaType< QList<int> >();
}

void GameLoopTest::init()
{
    GameMatrix::create(5, 1, 2);
    _players.append(new PlayerStub(0));
    _players.append(new PlayerStub(1));
    _gameLoop = new GameLoop(_players);
    _movedSpy = new QSignalSpy(_gameLoop, SIGNAL(moved(Move)));
    _turnSpy = new QSignalSpy(_gameLoop, SIGNAL(turn(int)));
    _winSpy = new QSignalSpy(_gameLoop, SIGNAL(win(int)));
    _drawSpy = new QSignalSpy(_gameLoop, SIGNAL(draw(QList<int>)));

    //postcondizioni
    QVERIFY(_movedSpy->isValid() == true);
    QVERIFY(_turnSpy->isValid() == true);
    QVERIFY(_winSpy->isValid() == true);
    QVERIFY(_drawSpy->isValid() == true);
    QCOMPARE(_movedSpy->count(), 0);
    QCOMPARE(_turnSpy->count(), 0);
    QCOMPARE(_winSpy->count(), 0);
    QCOMPARE(_drawSpy->count(), 0);
}

void GameLoopTest::cleanup()
{
    _gameLoop->stop();
    _gameLoop->deleteLater();
    _players.clear();
    delete _movedSpy;
    delete _turnSpy;
    delete _winSpy;
    delete _drawSpy;

}

void GameLoopTest::gameLoop()
{
    //postcondizioni
    QVERIFY(_gameLoop->_currentPlayer == _players[0]);
    QCOMPARE(_gameLoop->_winStatus.size(), 2);
    QVERIFY(_gameLoop->_winStatus[0] == false);
    QVERIFY(_gameLoop->_winStatus[1] == false);
}

void GameLoopTest::setTurn()
{
    //ambiente
    _gameLoop->setTurn(1);
    //postcondizioni
    QVERIFY(_gameLoop->_currentPlayer == _players[1]);
}

void GameLoopTest::runWin()
{
    //ambiente
    QList<Point> list0;
    QList<Point> list1;
    list0.append(Point(0, 0, 0));
    list1.append(Point(1, 1, 1));
    list0.append(Point(1, 0, 0));
    list1.append(Point(2, 2, 2));
    list0.append(Point(2, 0, 0));
    list1.append(Point(3, 3, 3));
    list0.append(Point(3, 0, 0));
    list1.append(Point(4, 4, 4));
    list0.append(Point(4, 0, 0));
    list1.append(Point(4, 3, 3));
    dynamic_cast<PlayerStub*>(_players[0])->setPoints(list0);
    dynamic_cast<PlayerStub*>(_players[1])->setPoints(list1);
    _players[0]->moveToThread(_gameLoop);
    _players[1]->moveToThread(_gameLoop);
    _gameLoop->start();

    //postcondizioni
    QVERIFY(_gameLoop->wait() == true);
    QCOMPARE(_turnSpy->count(), 10);
    QCOMPARE(_movedSpy->count(), 10);
    QCOMPARE(_winSpy->count(), 1);
    QCOMPARE(_winSpy->at(0).at(0).toInt(), 0);
    QCOMPARE(_drawSpy->count(), 0);
    QCOMPARE(_turnSpy->at(0).at(0).toInt(), 0);
    QCOMPARE(_turnSpy->at(1).at(0).toInt(), 1);
    QCOMPARE(_turnSpy->at(2).at(0).toInt(), 0);
    QCOMPARE(_turnSpy->at(3).at(0).toInt(), 1);
    QCOMPARE(_turnSpy->at(4).at(0).toInt(), 0);
    QCOMPARE(_turnSpy->at(5).at(0).toInt(), 1);
    QCOMPARE(_turnSpy->at(6).at(0).toInt(), 0);
    QCOMPARE(_turnSpy->at(7).at(0).toInt(), 1);
    QCOMPARE(_turnSpy->at(8).at(0).toInt(), 0);
    QCOMPARE(_turnSpy->at(9).at(0).toInt(), 1);
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(0).at(0)).point(), Point(0, 0, 0));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(1).at(0)).point(), Point(1, 1, 1));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(2).at(0)).point(), Point(1, 0, 0));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(3).at(0)).point(), Point(2, 2, 2));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(4).at(0)).point(), Point(2, 0, 0));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(5).at(0)).point(), Point(3, 3, 3));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(6).at(0)).point(), Point(3, 0, 0));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(7).at(0)).point(), Point(4, 4, 4));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(8).at(0)).point(), Point(4, 0, 0));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(9).at(0)).point(), Point(4, 3, 3));
}

void GameLoopTest::runDraw()
{
    //ambiente
    QList<Point> list0;
    QList<Point> list1;
    list0.append(Point(0, 0, 0));
    list1.append(Point(1, 0, 1));
    list0.append(Point(1, 0, 0));
    list1.append(Point(1, 1, 1));
    list0.append(Point(2, 0, 0));
    list1.append(Point(1, 2, 1));
    list0.append(Point(3, 0, 0));
    list1.append(Point(1, 3, 1));
    list0.append(Point(4, 0, 0));
    list1.append(Point(1, 4, 1));
    dynamic_cast<PlayerStub*>(_players[0])->setPoints(list0);
    dynamic_cast<PlayerStub*>(_players[1])->setPoints(list1);
    _players[0]->moveToThread(_gameLoop);
    _players[1]->moveToThread(_gameLoop);
    _gameLoop->start();

    //postcondizioni
    QVERIFY(_gameLoop->wait() == true);
    QCOMPARE(_turnSpy->count(), 10);
    QCOMPARE(_movedSpy->count(), 10);
    QCOMPARE(_winSpy->count(), 0);
    QCOMPARE(_drawSpy->count(), 1);
    QCOMPARE(_turnSpy->at(0).at(0).toInt(), 0);
    QCOMPARE(_turnSpy->at(1).at(0).toInt(), 1);
    QCOMPARE(_turnSpy->at(2).at(0).toInt(), 0);
    QCOMPARE(_turnSpy->at(3).at(0).toInt(), 1);
    QCOMPARE(_turnSpy->at(4).at(0).toInt(), 0);
    QCOMPARE(_turnSpy->at(5).at(0).toInt(), 1);
    QCOMPARE(_turnSpy->at(6).at(0).toInt(), 0);
    QCOMPARE(_turnSpy->at(7).at(0).toInt(), 1);
    QCOMPARE(_turnSpy->at(8).at(0).toInt(), 0);
    QCOMPARE(_turnSpy->at(9).at(0).toInt(), 1);
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(0).at(0)).point(), Point(0, 0, 0));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(1).at(0)).point(), Point(1, 0, 1));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(2).at(0)).point(), Point(1, 0, 0));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(3).at(0)).point(), Point(1, 1, 1));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(4).at(0)).point(), Point(2, 0, 0));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(5).at(0)).point(), Point(1, 2, 1));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(6).at(0)).point(), Point(3, 0, 0));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(7).at(0)).point(), Point(1, 3, 1));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(8).at(0)).point(), Point(4, 0, 0));
    QCOMPARE(qvariant_cast<Move>(_movedSpy->at(9).at(0)).point(), Point(1, 4, 1));
}

QTEST_MAIN(GameLoopTest)
