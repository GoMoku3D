TEMPLATE = app
TARGET = CCThreatSearchAITest
QT -= gui

include(../tests.pri)

HEADERS += CCThreatSearchAITest.h
SOURCES += CCThreatSearchAITest.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o \
           ../../build/Threat.o \
           ../../build/CCThreat.o \
           ../../build/ThreatSearchAI.o \
           ../../build/CCThreatSearchAI.o \
           ../../build/AI.o
