/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "CCThreatSearchAITest.h"

void CCThreatSearchAITest::init()
{
    _mat = GameMatrix::create(5, 3, 2);
    _ai = new CCThreatSearchAI(0);
    _ai->createThreat();
}

void CCThreatSearchAITest::cleanup()
{
    if (_ai != 0) {
        delete _ai;
        _ai = 0;
    }
    GameMatrix::destroy();
    _mat = 0;
}

void CCThreatSearchAITest::defenseHook1()
{
    //ambiente
    _mat->add(Move(1, Point(0, 0, 0)));
    _mat->add(Move(1, Point(1, 0, 0)));
    _mat->add(Move(1, Point(2, 0, 0)));
    _mat->add(Move(1, Point(2, 1, 0)));
    _mat->add(Move(1, Point(3, 1, 0)));
    _mat->add(Move(1, Point(4, 1, 0)));
    _mat->add(Move(1, Point(4, 2, 0)));
    _mat->add(Move(1, Point(5, 2, 0)));
    _mat->add(Move(1, Point(6, 2, 0)));
    _mat->add(Move(1, Point(6, 3, 0)));
    _mat->add(Move(1, Point(8, 3, 0)));
    _mat->add(Move(1, Point(8, 4, 0)));
    _mat->add(Move(1, Point(9, 4, 0)));
    _mat->add(Move(1, Point(10, 4, 0)));
    _mat->add(Move(1, Point(10, 5, 0)));
    _mat->add(Move(1, Point(11, 5, 0)));
    _mat->add(Move(1, Point(12, 5, 0)));
    _mat->add(Move(1, Point(12, 6, 0)));
    _mat->add(Move(1, Point(13, 6, 0)));
    _mat->add(Move(0, Point(14, 0, 0)));
    _mat->add(Move(1, Point(14, 6, 0)));
    _ai->init();
    Point result = _ai->defenseHook();

    //postcondizioni
    QCOMPARE(result, Point(7, 3, 0));
}

void CCThreatSearchAITest::defenseHook2()
{
    //ambiente
    _mat->add(Move(1, Point(0, 0, 0)));
    _mat->add(Move(1, Point(1, 0, 0)));
    _mat->add(Move(1, Point(2, 0, 0)));
    _mat->add(Move(1, Point(2, 1, 0)));
    _mat->add(Move(1, Point(3, 1, 0)));
    _mat->add(Move(1, Point(4, 1, 0)));
    _mat->add(Move(1, Point(4, 2, 0)));
    _mat->add(Move(1, Point(5, 2, 0)));
    _mat->add(Move(1, Point(6, 2, 0)));
    _mat->add(Move(1, Point(6, 3, 0)));
    _mat->add(Move(1, Point(7, 3, 0)));
    _mat->add(Move(1, Point(8, 3, 0)));
    _mat->add(Move(1, Point(8, 4, 0)));
    _mat->add(Move(1, Point(9, 4, 0)));
    _mat->add(Move(1, Point(10, 4, 0)));
    _mat->add(Move(1, Point(10, 5, 0)));
    _mat->add(Move(1, Point(11, 5, 0)));
    _mat->add(Move(1, Point(12, 5, 0)));
    _mat->add(Move(1, Point(12, 6, 0)));
    _mat->add(Move(1, Point(13, 6, 0)));
    _mat->add(Move(0, Point(14, 0, 0)));
    _ai->init();
    Point result = _ai->defenseHook();

    //postcondizioni
    QCOMPARE(result, Point(14, 6, 0));
}

QTEST_MAIN(CCThreatSearchAITest)
