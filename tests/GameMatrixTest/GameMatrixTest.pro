TEMPLATE = app
TARGET = GameMatrixTest
QT -= gui

include(../tests.pri)

HEADERS += GameMatrixTest.h
SOURCES += GameMatrixTest.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o
