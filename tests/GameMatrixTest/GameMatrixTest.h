/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#ifndef GAMEMATRIXTEST_H
#define GAMEMATRIXTEST_H

#include <QObject>
#include <QTest>

#include "GameMatrix.h"

class GameMatrixTest : public QObject
{
    Q_OBJECT

    private:
        GameMatrix *_mat;

    private slots:
        void init();
        void cleanup();
        void create();
        void add();
        void clear();
        void check();
        void elementAt();
        void setLastRound();
};

#endif
