/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "GameMatrixTest.h"

void GameMatrixTest::init()
{
    _mat = GameMatrix::create(5, 1, 2);
}

void GameMatrixTest::cleanup()
{
    _mat = 0;
    GameMatrix::destroy();
}

void GameMatrixTest::create()
{
    //postcondizioni
    QVERIFY(GameMatrix::_instance != 0);
    QCOMPARE(GameMatrix::EmptyPoint, -1);
    QCOMPARE(_mat->_lastRound.size(), 2);
    QCOMPARE(_mat->_lastRound[0], Point());
    QCOMPARE(_mat->_lastRound[1], Point());
    QCOMPARE(_mat->_d1, 5);
    QCOMPARE(_mat->_d2, 1);
    QCOMPARE(_mat->_freeCounter, 125);
    QVERIFY(_mat->_matrix != 0);
    for (int i = 0; i < 125; i++) {
        QCOMPARE(_mat->_matrix[i], -1);
    }
}
void GameMatrixTest::add()
{
    //ambiente
    _mat->add(Move(0, Point(0, 0, 0)));

    //postcondizioni
    QVERIFY(GameMatrix::_instance != 0);
    QCOMPARE(_mat->_lastRound[0], Point(0, 0, 0));
    QCOMPARE(_mat->_lastRound[1], Point());
    QCOMPARE(_mat->_d1, 5);
    QCOMPARE(_mat->_d2, 1);
    QCOMPARE(_mat->_freeCounter, 124);
    QVERIFY(_mat->_matrix != 0);
    QCOMPARE(_mat->_matrix[0], 0);
    for (int i = 1; i < 125; i++) {
        QCOMPARE(_mat->_matrix[i], -1);
    }
}

void GameMatrixTest::clear()
{
    //ambiente
    _mat->add(Move(0, Point(0, 0, 0)));
    _mat->clear(Point(0, 0, 0));
    //postcondizioni
    QVERIFY(GameMatrix::_instance != 0);
    QCOMPARE(_mat->_d1, 5);
    QCOMPARE(_mat->_d2, 1);
    QCOMPARE(_mat->_freeCounter, 125);
    QVERIFY(_mat->_matrix != 0);
    for (int i = 0; i < 125; i++) {
        QCOMPARE(_mat->_matrix[i], -1);
    }
}
void GameMatrixTest::check()
{
    //ambiente
    _mat->add(Move(0, Point(1, 1, 1)));
    //postcondizioni
    GameMatrix *mat = GameMatrix::_instance;
    QVERIFY(mat != 0);
    QVERIFY(mat->check(Point(1, 1, 1)) == false);
    QVERIFY(mat->check(Point(0, 0, 0)) == true);
    QVERIFY(mat->check(Point(5, 5, 5)) == false);
    QVERIFY(mat->check(Point(-1, -1, -1)) == false);
}

void GameMatrixTest::elementAt()
{
    //ambiente
    _mat->add(Move(0, Point(1, 1, 1)));
    //postcondizioni
    QCOMPARE(_mat->_matrix[31], _mat->elementAt(1, 1, 1));
    QCOMPARE(_mat->_matrix[31], _mat->elementAt(Point(1, 1, 1)));
    QCOMPARE(_mat->_matrix[0], _mat->elementAt(0, 0, 0));
    QCOMPARE(_mat->_matrix[0], _mat->elementAt(Point(0, 0, 0)));
    QCOMPARE(_mat->elementAt(-1, -1, -1), -2);
    QCOMPARE(_mat->elementAt(Point(-1, -1, -1)), -2);
    QCOMPARE(_mat->elementAt(5, 5, 5), -2);
    QCOMPARE(_mat->elementAt(Point(5, 5, 5)), -2);
}

void GameMatrixTest::setLastRound()
{
    //ambiente
    QList<Point> list;
    list.append(Point(0, 0, 0));
    list.append(Point(0, 0, 1));
    _mat->setLastRound(list);
    //postcondizioni
    QCOMPARE(list[0], Point(0, 0, 0));
    QCOMPARE(list[1], Point(0, 0, 1));
    QCOMPARE(_mat->_lastRound[0], list[0]);
    QCOMPARE(_mat->_lastRound[1], list[1]);
}

QTEST_MAIN(GameMatrixTest)
