/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "SuggesterTest.h"
#include "GameMatrix.h"

void SuggesterTest::initTestCase()
{
    qRegisterMetaType<Point>();
}

void SuggesterTest::init()
{
    GameMatrix::create(5, 1, 2);
    _testedObject = new Suggester(0);
    _spy = new QSignalSpy(_testedObject, SIGNAL(suggestedMoveReady(Point)));
    //postcondizioni
    QVERIFY(_spy->isValid() == true);
    QCOMPARE(_spy->count(), 0);
}

void SuggesterTest::cleanup()
{
    _testedObject->deleteLater();
    delete _spy;
    GameMatrix::destroy();
}

void SuggesterTest::suggestedMoveReady()
{
    _testedObject->start();

    QVERIFY(_testedObject->wait() == true);
    QCOMPARE(_spy->count(), 1);
    QCOMPARE(_spy->at(0).at(0).typeName(), "Point");
}

QTEST_MAIN(SuggesterTest)
