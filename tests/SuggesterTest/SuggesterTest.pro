TEMPLATE = app
TARGET = SuggesterTest
QT -= gui

include(../tests.pri)

HEADERS += SuggesterTest.h
SOURCES += SuggesterTest.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o \
           ../../build/Threat.o \
           ../../build/CCThreat.o \
           ../../build/ThreatSearchAI.o \
           ../../build/CCThreatSearchAI.o \
           ../../build/AI.o \
           ../../build/Suggester.o \
           ../../build/moc_Suggester.o
