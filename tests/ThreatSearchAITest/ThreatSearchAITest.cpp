/********************************************************************
 *
 * Copyright (C) 2008  Daniele Battaglia
 *
 * This file is part of GoMoku3D.
 *
 * GoMoku3D is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * GoMoku3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoMoku3D. If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************/

#include "ThreatSearchAITest.h"

void ThreatSearchAITest::init()
{
    _mat = GameMatrix::create(5, 2, 2);
    _ai = new ThreatSearchAI(0);
    _ai->createThreat();
}

void ThreatSearchAITest::cleanup()
{
    if (_ai != 0) {
        delete _ai;
        _ai = 0;
    }
    GameMatrix::destroy();
    _mat = 0;
}

void ThreatSearchAITest::move()
{
    //ambiente
    _mat->add(Move(0, Point(0, 0, 0)));
    _mat->add(Move(1, Point(0, 0, 1)));
    _mat->add(Move(0, Point(1, 0, 0)));
    _mat->add(Move(1, Point(0, 0, 2)));
    _mat->add(Move(0, Point(2, 0, 0)));
    _mat->add(Move(1, Point(0, 1, 0)));
    _mat->add(Move(0, Point(3, 0, 0)));
    _mat->add(Move(1, Point(0, 2, 0)));
    _ai->init();
    Point result = _ai->move(1);

    //postcondizioni
    QCOMPARE(result, Point(4, 0, 0));
}

void ThreatSearchAITest::defense1()
{
    //ambiente
    _mat->add(Move(1, Point(0, 0, 0)));
    _mat->add(Move(0, Point(0, 0, 1)));
    _mat->add(Move(1, Point(1, 0, 0)));
    _mat->add(Move(0, Point(0, 0, 2)));
    _mat->add(Move(1, Point(2, 0, 0)));
    _mat->add(Move(0, Point(0, 1, 0)));
    _ai->init();
    Point result = _ai->move(4);

    //postcondizioni
    QVERIFY(result == Point(4, 0, 0) || result == Point(3, 0, 0));
}

void ThreatSearchAITest::defense2()
{
    _mat->add(Move(1, Point(0, 0, 0)));
    _mat->add(Move(0, Point(0, 0, 1)));
    _mat->add(Move(1, Point(1, 0, 0)));
    _mat->add(Move(0, Point(0, 0, 2)));
    _mat->add(Move(1, Point(2, 0, 0)));
    _mat->add(Move(0, Point(0, 1, 0)));
    _mat->add(Move(1, Point(3, 0, 0)));
    _mat->add(Move(0, Point(0, 2, 0)));
    _ai->init();
    Point result = _ai->move(1);

    //postcondizioni
    QCOMPARE(result, Point(4, 0, 0));
}

QTEST_MAIN(ThreatSearchAITest)
