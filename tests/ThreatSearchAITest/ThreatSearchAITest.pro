TEMPLATE = app
TARGET = ThreatSearchAITest
QT -= gui

include(../tests.pri)

HEADERS += ThreatSearchAITest.h
SOURCES += ThreatSearchAITest.cpp
OBJECTS += ../../build/Point.o \
           ../../build/Move.o \
           ../../build/GameMatrix.o \
           ../../build/Threat.o \
           ../../build/ThreatSearchAI.o \
           ../../build/AI.o
