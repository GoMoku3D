TEMPLATE = subdirs
SUBDIRS = PointTest \
          MoveTest \
          GameMatrixTest \
          SyncSharedConditionTest \
          GameLoopTest \
          ThreatTest \
          ThreatSearchAITest \
          CCThreatTest \
          CCThreatSearchAITest \
          SuggesterTest \
          GUITest \
          StreamSocketTest \
          ClientSocketTest
