\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{appendix}
\usepackage{fancyhdr}
\usepackage{filecontents}
\usepackage{graphicx}
\usepackage{lastpage}
\usepackage{ltxtable}
\usepackage{ulem}
\usepackage[pdftex]{hyperref}

\hypersetup{
	breaklinks,
	colorlinks,
	linkcolor=blue,
	pdftitle={Piano\_di\_Qualifica\_v4.0.pdf},
	pdfsubject={Piano di Qualifica},
	pdfauthor={ITWorks!},
	pdfcreator={TeXLive-2007}
}

\renewcommand{\appendixtocname}{Appendici}
\renewcommand{\appendixpagename}{Appendici}
\renewcommand{\headrulewidth}{0.6pt}
\renewcommand{\footrulewidth}{0.4pt}
\renewcommand{\labelitemiii}{$\triangleright$}

\begin{document}

\normalem
\newtoks\titolo
\titolo{Piano di Qualifica}
\newtoks\data
\data{\today}

\pagestyle{fancy}
\lhead{\includegraphics[scale=0.3]{../Logo_simple}}
\chead{}
\rhead{\small{itworks@googlegroups.com}}
\lfoot{\small{\the\titolo}}
\cfoot{}
\rfoot{\thepage\ di \pageref*{LastPage}}

\thispagestyle{empty}

\begin{center}

\includegraphics[scale=0.7]{../Logo_green}
\vspace{2cm}
\\\Huge{\textsc{\the\titolo}}
\vspace{1cm}
\\\Large{\textsl{\the\data}}
\vspace{1.2cm}

\begin{abstract}
Viene data una visione globale della strategia di verifica che la nostra azienda intende attuare nel corso della realizzazione del progetto \mbox{\textsl{GoMoku3D}}. Questo implica anche la specifica delle risorse, degli strumenti e delle tecnologie richieste.
\end{abstract}

\small
\vspace*{\stretch{1}}
\begin{tabular}{r|l}
	\textbf{Redazione} & Martina Astegno \\
						& Daniele Battaglia \\
						& Davide Pesavento \\
						& Tobia Zorzan \\
	\textbf{Revisione} & Martina Bernardini \\
						& Davide Pesavento \\
						& Martina Astegno \\
						& Nicolò Navarin \\
	\textbf{Approvazione} & Martina Bernardini \\
						& Nicolò Navarin \\
						& Martina Astegno \\
                        & Daniele Battaglia \\
	\textbf{Versione} & $4.0$ \\
	\textbf{Data} & \the\data \\
	\textbf{Stato} & Formale \\
	\textbf{Uso} & Esterno \\
	\textbf{Distribuzione} & Prof.\ Tullio Vardanega \\
							& Prof.\ Renato Conte \\
							& ITWorks! \\
\end{tabular}
\vspace{\stretch{1}}

\end{center}

\pagebreak

\normalsize
\vspace*{0.1cm}
\tableofcontents

\pagebreak

\listoffigures

\pagebreak

\section{Introduzione}

\subsection{Scopo del documento}
L'obiettivo del presente documento è di chiarire in modo preciso le tecniche di verifica e validazione che l'azienda ritiene di dover mettere in atto, in particolare quando intervenire in base alle revisioni previste. Verranno inoltre fissati anche i ruoli, responsabili di mettere in atto tali tecniche.

\subsection{Scopo del prodotto}
L'azienda \textsl{ITWorks!} si prefigge come scopo la realizzazione di un prodotto che risponda ai requisiti individuati in fase di analisi, in particolare si tratta di un sistema software per l'implementazione del gioco chiamato \mbox{\textbf{GoMoku3D}}. Pertanto si può far riferimento direttamente alla documentazione relativa all'\textit{Analisi dei Requisiti}.

\subsection{Glossario}
Il \textit{Glossario} cui far riferimento è fornito in un file separato, allegato al presente \textit{Piano di Qualifica}.
Le definizioni del glossario si applicano a tutti i documenti formali allo scopo di rendere non ambigua e omogenea la terminologia tecnica utilizzata.

\subsection{Riferimenti}
\subsubsection{Normativi}
\begin{itemize}
\item Capitolato d'appalto. \\ \url{http://www.math.unipd.it/~tullio/IS-1/2007/Progetti/GoMoku3D.html}
\item IEEE Std 1008-87 - \textit{Standard for Software Unit Testing}
\item IEEE Std 730TM-2002 (revision of IEEE Std 730-1998) - \textit{Standard for Software Quality Assurance Plans}
\end{itemize}
\subsubsection{Informativi}
\label{txt:links}
\begin{itemize}
\item \textit{Guide to the SWEBOK.} \\
\url{http://www.swebok.org/}
\item Documentazione di QTestLib versione 4.3. \\
\url{http://doc.trolltech.com/4.3/qtestlib-manual.html}
\item Pagina web del docente. \\
\url{http://www.math.unipd.it/~tullio/}
\end{itemize}

\pagebreak

\section{Visione generale della strategia di verifica}

\subsection{Organizzazione, pianificazione strategica e temporale, responsabilità}
\label{resp}
Verifica e validazione sono attività essenziali nel processo di qualifica e in quanto tali hanno bisogno di essere organizzate e fissate sulla base di particolari criteri stabiliti all'interno dell'azienda. Questo compito è affidato al responsabile, che in prima persona o delegando l'amministratore, specifica la ripartizione in moduli delle attività di verifica che devono essere messe in atto. Fin da subito, e quindi nell'\textit{Analisi dei Requisiti} si manifesta la necessità di un intervento da parte del verificatore, che dovrà accertare la presenza dei soli requisiti necessari e sufficienti al prodotto, inclusi quelli introdotti direttamente dal fornitore a integrarne il valore aggiunto.
\`E compito del verificatore evidenziare eventuali anomalie o non conformità alle \textit{Norme di Progetto} riscontrate nel corso della sua attività. Tutte queste incongruenze, opportunamente comunicate a chi ha redatto il documento, dovranno essere risolte e documentate come previsto nella sezione \ref{anomalie}. Quanto detto alla sezione \ref{anomalie} non si applica nel caso di difetti di forma, infatti basterà notificare all'autore del documento la non conformità del suddetto, affinché proceda ad effettuare le modifiche necessarie, il tutto per evitare un sovraccarico al sistema di \uline{ticketing}.
La seguente tabella indica in modo dettagliato il momento e gli incaricati a svolgere le attività di verifica e validazione. Vista la scelta del modello di ciclo di vita incrementale vi sarà un'unica fase di \textit{Analisi} iniziale, mentre la sequenza \textit{Progettazione-Programmazione-Qualifica} saranno ripetute come indicato nel \textit{Piano di Progetto}.
\\\\
\begin{tabularx}{\textwidth}{l|X}
	\hline
	\textbf{Analisi} & Tracciamento tra requisiti utente e requisiti software. \\
	\hline
	\textbf{Progettazione} & Tracciamento tra requisiti software e descrizione dei componenti, definizione dei test \mbox{\textit{Driver \& Stub}} che verranno effettuati in fasi future. \\
	\hline
	\textbf{Programmazione} & Test \textit{white box} eseguiti con l'ausilio di \textit{debugger}. \\
	\hline
	\textbf{Qualifica} & Esecuzione dei test \mbox{\textit{Driver \& Stub}} precedentemente definiti, oppure, in fase di validazione del sistema, $\alpha$-test, e relativa documentazione. \\
	\hline
\end{tabularx}
\\\\

\subsection{Dettaglio delle revisioni}
L'azienda dovrà confrontarsi con due tipologie di processi di revisione, quali:
\begin{description}
	\item[Revisione formale:] fondamentalmente di carattere bloccante, condotta direttamente dal cliente che gode di piena autorità (\textit{Audit Process}):
		\begin{itemize}
			\item Revisione dei Requisiti (RR)
			\item Revisione di Accettazione (RA)
		\end{itemize}
	\item[Revisione informale:] di progresso, non è bloccante, ma consente maggiore interazione tra cliente e fornitore (\textit{Joint Review Process}):
		\begin{itemize}
			\item Revisione del Progetto Preliminare (RPP)
			\item Revisione del Progetto Definitivo (RPD)\footnote{Questa revisione verrà effettuata in modo interno senza l'intervento del committente, come con esso accordato.}
			\item Revisione di Qualifica (RQ)
		\end{itemize}
\end{description}
Tutti i documenti redatti in sede di \textit{Audit Process} hanno valenza contrattuale, mentre quelli relativi al \textit{Joint Review Process} hanno solo scopo informativo.

\subsubsection{Revisione dei Requisiti}
\begin{itemize}
	\item Prodotti in ingresso: Capitolato d'Appalto, Analisi dei Requisiti e Piano di Progetto.
	\item Funzioni: accordarsi con il committente sulle funzionalità richieste al prodotto.
	\item Stato del prodotto in uscita: descritto.
\end{itemize}

\subsubsection{Revisione del Progetto Preliminare}
\begin{itemize}
	\item Prodotti in ingresso: Specifica tecnica, aggiornamento del Piano di Qualifica.
	\item Funzioni: accertamento di realizzabilità (esistenza di strategie, tecniche, metodologie e tecnologie adeguate allo scopo), e attivazione della fase realizzativa del prodotto.
	\item Stato del prodotto in uscita: specificato.
\end{itemize}

\subsubsection{Revisione del Progetto Definitivo}
\begin{itemize}
	\item Prodotti in ingresso: Definizione del Prodotto, aggiornamento del Piano di Qualifica.
	\item Funzioni: informare il committente sulle caratteristiche finali del prodotto e attivare la fase ascendente di qualifica.
	\item Stato del prodotto in uscita: definito.
\end{itemize}

\subsubsection{Revisione di Qualifica}
\begin{itemize}
	\item Prodotti in ingresso: aggiornamento del Piano di Qualifica, versione preliminare del Manuale Utente e specifica delle prove proposte dal fornitore per il collaudo.
	\item Funzioni: approvazione dell'esito finale della fase di verifica, attivazione della fase di validazione.
	\item Stato del prodotto in uscita: qualificato.
\end{itemize}

\subsubsection{Revisione di Accettazione}
\begin{itemize}
	\item Prodotti in ingresso: versione definitiva del Piano di Qualifica, versione definitiva del Manuale Utente.
	\item Funzioni: collaudo del sistema per accettazione da parte del committente, accertamento di soddisfacimento di tutti i requisiti pattuiti in sede contrattuale.
	\item Stato del prodotto in uscita: accettato.
\end{itemize}

\subsection{Risorse necessarie e disponibili}
L'attività di verifica per essere concretizzata necessita di risorse di natura principalmente umana e tecnologica. Pertanto è compito dell'amministratore accertare in ogni momento l'efficienza e l'affidabilità dei mezzi tecnologici, in modo da consentire al verificatore di svolgere il proprio lavoro al meglio. Il responsabile ha il compito di controllare che le scadenze siano rispettate in modo da non ritardare l'attività di qualifica.

\subsection{Strumenti, tecniche, metodi}
\subsubsection{Tracciamento}
Al fine di evitare software ridondante o che presenta parti non richieste è indispensabile avere a disposizione il tracciamento completo e preciso di tutti i requisiti rilevati, anche in previsione di eventuali cambiamenti futuri opportunamente gestiti (\uline{ticketing}). I documenti di tracciamento dovranno essere redatti dai ruoli indicati nella sezione \ref{resp} e il compito del verificatore sarà quello di controllare che non esistano incoerenze e che il prodotto soddisfi tutti e soli i requisiti tracciati.
\subsubsection{Documenti}
Per quanto riguarda i documenti il verificatore è chiamato a controllare che:
\begin{itemize}
	\item il linguaggio usato non sia ambiguo;
	\item il documento non presenti errori linguistici;
	\item il contenuto sia corretto e completo rispetto a quanto richiesto dalla natura del documento stesso.
\end{itemize}
Particolare attenzione verrà data ai documenti di \textit{Analisi dei Requisiti} e \textit{Specifica Tecnica} per i quali il controllo sul contenuto dovrà comprendere metriche di valutazione specifiche. Per l'\textit{Analisi dei Requisiti} verrà posta particolare attenzione sull'atomicità dei singoli requisiti mentre per la \textit{Specifica Tecnica} dovrà essere verificato che il livello di accoppiamento sia mantenuto ragionevolmente basso, che il riuso di componenti esistenti sia fatto in modo non opportunistico e che siano applicati principi di progettazione ritenuti utili come ad esempio il principio di \textit{encapsulation}.

\subsubsection{Codice}
Partendo dal presupposto che il sistema sia suddiviso in unità, cioè in elementi che rappresentano le funzionalità di specifiche componenti, l'attuazione della verifica richiede essenzialmente due generi di attività distinte, che possiamo ricondurre a:
\begin{itemize}
	\item \textbf{Analisi Statica}: viene fatta senza richiedere esecuzione di codice, ed essendo particolarmente onerosa e complessa è mirata a precise componenti. Verrà effettuata con le tecniche di:
		\begin{itemize}
			\item Analisi di flusso dei dati, per individuare l'uso di variabili non inizializzate correttamente.
			\item Analisi di flusso di controllo, per individuare l'esistenza di rami irraggiungibili del codice.
			\item \textit{Inspection} o \textit{walkthrough}, a discrezione del verificatore e della sua esperienza. \`E fortemente consigliato il metodo di \textit{inspection}; nel caso in cui si adotti la tecnica di \textit{walkthrough}, questo dovrà essere approvato dal responsabile che ne valuterà l'impatto in termini di costi e tempi, comunque in parte già previsti.
		\end{itemize}
	\item \textbf{Analisi Dinamica}: prevede l'esecuzione di test a due livelli:
		\begin{itemize}
			\item Unità: testate con il metodo \mbox{\textit{Driver \& Stub}} dove e come definito in sede di progettazione.
			\item Sistema: validato sui requisiti software con l'ausilio di $\alpha$-test e sui requisiti utente con l'ausilio di $\beta$-test.
		\end{itemize}
Grazie al $\beta$-test saremo in grado di raccogliere opinioni sull'usabilità del prodotto dal campione di test, mentre con l'$\alpha$-test, interno all'azienda, cercheremo di valutare i risultati raggiunti dal prodotto in termini di affidabilità ed efficienza e rilevare eventuali non conformità funzionali.
Per valutare l'effettiva utilità dei test verrà tenuta traccia del numero di difetti rilevati sul codice rispetto al tempo di prova e tali informazioni dovranno essere fornite dall'amministratore al responsabile. L'amministratore è quindi tenuto a realizzare tali statistiche a partire dai dati presenti nel sistema di \uline{ticketing}.
Al fine di ricevere del \textit{feedback} sul prodotto da parte di terzi, il \textit{software} \mbox{\textsl{GoMoku3D}} verrà distribuito ad un numero di volontari esterni già selezionati dall'azienda in precedenza. Questa \texttt{release} preliminare verrà effettuata non appena il prodotto sarà utilizzabile nella modalità \textit{stand-alone} e avrà passato con successo i test di unità previsti per le componenti \textsl{Core} e \textsl{GUI}.
Saranno poi raccolte le valutazioni dei volontari riguardo l'usabilità dell'interfaccia grafica fornita, la completezza e la chiarezza del \textsl{Manuale Utente} e l'eventuale riscontro di comportamenti anomali dell'applicazione durante l'installazione e l'utilizzo.
\end{itemize}

\section{Gestione amministrativa delle revisioni}

\subsection{Comunicazione e risoluzione di anomalie}
\label{anomalie}
Eventuali anomalie possono essere riscontrate non solo nel corso dell'attività di verifica, ma anche in altri momenti del ciclo di vita. Ciascun membro del gruppo ha perciò facoltà di provvedere alla comunicazione di quanto riscontrato anche se non in veste di verificatore.
\subsubsection{Categorie}
Le anomalie sono raggruppate nelle seguenti categorie in base alla loro origine:
\begin{itemize}
	\item \textbf{Analisi}: errori riscontrati sulla fase di analisi, elementi superflui, incoerenze di vario genere.
	\item \textbf{Progettazione}: errori riscontrati sulla fase di progettazione, elementi superflui, incoerenze di vario genere.
	\item \textbf{Sviluppo}: errori riscontrati sulla fase di codifica, plausibilmente nella fase di test.
\end{itemize}
\subsubsection{Comunicazione iniziale}
La comunicazione delle anomalie riscontrate dovrà essere effettuata usando lo strumento di gestione automatica fornito all'indirizzo: \url{http://gomoku3d.devjavu.com}. Ciascun componente del gruppo dispone di un nome utente e di una password da usare per l'autenticazione nel sistema. Per comunicare una nuova anomalia sarà necessario aprire un nuovo \uline{ticket}. Nel creare un nuovo ticket si dovranno rispettare le seguenti linee guida:
\begin{itemize}
	\item inserire un titolo il più sintetico ed esemplificativo possibile;
	\item essere chiari e diretti nella descrizione del problema;
	\item fare uso di termini e riferimenti non ambigui nella descrizione;
	\item se già individuato, indicare il file a cui devono essere apportate modifiche;
	\item indicare la categoria di appartenenza dell'anomalia;
	\item assegnare inizialmente il ticket all'indirizzo del gruppo \href{mailto:itworks.swe@gmail.com}{itworks.swe@gmail.com}, affinché l'aggiunta del nuovo ticket venga comunicata automaticamente a tutti i componenti.
\end{itemize}
\subsubsection{Gestione dell'anomalia e trattamento delle discrepanze}
Successivamente il responsabile o un suo delegato verificherà che effettivamente l'anomalia è presente, ed assegnerà il compito di risolvere l'anomalia ad uno dei componenti del gruppo. Tale procedura può essere eseguita sempre con il sistema automatizzato, il quale comunicherà al nuovo proprietario ogni aggiornamento. Nel caso la risoluzione di un'anomalia richieda più di tre giorni lavorativi, il proprietario del ticket è invitato a segnalare nel ticket stesso eventuali aggiornamenti e procedure in atto. Quando il proprietario di un ticket ritiene risolta l'anomalia, lo comunicherà impostando lo stato del ticket a \textit{``fixed''}. I verificatori hanno il dovere di controllare l'effettiva risoluzione dell'anomalia.

\subsection{Procedure di controllo di qualità di processo}
Al fine di migliorare efficacia ed efficienza dei processi l'azienda ha deciso di applicare ove opportuno delle tecniche volte a realizzare il meccanismo del ciclo \uline{PDCA}.
\subsubsection{Miglioramento di processo}
Le fasi logiche in cui si suddivide il meccanismo di miglioramento possono essere così riassunte:
\begin{itemize}
	\item \textbf{Misurazione}: quantitativa, basata su metriche.
	\item \textbf{Analisi e Modellazione}: si cerca di capire la struttura del processo, eventualmente attraverso un diagramma e, in base alle misurazioni e alla loro interpretazione, si prova ad individuare in che punto di tale struttura esiste margine di miglioramento per il processo.
	\item \textbf{Cambiamento}: viene apportato sulla base dei risultati del punto precedente, garantendo comunque la misurabilità dell'effetto migliorativo.
Può essere fatto tramite:
		\begin{itemize}
			\item ordinamento delle attività;
			\item introduzione o rimozione di uscite del processo;
			\item definizione di nuovi ruoli e responsabilità.
		\end{itemize}
\end{itemize}
\subsubsection{Metriche}
Con questo termine si intende l'insieme di parametri misurabili su un processo. Questi possono essere riassunti in:
\begin{itemize}
	\item \textbf{Tempo}: rappresenta l'ammontare totale delle ore effettivamente utilizzate dall'avvio alla terminazione del processo.
	\item \textbf{Risorse}: rappresenta il numero di risorse umane e strumenti utilizzati nel processo.
	\item \textbf{Numero di difetti} (o non conformità rispetto alle norme): riscontrate nello svolgimento del processo.
\end{itemize}
A tal proposito, l'azienda ha deciso di utilizzare come strumento di supporto "cccc", un tool per il calcolo delle metriche sul codice sorgente.

\subsection{Procedure e strumenti di misurazione di qualità di prodotto}
Per assicurare che il prodotto realizzato soddisfi a pieno i requisiti di qualità previsti, l'azienda \textsl{ITWorks!} ha deciso di utilizzare il tool \textbf{cccc}, che valuta i file sorgente sulla base di diverse metriche. Tra quelle messe a disposizione, ne abbiamo adottate alcune, e precisamente:\\
\begin{itemize}
\item Complessità ciclomatica: è una metrica strutturale relativa al flusso di controllo di un programma e rappresenta la sua complessità logica, cioè lo sforzo per realizzarlo e comprenderlo.
\item Linee di codice: individua l'estensione del codice preso in esame.
\item Linee di commenti: individua la quantità di commenti presenti nel codice; questi ultimi infatti sono particolarmente utili per la comprensione del codice stesso, soprattutto se utilizzato da terze persone.
\item Rapporto tra linee di codice e linee di commenti: individua quante linee di commenti ci sono rapportandole al numero di linee di codice presenti.
\end{itemize}
Nelle tabelle che seguono, la linea continua che compare in corrispondenza del rapporto SLOC/COM sta ad indicare che il numero di linee di commenti è superiore rispetto alle linee di codice esistenti; la prima entrata della tabella indica invece la complessità ciclomatica media dei metodi della classe considerata.

\subsubsection{Valori metriche AI}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|}
\hline
\hline
\textit{\textbf{Classe}} & Complessità  & Linee di  & Linee di   & SLOC/COM  \\
\textit{\textbf{}} & ciclomatica & codice(SLOC) & commenti(COM) & (SL\_C) \\
\hline
\hline
AI			   & 0.33 	& 11	& 20	& ---	\\
\hline
CCThreatSearchAI	& 3.50	& 60	& 20	& 3.00	\\
\hline
Suggester		& 0.50	& 24	& 20	& 1.20	\\
\hline
Threat			& 3.82	& 185	& 20	& 9.25	\\
\hline
CCThreat		& 2.86	& 92	& 20	& 4.60	\\
\hline
ThreatSearchAI		& 5.82	& 210	& 20	& 10.50	\\
\hline
\end{tabular}
\end{center}

Nella tabella la complessità ciclomatica assume valori più alti in corrispondenza delle classi \texttt{CCThreatSearchAI}, \texttt{Threat},  \texttt{CCThreat} e \texttt{ThreatSearchAI} perché in ognuna sono presenti almeno tre cicli \texttt{for} annidati e viene effettuato molto spesso il \textit{pattern matching}. Tuttavia abbiamo cercato di mantenerla ad un livello accettabile distribuendo il carico di lavoro tra diverse classi.\\

\subsubsection{Valori metriche Core}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|}
\hline
\hline
\textit{\textbf{Classe}} & Complessità  & Linee di  & Linee di   & SLOC/COM  \\
\textit{\textbf{}} & ciclomatica & codice(SLOC) & commenti(COM) & (SL\_C) \\
\hline
\hline
AIPlayer		& 0.43	& 36	& 20	& 1.80	\\
\hline
GameLoop		& 3.44	& 130	& 23	& 5.65	\\
\hline
GameMatrix		& 2.29	& 160	& 20	& 8.00	\\
\hline
GUISettings		& 0.50	& 40	& 20	& 2.00	\\
\hline
HumanPlayer		& 0.33	& 12	& 20	& ---	\\
\hline
LocalSettings		& 1.17	& 48	& 20	& 2.40	\\
\hline
Move			& 1.17	& 25	& 20	& 1.25	\\
\hline
Player			& 0.50	& 16	& 20	& ---	\\
\hline
Point			& 2.17	& 62	& 1	& 62.00 \\
\hline
RemotePlayer		& 0.67	& 15	& 20	& --- \\
\hline
ServerSettings		& 0.79	& 76	& 20	& 3.80 \\
\hline
SynchSharedCondition	& 0.80	& 25	& 20	& 1.25 \\
\hline
\end{tabular}
\end{center}

In questo caso l'unica classe con complessità ciclomatica relativamente alta è \texttt{GameLoop}; questo perché essa deve gestire tutti i possibili messaggi asincroni che arrivano dalla rete e dalla interfaccia grafica e anche perché sono stati utilizzati diversi costrutti condizionali.\\

\subsubsection{Valori metriche GUI}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|}
\hline
\hline
\textit{\textbf{Classe}} & Complessità  & Linee di  & Linee di   & SLOC/COM  \\
\textit{\textbf{}} & ciclomatica & codice(SLOC) & commenti(COM) & (SL\_C) \\
\hline
\hline
MainWindow			& 4.40	& 704	&50	&14.08	\\
\hline
OnlineDialog			& 0	& 3	& 20	& ---	\\
\hline
PlayerInfo			& 0.60	& 21	& 20	& 1.05	\\
\hline
PlayersWidget			& 2.75	& 84	& 20	& 4.20	\\
\hline
Preferences			& 1.50	& 28	& 24	& 1.17	\\
\hline
RenderWidget			& 1.27	& 102	& 87	& 1.17	\\
\hline
SceneGraph			& 2.61	& 476	& 61	& 7.80	\\
\hline
ServerSettingsDialog		& 5.44	& 311	& 21	& 14.81\\
\hline
StandAloneDialog		& 4.50	& 120	& 20	& 6.00	\\
\hline
TimerWidget			& 0.40	& 35	& 22	& 1.59	\\
\hline
ChatWidget			& 0	& 26	& 20	& 1.30	\\
\hline
ColorLabel			& 0.83	& 34	& 20	& 1.70	\\
\hline
CubeRenderArea			& 6.11  & 727	& 79	& 9.20	\\
\hline
HistoryModel			& 2.08	& 85	& 20	& 4.25	\\
\hline
\end{tabular}
\end{center}

Per quanto riguarda la componente \textit{GUI}, due sono le classi con una complessità ciclomatica relativamente elevata:
\begin{itemize}
\item \texttt{CubeRenderArea}: ci sono diversi costrutti \textit{switch case} per poter gestire le varie angolazioni della visuale e tutte le possibili interazioni. 
\item \texttt{MainWindow}: controlla la consistenza dei dati relativi alla grafica.
\end{itemize}

\subsubsection{Valori metriche Network}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|}
\hline
\hline
\textit{\textbf{Classe}} & Complessità  & Linee di  & Linee di   & SLOC/COM  \\
\textit{\textbf{}} & ciclomatica & codice(SLOC) & commenti(COM) & (SL\_C) \\
\hline
\hline
ClientSocket	& 1.50	& 35	& 20	& 1.75	\\
\hline
GameClient	& 1.14	& 67	& 22	& 3.05	\\
\hline
GameServer	& 4.18	& 239	& 35	& 6.83	\\
\hline
ServerSocket	& 1.13	& 60	& 21	& 2.86	\\
\hline
StreamSocket	& 3.50	& 172	& 5	& 34.40\\
\hline
\end{tabular}
\end{center}

Per quanto riguarda la componente \textit{Network}, due sono le classi con una complessità ciclomatica relativamente elevata:
\begin{itemize}
\item \texttt{StreamSocket}: il valore della complessità ciclomatica è dovuto alla gestione del \textit{parser} del flusso dati XML.
\item \texttt{GameServer}: questa classe deve controllare i dati forniti dal \textit{Client} prima di poter accettare giocatori e/o spettatori.
\end{itemize}

\section{Resoconto delle attività di verifica}
\subsection{Tracciamento componenti $\leftrightarrow$ requisiti}
Nella fase di progettazione ciascun componente, classe o metodo è studiato come risposta ad un determinato requisito. Tale procedura è stata applicata per ogni tipo di requisito, sia esso obbligatorio o opzionale. Per tener traccia di tali corrispondenze è stata redatta una tabella consultabile nella relativa sezione del documento di \textit{Specifica Tecnica}. Con questa metodologia si facilita una successiva fase di verifica in cui viene controllato se ciascun componente risponde effettivamente ad una specifica del problema e soprattutto se tutti i requisiti sono stati soddisfatti.
\subsection{Dettaglio delle verifiche tramite analisi}
\subsubsection{RPP}
Alla revisione attuale non è possibile indicare un piano completo delle attività di analisi poichè la fase di progettazione non ha ancora raggiunto un livello di dettaglio sufficiente. Alla prossima revisione saranno indicati i test e le modalità stabilite durante la progettazione di dettaglio.
\subsubsection{RQ}
Completata la progettazione di dettaglio, contenuta nella Definizione di Prodotto, si sono potuti definire tutti i test relativi alle singole componenti che sono di seguito riportati.

\clearpage

\section{Specifica dei test di unità}
Per quanto riguarda i test di unità si è deciso di usare lo \textit{Unit Test Framework} fornito dalle librerie Qt versione 4.3. Tale \textit{framework} aiuta a unificare la struttura dei test e a semplificare la creazione di classi \textit{driver} e classi \textit{stub} per applicazioni basate sulle librerie Qt. Per maggiori informazioni e norme sulla progettazione dei test si faccia riferimento al \textit{link} alla documentazione Qt che si può trovare nel presente documento alla Sezione~\ref{txt:links}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Componente \textsl{Core}} %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{PQ_Core}

\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Componente \textsl{AI}} %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{PQ_AI}

\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Componente \textsl{GUI}} %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{PQ_GUI}

\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Componente \textsl{Network}} %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{PQ_Network}

\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Resoconti}

\subsection{Resoconto sui test di unità della componente \textit{Core}}
\input{Resoconto_Test_Core}

Osservando i dati contenuti nella tabella, si può notare che i test eseguiti inizialmente sulla classe \texttt{GameLoop} hanno riportato 3 fallimenti. Due di questi sono stati risolti con la seconda iterazione, mentre il terzo ha richiesto un'ulteriore ripetizione dei test, per un totale di 15 test complessivi.

\subsection{Resoconto sui test di unità della componente \textit{AI}}
\input{Resoconto_Test_AI}

Osservando i test si può notare che tutti quelli falliti con la prima esecuzione, sono stati immediatamente risolti, e rieseguendo nuovamente la test suite non sono stati rilevati ulteriori problemi.

\subsection{Resoconto sui test di unità della visuale 3D}
\input{Resoconto_Test_3D}

Per quanto riguarda invece la visuale 3D, sono state testate le tre classi coinvolte ed in particolare sono stati presi in considerazione quattro metodi per eseguire il test. Il primo fallimento è relativo al metodo \texttt{drawMove}: non veniva calcolato correttamente il canale alfa dei colori. Il difetto, una volta individuato, è stato corretto e rieseguendo nuovamente i test si è riscontrato il fallimento di un altro metodo, \texttt{explode}, dove gli indici del primo e dell'ultimo cubo spostati non erano corretti. Alla terza ripetizione dei test, non sono stati individuati ulteriori problemi.

\subsection{Resoconto sui test di unità della componente \textit{Network}}
\input{Resoconto_Test_Network}

Dai risultati esposti nella tabella precedente, possiamo constatare che i test relativi alla classe \texttt{ClientSocket} sono falliti in quanto si sono verificati problemi con il parsing del flusso XML relativamente all'elemento \texttt{<history>}; per quanto riguarda invece \texttt{StreamSocket} l'unico errore che si è verificato era relativo all'apertura dello \textit{stream} XML.


\pagebreak

\appendix
\appendixpage

\section{Registro delle modifiche}
\vspace{5mm}

\begin{center}

\small

\begin{tabularx}{\textwidth}{|*{3}{X}|}
	\hline
	\textbf{Versione} & \textbf{Data} & \textbf{Autore} \\
	\hline
\end{tabularx}

\begin{filecontents}{Piano_di_Qualifica.changelog}
\begin{longtable}{*{3}{X}}
	\vspace{1mm}
	\\\hline
	4.0 & 31/03/2008 & Daniele Battaglia \\
	\multicolumn{3}{p{0.95\textwidth}}{Documento approvato per RA.} \\

	\vspace{1mm}
	\\\hline
	3.2 & 16/03/2008 & Martina Astegno \\
	\multicolumn{3}{p{0.95\textwidth}}{Ampliata la sezione relativa alle metriche.} \\

	\vspace{1mm}
	\\\hline
	3.1 & 15/03/2008 & Martina Astegno \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunta sezione Resoconti.} \\

	\vspace{1mm}
	\\\hline
	3.0 & 03/03/2008 & Martina Astegno \\
	\multicolumn{3}{p{0.95\textwidth}}{Documento approvato per RQ.} \\

	\vspace{1mm}
	\\\hline
	2.10 & 27/02/2008 & Martina Bernardini \\
	\multicolumn{3}{p{0.95\textwidth}}{Verificata sezione test di unità componente \textsl{AI}.} \\

	\vspace{1mm}
	\\\hline
	2.9 & 25/02/2008 & Martina Astegno \\
	\multicolumn{3}{p{0.95\textwidth}}{Verificata sezione test di unità componente \textsl{GUI}.} \\

	\vspace{1mm}
	\\\hline
	2.8 & 22/02/2008 & Daniele Battaglia \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunta sezione test di unità componente \textsl{AI}.} \\

	\vspace{1mm}
	\\\hline
	2.7 & 22/02/2008 & Tobia Zorzan \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunta sezione test di unità componente \textsl{GUI}.} \\

	\vspace{1mm}
	\\\hline
	2.6 & 21/02/2008 & Martina Bernardini \\
	\multicolumn{3}{p{0.95\textwidth}}{Verificata sezione test di unità componente \textsl{Network}.} \\

	\vspace{1mm}
	\\\hline
	2.5 & 19/02/2008 & Davide Pesavento \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunta sezione test di unità componente \textsl{Network}.} \\

	\vspace{1mm}
	\\\hline
	2.4 & 18/02/2008 & Nicolò Navarin \\
	\multicolumn{3}{p{0.95\textwidth}}{Verificata sezione test di unità componente \textsl{Core}.} \\

	\vspace{1mm}
	\\\hline
	2.3 & 15/02/2008 & Daniele Battaglia \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunta sezione test di unità componente \textsl{Core}.} \\

	\vspace{1mm}
	\\\hline
	2.2 & 08/02/2008 & Davide Pesavento \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiornamento alla versione 1.3 del template di documento.} \\

	\vspace{1mm}
	\\\hline
	2.0 & 24/01/2008 & Nicolò Navarin \\
	\multicolumn{3}{p{0.95\textwidth}}{Approvazione del documento per RPP.} \\

	\vspace{1mm}
	\\\hline
	1.3 & 24/01/2008 & Martina Bernardini \\
	\multicolumn{3}{p{0.95\textwidth}}{Verifica del documento.} \\

	\vspace{1mm}
	\\\hline
	1.2 & 20/01/2008 & Martina Astegno \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunta sezione Resoconto delle attività di verifica.} \\

	\vspace{1mm}
	\\\hline
	1.1 & 11/01/2008 & Tobia Zorzan \\
	\multicolumn{3}{p{0.95\textwidth}}{Modifica della gestione delle anomalie adottando un sistema di ticketing automatico.} \\

	\vspace{1mm}
	\\\hline
	1.0 & 06/12/2007 & Martina Bernardini \\
	\multicolumn{3}{p{0.95\textwidth}}{Verifica e approvazione documento.} \\

	\vspace{1mm}
	\\\hline
	0.5 & 05/12/2007 & Davide Pesavento \\
	\multicolumn{3}{p{0.95\textwidth}}{Verifica del documento. Corretti alcuni errori e migliorato l'aspetto tipografico.} \\

	\vspace{1mm}
	\\\hline
	0.4 & 05/12/2007 & Martina Astegno \\
	\multicolumn{3}{p{0.95\textwidth}}{Completata la sezione sul controllo di qualità di processo.} \\

	\vspace{1mm}
	\\\hline
	0.3 & 04/12/2007 & Davide Pesavento \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiornamento alla versione 1.2 del template di documento.} \\

	\vspace{1mm}
	\\\hline
	0.2 & 03/12/2007 & Daniele Battaglia \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunta sezione relativa alle tecniche di analisi statica e dinamica del codice.} \\

	\vspace{1mm}
	\\\hline
	0.1 & 02/12/2007 & Martina Astegno \\
	\multicolumn{3}{p{0.95\textwidth}}{Versione iniziale.} \\
\end{longtable}
\end{filecontents}
\LTXtable{\textwidth}{Piano_di_Qualifica.changelog}

\end{center}

\end{document}
