\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{appendix}
\usepackage{fancyhdr}
\usepackage{filecontents}
\usepackage{graphicx}
\usepackage{lastpage}
\usepackage{ltxtable}
\usepackage{pdflscape}
\usepackage{ulem}
\usepackage[pdftex]{hyperref}

\hypersetup{
	breaklinks,
	colorlinks,
	linkcolor=blue,
	pdftitle={Specifica\_Tecnica\_v3.0.pdf},
	pdfsubject={Specifica Tecnica},
	pdfauthor={ITWorks!},
	pdfcreator={TeXLive-2007}
}

\renewcommand{\appendixtocname}{Appendici}
\renewcommand{\appendixpagename}{Appendici}
\renewcommand{\headrulewidth}{0.6pt}
\renewcommand{\footrulewidth}{0.4pt}

\begin{document}

\normalem
\newtoks\titolo
\titolo{Specifica Tecnica}
\newtoks\data
\data{\today}

\pagestyle{fancy}
\lhead{\includegraphics[scale=0.3]{../Logo_simple}}
\chead{}
\rhead{\small{itworks@googlegroups.com}}
\lfoot{\small{\the\titolo}}
\cfoot{}
\rfoot{\thepage\ di \pageref*{LastPage}}

\thispagestyle{empty}

\begin{center}

\includegraphics[scale=0.7]{../Logo_green}
\vspace{2cm}
\\\Huge{\textsc{\the\titolo}}
\vspace{1cm}
\\\Large{\textsl{\the\data}}
\vspace{1.2cm}

\begin{abstract}
Descrizione generale delle classi che compongono l'architettura del software GoMoku3D.
\end{abstract}

\small
\vspace*{\stretch{1}}
\begin{tabular}{r|l}
	\textbf{Redazione} & Daniele Battaglia \\
						& Davide Pesavento \\
						& Nicolò Navarin \\
						& Tobia Zorzan\\
                        & Martina Astegno \\
	\textbf{Revisione} & Davide Pesavento \\
						& Tobia Zorzan \\
						& Martina Astegno\\
						& Martina Bernardini\\
	\textbf{Approvazione} & Davide Pesavento \\
						& Nicolò Navarin\\
	\textbf{Versione} & $3.0$ \\
	\textbf{Data} & \the\data \\
	\textbf{Stato} & Formale \\
	\textbf{Uso} & Esterno \\
	\textbf{Distribuzione} & Prof.\ Tullio Vardanega \\
							& Prof.\ Renato Conte \\
							& ITWorks! \\
\end{tabular}
\vspace{\stretch{1}}

\end{center}

\pagebreak

\normalsize
\vspace*{0.1cm}
\tableofcontents

\pagebreak

\listoffigures

\pagebreak

\section{Introduzione}
\subsection{Scopo del documento}
Questo documento fornisce al committente una descrizione architetturale del prodotto \mbox{\textbf{GoMoku3D}}, basandosi sul documento di \textit{Analisi dei Requisiti} attraverso vari diagrammi della specifica UML. Analizzeremo l'architettura di sistema partendo da una visione ad alto livello, per scendere successivamente più in dettaglio. In accordo con il nostro \textit{Piano di Progetto}, questo documento sarà sottoposto ad ulteriori modifiche, almeno fino alla fine della seconda fase.

\subsection{Scopo del prodotto}
Il prodotto che proponiamo è una variante 3D del noto gioco \textit{GoMoku}. Tale software dovrà supportare una modalità online oltre che a quella in locale e fornire soluzioni grafiche che ne rendano l'utilizzo semplice e immediato con particolare attenzione alla giocabilità.

\subsection{Glossario}
Il \textit{Glossario} cui far riferimento è fornito in un file separato, allegato alla presente \textit{Specifica Tecnica}.
Le definizioni del glossario si applicano a tutti i documenti formali allo scopo di rendere non ambigua e omogenea la terminologia tecnica utilizzata.

\subsection{Riferimenti}
\subsubsection{Normativi}
\begin{itemize}
	\item OMG, \textit{Unified Modeling Language Superstructure}, v2.1.2
\end{itemize}
\subsubsection{Informativi}
\begin{itemize}
	\item Addison Wesley, \textit{UML Distilled} ($3^a$ edizione)
	\item Gang of Four, \textit{Design Patterns, Elements of Reusable Object Oriented Software}
	\item Documentazione Qt versione 4.3. \\\url{http://doc.trolltech.com/4.3/index.html}
	\item Documentazione Coin3D versione 2.5.0. \\\url{http://doc.coin3d.org/Coin/}
	\item Documentazione SoQt versione 1.4.1. \\\url{http://doc.coin3d.org/SoQt/}
\end{itemize}

\pagebreak

\section{Definizione del prodotto}
\subsection{Diagramma delle attività}
Iniziamo a definire il prodotto che presentiamo, descrivendo la logica di alto livello del software tramite l'ausilio dei diagrammi delle attività. Essi descrivono il flusso degli eventi all'interno dell'applicazione.

\begin{figure}[hpbt]
\centering
\includegraphics[width=13cm]{attivita-generale.png}
\caption{Diagramma generale delle attività}
\label{fig:attivita-generale}
\end{figure}
La Figura~\ref{fig:attivita-generale} mette in evidenza le tre alternative che si presentano all'avvio dell'applicazione: l'utente infatti può scegliere se avviare una partita online o una stand-alone, oppure caricare una partita precedentemente salvata. Nel primo caso deve poi decidere se agire come client o server e procedere poi ad impostare i campi richiesti; è ammessa la possibilità di fungere da server dedicato. Infine sia per la partita online che per quella stand-alone, è prevista l'opportunità di assistere al gioco in qualità di spettatore, senza agire attivamente.

\begin{figure}[hpbt]
\centering
\includegraphics[width=13cm]{attivita-stand-alone.png}
\caption{Attività gioca partita stand-alone}
\label{fig:attivita-stand-alone}
\end{figure}
La Figura~\ref{fig:attivita-stand-alone} descrive in dettaglio il flusso degli eventi in una partita stand-alone. Inizialmente si visualizza lo stato della partita in corso e lo storico delle mosse, poi si individua chi deve eseguire la mossa, e gli avversari nel frattempo si mettono in attesa. Nel momento in cui è il proprio turno, si può scegliere di interrompere la partita e quindi salvare, tornare indietro nello storico e ripristinare una situazione passata, oppure richiedere il suggerimento. Una volta eseguita la mossa, se questa non ha portato alla vittoria, si torna all'attività iniziale del diagramma e da lì si procede.

\begin{figure}[hpbt]
\centering
\includegraphics[width=13cm]{attivita-online.png}
\caption{Attività gioca partita online}
\label{fig:attivita-online}
\end{figure}
La Figura~\ref{fig:attivita-online} ha lo scopo di descrivere più in dettaglio la modalità di gioco online dal punto di vista del client; le impostazioni vengono fornite come dato in ingresso, e a questo punto possono partire due thread, uno che gestisce la chat, e l'altro per il flusso di gioco. Anche in questa modalità di gioco si può visualizzare lo storico delle mosse; nel momento in cui è il proprio turno di gioco viene avviato un timer (se previsto nelle impostazioni), e quindi la mossa deve essere eseguita entro un certo tempo limite. Nel caso in cui la mossa non sia valida o sia stata eseguita oltre il tempo massimo, o ancora conduca alla vittoria, lo si comunica al server e la partita termina. Altrimenti, si comunica al server la mossa valida e il turno passa all'avversario.

\begin{figure}[hpbt]
\centering
\includegraphics[width=11cm]{attivita-spettatore.png}
\caption{Attività gioca in modalità spettatore}
\label{fig:attivita-spettatore}
\end{figure}
La Figura~\ref{fig:attivita-spettatore} analizza la modalità spettatore e anche in questo caso le impostazioni costituiscono il dato in ingresso; il flusso di gioco quindi è un ciclo che ha inizio con la visualizzazione dello stato della partita, procede poi con la mossa ricevuta e si ripete finché la partita non termina.

\clearpage

\subsection{Architettura generale del sistema}
Partendo dalle informazioni contenute nei precedenti diagrammi di attività, abbiamo potuto individuare le macro-componenti del software e come esse devono interagire tra loro. Come mostrato in Figura~\ref{fig:class-global}, l'architettura ad alto livello è composta da 4 componenti:
\begin{itemize}
\item \textbf{Core:} è composto dalle classi che implementano la logica di base necessaria al corretto funzionamento del gioco e da alcune classi ausiliarie comuni a tutta l'applicazione;
\item \textbf{GUI:} contiene tutte le classi che rendono possibile l'interazione tra l'utente e l'applicazione;
\item \textbf{Network:} racchiude le classi usate per la gestione e l'interfacciamento con la rete;
\item \textbf{AI:} contiene la classe che implementa l'algoritmo di intelligenza artificiale e altre classi di supporto.
\end{itemize}

\begin{landscape}
\begin{figure}[hpbt]
\centering
\includegraphics[width=20cm]{GlobalClassDiagram.png}
\caption{Diagramma delle classi ad alto livello}
\label{fig:class-global}
\end{figure}
\end{landscape}

\clearpage

\section{Descrizione dei componenti}

\subsection{Componente \textsl{Core}}

\begin{figure}[hpbt]
\centering
\includegraphics[width=13cm]{CoreDiagram.png}
\caption{Diagramma delle classi del componente \textsl{Core}}
\label{fig:class-core}
\end{figure}

\begin{description}
	\item[GameLoop] \`E il nucleo del componente \textsl{Core}: questa classe è un \textit{thread} che gestisce il susseguirsi dei turni di gioco in modo asincrono rispetto alle componenti GUI e Network ed ha il compito di segnalare quando è il turno di un giocatore, la \uline{mossa} da lui effettuata ed eventuali condizioni di vittoria o pareggio.
	\item[GameMatrix] Racchiude le strutture dati necessarie per rappresentare la \uline{matrice di gioco}. Tali strutture sono condivise anche con il componente \textsl{AI}, che le utilizza nei propri algoritmi, al fine di ridurre i requisiti complessivi di memoria dell'applicazione. Questa classe rende inoltre disponibili dei metodi per il controllo di validità delle mosse (\texttt{check()}) e delle condizioni di vittoria (\texttt{add()}).
	\item[SyncSharedCondition] Questa classe è un \textit{singleton} che serve a sincronizzare agevolmente i \textit{thread} concorrenti e fornisce un meccanismo controllato per lo scambio di dati specifici tra le diverse componenti dell'applicazione. Internamente sfrutta le classi \texttt{QMutex} e \texttt{QWaitCondition} della libreria QtCore, che assicurano operazioni cross-platform pur mantenendo un'elevata efficienza.
	\item[Settings] La gerarchia composta dalle classi \texttt{AbstractSettings}, \\\texttt{SettingsManager}, \texttt{GUISettings}, \texttt{ServerSettings} e \texttt{LocalSettings} permette la gestione delle opzioni del software che devono essere mantenute persistenti tra una sessione e l'altra; il \textit{backend} di archiviazione delle opzioni è interamente cross-platform grazie alla classe \texttt{QSettings} della libreria QtCore.
	\item[Move e Point] Sono classi ausiliarie che incapsulano rispettivamente una \uline{mossa} di un giocatore e una coordinata all'interno della \uline{matrice di gioco}, offrendo un'interfaccia comune per le operazioni su esse.
\end{description}

\begin{figure}[hpbt]
\centering
\includegraphics[width=13cm]{PlayerDiagram.png}
\caption{Diagramma della gerarchia dei Player e del componente \textsl{AI}}
\label{fig:class-player}
\end{figure}

\begin{description}
	\item[Player] \`E una classe astratta che viene usata da \texttt{GameLoop} tramite puntatore polimorfo per poter trattare allo stesso modo diversi tipi di giocatore. Le classi concrete che definiscono i giocatori sono:
	\begin{itemize}
		\item \textbf{HumanPlayer:} \uline{giocatore umano} che interagisce con l'applicazione tramite la GUI;
		\item \textbf{RemotePlayer:} presente solo in modalità online, rappresenta un giocatore non locale;
		\item \textbf{AIPlayer:} \uline{giocatore artificiale} che utilizza le funzionalità della classe \texttt{AI} che gli permettono di calcolare la mossa da effettuare.
	\end{itemize}
\end{description}

\clearpage

\subsection{Componente \textsl{AI}}

\begin{figure}[hpbt]
\centering
\includegraphics[width=13cm]{AIDiagram.png}
\caption{Diagramma delle classi del componente \textsl{AI}}
\label{fig:class-ai}
\end{figure}

\begin{description}
	\item[AI] Classe astratta da cui derivano le classi che effettivamente implementano l'algoritmo di intelligenza artificiale. Fornisce un'interfaccia comune in modo da semplificare l'utilizzo del componente da parte del resto dell'applicazione.
	\item[ThreatSearchAI] Classe che concretizza l'interfaccia richiesta da \texttt{AI}. Viene utilizzata nel caso di \uline{difficoltà II} con valore diverso da 3. Mantiene un riferimento ad un oggetto di tipo \texttt{Threat} che fornisce i dati necessari all'algoritmo di scelta della mossa raccolti dall'inizio della partita.
	\item[Threat] Incapsula una struttura dati che mantiene informazioni sullo stato di gioco di un giocatore, ovvero serve a mantenere in memoria tutte le possibili locazioni nella \uline{matrice di gioco} in cui tale giocatore ha già posizionato almeno una \uline{pedina} e in cui è ancora possibile conseguire una situazione di vittoria. Ha come classe interna \texttt{Node}, necessaria per semplificare la gestione dei dati.
	\item[Threat::Node] Un'istanza di questa classe serve a rappresentare una singola minaccia offensiva, in particolare la sua collocazione nella \uline{matrice di gioco} e l'entità di tale minaccia.
	\item[CCThreatSearchAI] Concretizza l'interfaccia richiesta da \texttt{AI}. Questo algoritmo è da usare solamente se il parametro di \uline{difficoltà II} è uguale a 3. \`E una classe derivata da \texttt{ThreatSearchAI} in quanto l'algoritmo ha lo stesso comportamento a meno della modalità difensiva, che va integrata a causa dell'aggiunta della condizione di vittoria dovuta al valore di \uline{difficoltà II}. Utilizza una struttura dati di tipo \texttt{CCThreat}.
	\item[CCThreat] Deriva dalla classe \texttt{Threat} e ne mantiene le funzionalità; al tempo stesso introduce un'ulteriore struttura dati che ha il compito di alleggerire il carico computazionale dell'algoritmo nel caso in cui \uline{difficoltà II} sia uguale a 3. Per fare ciò utilizza la classe interna \texttt{ConnectedComponent}.
	\item[CCThreat::ConnectedComponent] Struttura dati le cui istanze rappresentano componenti connesse (formate da \uline{pedine} dello stesso giocatore adiacenti) e informazioni sull'estensione di tali componenti rispetto alla \uline{matrice di gioco}.
	\item[Suggester] \`E un \textit{thread} che permette alla GUI di calcolare una mossa parallelamente al consueto svolgimento delle operazioni dell'applicazione e di suggerirla all'utente che la richiede. \`E incaricata inoltre di istanziare la classe derivata da AI più opportuna a seconda delle impostazioni di gioco.
\end{description}

\clearpage

\subsection{Componente \textsl{GUI}}

\begin{figure}[hpbt]
\centering
\includegraphics[width=13cm]{WidgetDiagram.png}
\caption{Diagramma delle classi dei \textit{widgets} della GUI}
\label{fig:class-widget}
\end{figure}

\begin{figure}[hpbt]
\centering
\includegraphics[width=13cm]{DialogDiagram.png}
\caption{Diagramma delle classi dei \textit{dialogs} della GUI}
\label{fig:class-dialog}
\end{figure}

\begin{description}
	\item[MainWindow] \`E la classe principale dell'interfaccia utente; viene istanziata una sola volta all'avvio dell'applicazione e distrutta al termine di essa; è responsabile della creazione e distruzione di tutti gli altri oggetti. Ha il compito di ricevere segnali provenienti da altre componenti dell'applicazione e modificare adeguatamente lo stato delle altre classi della GUI.
	\item[PlayerInfo e PlayersWidget] \texttt{PlayerInfo} è la struttura dati usata per gestire la corrispondenza tra dati funzionali e informazioni grafiche dei giocatori; \texttt{PlayersWidget}, derivata da \texttt{QWidget}, presenta tali informazioni all'utente.
	\item[HistoryModel] Concretizza \texttt{QAbstractTableModel} per implementare la struttura dati sottostante lo storico delle mosse. L'interfacciamento con l'utente viene realizzato con l'ausilio della classe \texttt{QTableView} della libreria QtGui.
	\item[ChatWidget] Permette lo scambio di messaggi di testo tra i giocatori partecipanti ad una stessa partita online. Questa classe dialoga direttamente con lo strato di rete tramite \textit{signals} e \textit{slots}.
	\item[TimerWidget] Fornisce una semplice visualizzazione del tempo rimanente per effettuare la propria mossa durante una partita online, nel caso in cui si utilizzi tale variante alle regole di gioco.
\end{description}

\subsubsection{Visuale 3D}

\begin{figure}[hpbt]
\centering
\includegraphics[width=13cm]{3DDiagram.png}
\caption{Diagramma delle classi della visuale 3D}
\label{fig:class-3d}
\end{figure}

La visualizzazione 3D della matrice di gioco è fornita dall'interazione delle seguenti classi:
\begin{description}
	\item[RenderWidget] Derivata dalla classe \texttt{QWidget}, fornisce un'interfaccia standard alla visualizzazione 3D, così da poter essere gestita come un qualsiasi elemento grafico delle librerie Qt. I metodi di questa classe sono le operazioni principali che il resto dell'applicazione può richiedere alla parte grafica.
	\item[CubeRenderArea] Derivata dalla classe \texttt{SoQtRenderArea}, è l'implementazione che fornisce un contesto OpenGL all'applicazione. Tale contesto è gestito con le librerie SoQt. Questa classe rappresenta, oltre che l'esecutore del \textit{rendering} della scena, anche il gestore di tutti gli eventi generati dall'interazione con l'utente.
	\item[CubeRenderArea::Orientation] \`E una classe interna enumerazione. Può assumere 6 valori che indicano un semiasse di un sistema cartesiano in 3 dimensioni. Il valore \texttt{NO\_ORIENTATION} è usato nel caso non vi sia alcuna direzione assegnata.
	\item[CubeRenderArea::Direction] Classe interna enumerazione, indica le sei direzioni possibili in cui è possibile spostare un oggetto nello spazio.
	\item[SceneGraph] \`E una classe usata esclusivamente da \texttt{CubeRenderArea}. \`E un'insieme di strutture dati e metodi che compongono e manipolano il grafo della scena. Tutti gli elementi della scena 3D infatti sono dei nodi, e l'intero albero composto da questi nodi rappresenta tutti gli elementi e le proprietà che devono essere utilizzate nelle operazioni di \textit{rendering}.
\end{description}

\clearpage

\subsection{Componente \textsl{Network}}

\begin{figure}[hpbt]
\centering
\includegraphics[width=13cm]{NetworkDiagram.png}
\caption{Diagramma delle classi del componente \textsl{Network}}
\label{fig:class-network}
\end{figure}

\begin{description}
	\item[Network] \`E la classe \textit{fa\c{c}ade} del componente \textsl{Network} e, come tale, fornisce al resto dell'applicazione un'interfaccia semplice e di alto livello al complesso sottosistema per la gestione della rete. In realtà questa è una classe astratta che dichiara solamente quale deve essere tale interfaccia: l'implementazione è delegata alle sottoclassi concrete \texttt{GameClient} e \texttt{GameServer}, poiché il comportamento varia a seconda se l'applicazione funge da server oppure da client per la partita.
	\item[GameClient] Concretizza \texttt{Network}. Viene usata lato client per la connessione iniziale al server e la successiva gestione del protocollo di comunicazione.
	\item[GameServer] Concretizza \texttt{Network}. Permette di gestire le connessioni in entrata tramite la classe \texttt{QTcpServer} e l'eventuale continuazione della comunicazione come giocatore o spettatore della partita durante lo svolgimento di essa.
	\item[StreamSocket] Implementa le funzionalità di basso livello necessarie a scambiare i messaggi del protocollo comuni a client e server. Il protocollo è basato su uno \textit{stream} XML bidirezionale trasmesso su una connessione TCP. Questa classe sfrutta la derivazione protetta dalle classi \texttt{QXmlStreamReader} e \texttt{QXmlStreamWriter} della libreria QtXml per leggere e scrivere lo \textit{stream} XML. Tutte le comunicazioni con l'host remoto vengono effettuate attraverso \texttt{QTcpSocket} della libreria QtNetwork.
	\item[ClientSocket e ServerSocket] Estendono \texttt{StreamSocket} e ne arricchiscono l'interfaccia con metodi specifici di client e server.
\end{description}

\clearpage

\section{Design Patterns}
In questa sezione sono contenuti i \textit{design patterns} utilizzati nella progettazione del prodotto.

\subsection{Singleton}
Le classi \texttt{SyncSharedCondition} e \texttt{GameMatrix} sono state progettate seguendo il pattern del \textit{``Singleton''}.

\subsection{Fa\c{c}ade}
La classe \texttt{Network} è stata progettata secondo il pattern \textit{``Fa\c{c}ade''}.

\subsection{Strategy}
La gerarchia di classi del componente \textsl{AI} rappresenta un pattern di tipo \mbox{\textit{``Strategy''}}. Esso è così composto:
\begin{itemize}
	\item \textbf{Strategy} (\texttt{AI}): dichiara un'interfaccia comune utilizzata da Context.
	\item \textbf{ConcreteStrategy} (\texttt{ThreatSearchAI}, \texttt{CCThreatSearchAI}): implementano il metodo \texttt{move(level : int)} e \texttt{forceMove()} dell'interfaccia Strategy.
	\item \textbf{Context} (\texttt{AIPlayer}, \texttt{Suggester}): utilizzano il puntatore polimorfo \texttt{\_ai} invocando i metodi dichiarati da Strategy e implementati da ConcreteStrategy.
\end{itemize}

\subsection{Factory method}
La gerarchia di classi del componente \textsl{AI} è stata progettata seguendo un pattern \mbox{\textit{``Factory method''}}.
\begin{itemize}
	\item \textbf{Creator} (\texttt{ThreatSearchAI}): dichiara il factory method \texttt{createThreat()} che ritorna un riferimento ad un oggetto di tipo Product.
	\item \textbf{ConcreteCreator} (\texttt{ThreatSearchAI}, \texttt{CCThreatSearchAI}): implementa il factory method \texttt{createThreat()} e ritorna un'istanza di ConcreteProduct.
	\item \textbf{Product} (\texttt{Threat}): definisce l'interfaccia degli oggetti creati dal factory method.
	\item \textbf{ConcreteProduct} (\texttt{Threat}, \texttt{CCThreat}): implementa l'interfaccia presente in Product.
\end{itemize}

\subsection{Template method}
I seguenti metodi sono dei \mbox{\textit{``Template method''}}:
\begin{itemize}
	\item \texttt{Player::move()}, invoca il metodo astratto \texttt{Player::doMove()} che deve essere ridefinito dalle sottoclassi.
	\item \verb!ThreatSearchAI::defense(level : int)!, effettua la chiamata a \\\verb!ThreatSearchAI::defenseHook()! che può essere ridefinito nelle classi derivate.
	\item \verb!Threat::insert(point : Point)!, effettua la chiamata a \\\verb!Threat::insertHook(p : Point)! che può essere ridefinito nelle classi derivate.
	\item \verb!Threat::scanFromMatrix()!, effettua la chiamata a \\\verb!Threat::scanFromMatrixHook(p : Point)! che può essere ridefinito nelle classi derivate.
\end{itemize}

\subsection{Adapter}
La gerarchia di classi per la gestione delle impostazioni è un pattern di tipo \textit{``Adapter''} a più livelli.
\begin{itemize}
	\item \textbf{Target} (\texttt{AbstractSettings}, \texttt{SettingsManager}): definiscono l'interfaccia che può essere utilizzata dai Client.
	\item \textbf{Adaptee} (\texttt{QSettings}): è l'interfaccia esistente che necessita di adattamento.
	\item \textbf{Adapter} (\texttt{LocalSettings}, \texttt{ServerSettings}, \texttt{GUISettings}): adatta l'interfaccia di Adaptee all'interfaccia di Target. In questo caso \texttt{LocalSettings} e \texttt{ServerSettings} adattano l'interfaccia di \texttt{QSettings} a quella di \texttt{SettingsManager}, mentre \texttt{GUISettings} adatta \texttt{QSettings} solamente all'interfaccia più limitata di \texttt{AbstractSettings}.
	\item \textbf{Client} (\texttt{MainWindow}): utilizza gli Adapter attraverso l'interfaccia di Target.
\end{itemize}

\clearpage

\section{Stime di fattibilità e bisogno di risorse}
Tra le richieste da parte del committente sono presenti la portabilità del prodotto su più sistemi operativi e la necessità di interfacciarsi ad una rete informatica (anche geograficamente distribuita). \`E anche necessario che il software possieda un'interfaccia grafica con visuale 3D. Per quanto riguarda il linguaggio di programmazione scelto per la realizzazione del software abbiamo optato per il \textbf{C++}. Questa scelta è stata dettata oltre che dalle richieste sopra citate, anche dalla relativa esigenza di risorse computazionali da parte degli algoritmi di intelligenza artificiale che abbiamo individuato. Per quanto concerne le librerie, è stato deciso di utilizzare per l'interfaccia utente le librerie Qt versione 4.3, che integrano la portabilità e la gestione delle connessioni di rete. Per la parte di grafica tridimensionale, dopo uno studio delle varie librerie grafiche disponibili, abbiamo deciso di utilizzare le Coin3D basate su OpenGL che, grazie alla loro nativa integrazione con le librerie Qt, alla portabilità e alla relativa semplicità d'uso, agevoleranno la realizzazione del prodotto. Tutto questo materiale è disponibile sotto licenza GNU GPLv2.

Utilizzando queste risorse e visti i tempi di consegna del prodotto, la ditta \mbox{\textsl{ITWorks!}} ritiene quindi soddisfacibili le richieste del committente. Per tempistiche più dettagliate si rimanda al \textit{Piano di Progetto}.

\pagebreak

\section{Tracciamento requisiti $\leftrightarrow$ diagrammi di attività}
\LTXtable{\textwidth}{Tracciamento_requisiti_attivita.tex}

\pagebreak

\section{Tracciamento requisiti $\rightarrow$ componenti}
\LTXtable{\textwidth}{Tracciamento_requisiti_componenti.tex}

\pagebreak

\section{Tracciamento componenti $\rightarrow$ requisiti}
\LTXtable{\textwidth}{Tracciamento_componenti_requisiti.tex}

\pagebreak

\appendix
\appendixpage

\section{Registro delle modifiche}
\vspace{5mm}

\begin{center}

\small

\begin{tabularx}{\textwidth}{|*{3}{X}|}
	\hline
	\textbf{Versione} & \textbf{Data} & \textbf{Autore} \\
	\hline
\end{tabularx}

\begin{filecontents}{Specifica_Tecnica.changelog}
\begin{longtable}{*{3}{X}}
    \vspace{1mm}
    \\\hline
    3.0 & 30/03/2008 & Nicolò Navarin \\
    \multicolumn{3}{p{0.95\textwidth}}{Documento verificato e approvato per la RA.} \\

    \vspace{1mm}
    \\\hline
    2.2 & 26/03/2008 & Martina Astegno \\
    \multicolumn{3}{p{0.95\textwidth}}{Aggiunte descrizioni dei diagrammi di attività presenti all'inizio del documento.} \\

    \vspace{1mm}
    \\\hline
    2.1 & 25/03/2008 & Martina Astegno \\
    \multicolumn{3}{p{0.95\textwidth}}{Aggiunto tracciamento inverso.} \\

    \vspace{1mm}
	\\\hline
	2.0 & 03/03/2008 & Nicolò Navarin \\
	\multicolumn{3}{p{0.95\textwidth}}{Approvato documento per la RQ.} \\

	\vspace{1mm}
	\\\hline
	1.8 & 23/02/2008 & Davide Pesavento \\
	\multicolumn{3}{p{0.95\textwidth}}{Verifica del documento. Alcune correzioni ortografiche e tipografiche.} \\

	\vspace{1mm}
	\\\hline
	1.7 & 22/02/2008 & Martina Bernardini \\
	\multicolumn{3}{p{0.95\textwidth}}{Verifica modifiche apportate alla componente \textsl{Network}.} \\

	\vspace{1mm}
	\\\hline
	1.6 & 22/02/2008 & Martina Astegno \\
	\multicolumn{3}{p{0.95\textwidth}}{Verifica delle modifiche apportate alla componente \textsl{3D} e \textsl{AI}.} \\

	\vspace{1mm}
	\\\hline
	1.5 & 18/02/2008 & Tobia Zorzan \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunto diagramma delle classi per la visuale 3D e relative descrizioni. Aggiornato diagramma delle classi del componente \textsl{GUI}.} \\

	\vspace{1mm}
	\\\hline
	1.4 & 16/02/2008 & Martina Bernardini \\
	\multicolumn{3}{p{0.95\textwidth}}{Approvato documento per RPD.} \\

	\vspace{1mm}
	\\\hline
	1.3 & 15/02/2008 & Daniele Battaglia \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunto diagramma delle classi del componente \textsl{AI}, relative descrizioni e \textit{design patterns}. Aggiornato il tracciamento componenti $\leftrightarrow$ requisiti.} \\

	\vspace{1mm}
	\\\hline
	1.2 & 15/02/2008 & Davide Pesavento \\
	\multicolumn{3}{p{0.95\textwidth}}{Varie modifiche di minore entità in seguito ad alcuni cambiamenti nei diagrammi.} \\

	\vspace{1mm}
	\\\hline
	1.1 & 12/02/2008 & Davide Pesavento \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunto diagramma delle classi del componente \textsl{Network} e relative descrizioni. Aggiornato di conseguenza il tracciamento componenti $\leftrightarrow$ requisiti.} \\

	\vspace{1mm}
	\\\hline
	1.0 & 26/01/2008 & Davide Pesavento \\
	\multicolumn{3}{p{0.95\textwidth}}{Revisione finale e approvazione per RPP.} \\

	\vspace{1mm}
	\\\hline
	0.8 & 26/01/2008 & Tobia Zorzan \\
	\multicolumn{3}{p{0.95\textwidth}}{Revisione del documento.} \\

	\vspace{1mm}
	\\\hline
	0.7 & 26/01/2008 & Daniele Battaglia \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunta sezione ``Tracciamento componenti $\leftrightarrow$ requisiti''.} \\

	\vspace{1mm}
	\\\hline
	0.6 & 25/01/2008 & Daniele Battaglia \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunti diagrammi di sequenza e descrizioni.} \\

	\vspace{1mm}
	\\\hline
	0.5 & 25/01/2008 & Davide Pesavento \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiornato alla versione 1.3 del template di documento.} \\

	\vspace{1mm}
	\\\hline
	0.4 & 25/01/2008 & Nicolò Navarin \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunta sezione ``Tracciamento attività $\leftrightarrow$ requisiti''.} \\

	\vspace{1mm}
	\\\hline
	0.3 & 24/01/2008 & Nicolò Navarin \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunto diagramma delle attività ad alto livello e la sezione introduttiva.} \\

	\vspace{1mm}
	\\\hline
	0.2 & 24/01/2008 & Daniele Battaglia \\
	\multicolumn{3}{p{0.95\textwidth}}{Aggiunti diagrammi delle classi e descrizioni.} \\

	\vspace{1mm}
	\\\hline
	0.1 & 23/01/2008 & Davide Pesavento \\
	\multicolumn{3}{p{0.95\textwidth}}{Versione iniziale con scheletro del documento.} \\
\end{longtable}
\end{filecontents}
\LTXtable{\textwidth}{Specifica_Tecnica.changelog}

\end{center}

\end{document}
