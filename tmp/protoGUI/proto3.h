#include <QtGui>

class EnterChat : public QLineEdit {
	Q_OBJECT
public:
	EnterChat(QWidget *parent) : QLineEdit(parent) {}
public slots:
	void handleReturn();
signals:
	void newText(const QString & text);
};

class TimerWidget : public QLCDNumber {
	Q_OBJECT
public:
	TimerWidget(QWidget *parent) : QLCDNumber(2, parent), secs(60) { display(60); }
public slots:
	void updateTimer();
private:
	int secs;
};
