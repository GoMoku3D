#include "proto3.h"

#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/SoQtRenderArea.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/nodes/SoSpotLight.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoRotation.h>
#include <Inventor/actions/SoBoxHighlightRenderAction.h>
#include <Inventor/projectors/SbSphereSheetProjector.h>
#include <Inventor/engines/SoInterpolateVec3f.h>
#include <Inventor/engines/SoOneShot.h>
#include <Inventor/nodes/SoTexture2.h>
#include <Inventor/sensors/SoOneShotSensor.h>
#include <Inventor/misc/SoChildList.h>


static const int NUM_CUBES = 27;

class MyRenderArea;

void rotateCamera(MyRenderArea *, float, float);
void zoomCamera(SoCamera *, int);
void explodeCube(void *, SoSensor *);
void implodeCube(MyRenderArea *);
void selectCubes(MyRenderArea*, SbBool, SbBool, SbBool, int, int, int);

class MyRenderArea : public SoQtRenderArea {
	public:
		SoPerspectiveCamera *camera;
		SbBool isRotating;
		SbBool hasPreviousPoint;
		SbSphereSheetProjector *projector;
		SoSeparator *selectedCube;
		SbBool isExploded;
		SoSeparator* (*graphMatrix)[NUM_CUBES][NUM_CUBES];
		SoSeparator* (*explodedCubes);
		int numExplodedCubes;
		SoGroup *graph;
		SoGroup *movedGraph; 
		SoCube *cubeShape;
		SoTranslation *mover; 
		SoOneShot *timer;
		SoInterpolateVec3f *interpolator;
		SoOneShotSensor *explodeSensor;
		
		float lastX;
		float lastY;
		
		MyRenderArea(QWidget *parent) : SoQtRenderArea(parent) {
		camera = new SoPerspectiveCamera();
		isRotating=false;
		hasPreviousPoint=false;
		
		projector = new SbSphereSheetProjector(SbSphere(SbVec3f(0, 0, 0), 0.6f));
		SbViewVolume volume;
		volume.ortho(-1, 1, -1, 1, -1, 1);
		projector->setViewVolume(volume);
		
		isExploded = false;
		
		graphMatrix = new SoSeparator*[NUM_CUBES][NUM_CUBES][NUM_CUBES];
		
		graph = new SoGroup();
		graph->ref();
		movedGraph = new SoGroup();
		movedGraph->ref();
		cubeShape = new SoCube();
						
		timer = new SoOneShot();
		timer->ref();
		timer->duration.setValue(1);
		timer->flags = SoOneShot::HOLD_FINAL;
		
		
		interpolator = new SoInterpolateVec3f();
		
		interpolator->alpha.connectFrom(&timer->ramp);
		
		mover = new SoTranslation();
		mover->ref();
		mover->translation.connectFrom(&interpolator->output);
		
				movedGraph->addChild(mover);

		
		lastX = 0.0;
		lastY = 0.0;
		
		explodeSensor = new SoOneShotSensor();
		explodeSensor->setFunction(explodeCube);
		explodeSensor->setData(this);
		}
	
};

SbBool renderAreaCB(void *userdata, QEvent *eventCB)
{
	MyRenderArea *renderArea = (MyRenderArea *) userdata;
	SbBool handled = FALSE;
	
	switch(eventCB->type()) {
		case QEvent::MouseButtonPress:
			renderArea->isRotating = true;
			break;
		case QEvent::MouseButtonRelease:
			renderArea->hasPreviousPoint = false;
			renderArea->isRotating = false;
			break;
		case QEvent::MouseMove:
			QMouseEvent *mouseevent = (QMouseEvent *) eventCB;
			if(renderArea->isRotating == true) {
				//needs to handle mouse dragging
				rotateCamera(renderArea, mouseevent->x(), mouseevent->y());
			}
			handled = true;
			break;
		case QEvent::Wheel:
				QWheelEvent *wheelevent = (QWheelEvent *) eventCB;
				zoomCamera(renderArea->camera, wheelevent->delta()/120);
			handled = true;
			break;
		case QEvent::KeyPress:
			QKeyEvent *keyevent = (QKeyEvent *) eventCB;
			switch(keyevent->key()) {
				case Qt::Key_Space:
					if(!renderArea->isExploded) {
						//explodeCube(renderArea);
						renderArea->explodeSensor->schedule();
						
					} else {
						implodeCube(renderArea);
					}
					handled=true;
					break;
			}
			break;		
		default:
			break;
	}
	
	return handled;
}

void applyRotationToCamera(SoCamera *camera, const SbRotation & rot) {
	// Find global coordinates of focal point.
	SbVec3f direction;
	camera->orientation.getValue().multVec(SbVec3f(0, 0, -1), direction);
	SbVec3f focalpoint = camera->position.getValue() + camera->focalDistance.getValue() * direction;

	// Set new orientation value by accumulating the new rotation.
	camera->orientation = rot * camera->orientation.getValue();

	// Reposition camera so we are still pointing at the same old focal point.
	camera->orientation.getValue().multVec(SbVec3f(0, 0, -1), direction);
	camera->position = focalpoint - camera->focalDistance.getValue() * direction;

}

void rotateCamera(MyRenderArea *renderarea, float newX, float newY) {
	if(renderarea->hasPreviousPoint) {
		//qDebug() << "Muovo da: " << renderarea->lastX << "," << renderarea->lastY << " a: " << newX << newY;

		SbVec2s glsize = renderarea->getViewportRegion().getViewportSizePixels();
		SbVec2f lastpos;
		lastpos[0] = renderarea->lastX / float(SoQtMax((int)(glsize[0]-1), 1));
		lastpos[1] = 1-(renderarea->lastY / float(SoQtMax((int)(glsize[1]-1), 1)));

		SbVec2f newpos;
		newpos[0] = newX / float(SoQtMax((int)(glsize[0]-1), 1));
		newpos[1] = 1-(newY / float(SoQtMax((int)(glsize[1]-1), 1)));

		//qDebug() << "Muovo da: " << lastpos[0] << "," << lastpos[1] << " a: " << newpos[0] << newpos[1];

		SbVec3f startpoint = renderarea->projector->project(lastpos);
		SbRotation r;
		SbVec3f endpoint = renderarea->projector->projectAndGetRotation(newpos, r);
		r.invert();
		
		//qDebug() << "Start Point: " << startpoint[0] << "," << startpoint[1] << "," << startpoint[2];
		//qDebug() << "End   Point: " << endpoint[0] << "," << endpoint[1] << "," << endpoint[2];
	
		applyRotationToCamera(renderarea->camera, r);
	}
	renderarea->hasPreviousPoint=true;
	renderarea->lastX = newX;
	renderarea->lastY = newY;
	return;
}

void zoomCamera(SoCamera *camera, int delta)
{
	// All to be fixed
	//qDebug() << "Zoom: " << delta;
	SbVec3f direction;
    camera->orientation.getValue().multVec(SbVec3f(0, 0, -1), direction);
	
	SbVec3f oldpos = camera->position.getValue();
    SbVec3f newpos = oldpos + (delta) * -direction;
	
	camera->position = newpos;
	camera->focalDistance.setValue(camera->focalDistance.getValue()+delta);
	camera->farDistance.setValue(camera->farDistance.getValue()+delta);

	return;
}

void CubeSelectCB(void *data, SoPath *path)
{
	qDebug() << "Cube Select CallBack....";
	MyRenderArea *renderarea = (MyRenderArea *) data;
	//qDebug() << "Selezionato" << path->getTail()->getTypeId().getName().getString();
	//call implicit redraw()
	path->getHead()->touch();
	SoSeparator *cubeInstance = (SoSeparator *) path->getNode(2);
	renderarea->selectedCube = cubeInstance;
	
	SoTranslation *cubeTranslation = (SoTranslation *) cubeInstance->getChild(0);
	float x, y, z;
	x = cubeTranslation->translation.getValue()[0];
	y = cubeTranslation->translation.getValue()[1];
	z = cubeTranslation->translation.getValue()[2];

	qDebug() << "Selezionato cubo in posizione: " << x/3 << "," << y/3 << "," << z/3;
}

void CubeDeselectCB(void *data, SoPath *path)
{
	MyRenderArea *renderarea = (MyRenderArea *) data;
	renderarea->selectedCube = NULL;
	path->getHead()->touch();
	//qDebug() << "Deselezionato";
}

void explodeCube(void *data, SoSensor *)
{
	MyRenderArea *renderarea = (MyRenderArea *)data;
	if(renderarea->selectedCube) {
	
		//Remove the mover from child for each previous exploded cube.		
		SbTime before, after;
		before.setToTimeOfDay();
		SoChildList *list = renderarea->movedGraph->getChildren();	
		for(int i=1; i<list->getLength(); i++) {
			//qDebug() << (renderarea->explodedCubes)[i];
			SoSeparator *cubeInstance = (SoSeparator *) list->get(i);
			SoMaterial *cubeMaterial = (SoMaterial *) cubeInstance->getChild(1);
			cubeMaterial->set("diffuseColor 0.6 0.6 0.6");
			
			
			renderarea->graph->addChild(cubeInstance);
		}
		renderarea->movedGraph->removeAllChildren();
		renderarea->movedGraph->addChild(renderarea->mover);

		delete[] renderarea->explodedCubes;
		renderarea->numExplodedCubes = 0;
		
		after.setToTimeOfDay();	

		qDebug() << "Cleaned out in: " << (after-before).getValue() << " seconds.";
		
		renderarea->isExploded = true;
		SoTranslation *cubeTranslation = (SoTranslation *) renderarea->selectedCube->getChild(0);
		float x, y, z;
		x = cubeTranslation->translation.getValue()[0];
		y = cubeTranslation->translation.getValue()[1];
		z = cubeTranslation->translation.getValue()[2];
		qDebug() << "Esplodo, cursore in posizione: " << x/3 << "," << y/3 << "," << z/3;
		
		SbVec3f direction;
		renderarea->camera->orientation.getValue().multVec(SbVec3f(0, 0, -1), direction);
		//qDebug() << "Current Camera direction: " << direction[0] << "," << direction[1] << "," << direction[2];
		
		int max = 0;
		for(int i=0; i<3; i++) {
			if(SoQtAbs(direction[i]) > SoQtAbs(direction[max])) {
				max = i;
			}
		}
		switch (max) {
			case 0:
				if(direction[0] < 0) {
					selectCubes(renderarea, true,false,false,NUM_CUBES,x/3+1,NUM_CUBES);
				} else {
					selectCubes(renderarea, true,false,false,NUM_CUBES,0,x/3);
				}
				break;
			case 1:
				if(direction[1] < 0) {
					selectCubes(renderarea, false,true,false,NUM_CUBES,y/3+1,NUM_CUBES);
				} else {
					selectCubes(renderarea, false,true,false,NUM_CUBES,0,y/3);
				}
				break;
			case 2:
				if(direction[2] < 0) {
					selectCubes(renderarea, false,false,true,NUM_CUBES,z/3+1,NUM_CUBES);
				} else {
					selectCubes(renderarea, false,false,true,NUM_CUBES,0,z/3);
				}
				break;
		}
	} 
}

void implodeCube(MyRenderArea *renderarea) {
	renderarea->isExploded = false;
	renderarea->interpolator->input0 = renderarea->interpolator->input1;
	renderarea->interpolator->input1 = SbVec3f(0,0,0);	
	renderarea->timer->trigger.touch();
}

void selectCubes(MyRenderArea* renderarea, SbBool constrainX, SbBool constrainY, SbBool constrainZ, int maxCubes, int constraintMin, int constraintMax) {
	//qDebug() << "min: " << constraintMin << " max: " << constraintMax;
	renderarea->numExplodedCubes = NUM_CUBES*NUM_CUBES*(constraintMax-constraintMin);
	renderarea->explodedCubes = new SoSeparator*[renderarea->numExplodedCubes];
	qDebug() << "selected " << renderarea->numExplodedCubes << "cubes for removing.";
	renderarea->interpolator->input0 = SbVec3f(0, 0, 0);
	int counter = 0;
	SbTime before, after;
	before.setToTimeOfDay();
	
	for(int i=constraintMin; i<constraintMax; i++) {
		for(int j=0; j<maxCubes; j++) {
			for(int k=0; k<maxCubes; k++) {
				if(constrainX) {
					//qDebug() << "mat[" << i <<"][" << j << "][" << k << "]";
					SoSeparator *cubeInstance = (renderarea->graphMatrix)[i][j][k];
					SoMaterial *cubeMaterial = (SoMaterial *) cubeInstance->getChild(1);
					cubeMaterial->set("diffuseColor 0.6 0.2 0.9");
					(renderarea->explodedCubes)[counter] = cubeInstance;
					counter++;
					
	 				//cubeInstance->replaceChild(4,renderarea->mover);
					//cubeInstance->addChild(renderarea->cubeShape);
					
					renderarea->movedGraph->addChild(cubeInstance);
					renderarea->graph->removeChild(cubeInstance);
					
					renderarea->interpolator->input1 = SbVec3f(0,0,3*NUM_CUBES);

					
				} else if (constrainY) {
					//qDebug() << "mat[" << j <<"][" << i << "][" << k << "]";
					SoSeparator *cubeInstance = (renderarea->graphMatrix)[j][i][k];
					SoMaterial *cubeMaterial = (SoMaterial *) cubeInstance->getChild(1);
					cubeMaterial->set("diffuseColor 0.6 0.2 0.9");
					
					(renderarea->explodedCubes)[counter] = cubeInstance;
					counter++;
					
					//cubeInstance->replaceChild(4,renderarea->mover);
					//cubeInstance->addChild(renderarea->cubeShape);
					
					renderarea->movedGraph->addChild(cubeInstance);
					renderarea->graph->removeChild(cubeInstance);
					
					renderarea->interpolator->input1 = SbVec3f(3*NUM_CUBES,0,0);


				} else if (constrainZ) {
					//qDebug() << "mat[" << j <<"][" << k << "][" << i << "]";
					SoSeparator *cubeInstance = (renderarea->graphMatrix)[j][k][i];
					SoMaterial *cubeMaterial = (SoMaterial *) cubeInstance->getChild(1);
					cubeMaterial->set("diffuseColor 0.6 0.2 0.9");
					
					(renderarea->explodedCubes)[counter] = cubeInstance;
					counter++;
					
					//cubeInstance->replaceChild(4,renderarea->mover);
					//cubeInstance->addChild(renderarea->cubeShape);
					
					renderarea->movedGraph->addChild(cubeInstance);
					renderarea->graph->removeChild(cubeInstance);
					
					renderarea->interpolator->input1 = SbVec3f(3*NUM_CUBES,0,0);
					

				}
			}
		}
	}
	
	after.setToTimeOfDay();
	qDebug() << "Time: " << (after-before).getValue();
	renderarea->interpolator->alpha.disconnect(&renderarea->timer->ramp);

	renderarea->timer->unref();
	renderarea->timer = new SoOneShot();
	renderarea->timer->ref();
	renderarea->timer->duration.setValue(1);
	renderarea->timer->flags = SoOneShot::HOLD_FINAL;
		
	renderarea->interpolator->alpha.connectFrom(&renderarea->timer->ramp);
	
	renderarea->render();
	
	qDebug() << "Start animation...";
	renderarea->timer->trigger.touch();
}

SoGroup *getGraph(MyRenderArea *renderarea) {
	SoGroup *graph = renderarea->graph;	
	
	SoCube *cubeShape = renderarea->cubeShape;
	
	SoTexture2 *cubeTexture = new SoTexture2();
	cubeTexture->model = SoTexture2::MODULATE;
	cubeTexture->filename = "/Users/cerberos/Documents/swe/test/prova/texture.tga";
	
	SoShapeHints *renderHints = new SoShapeHints();
	renderHints->vertexOrdering = SoShapeHints::COUNTERCLOCKWISE;
	renderHints->shapeType = SoShapeHints::SOLID;
	
	float xPosition, yPosition, zPosition;
	
	for (int x = 0; x < NUM_CUBES; x++) {
		for (int y = 0; y < NUM_CUBES; y++) {
			for (int z = 0; z < NUM_CUBES; z++) {
				SoSeparator *cubeInstance = new SoSeparator();
				
				SoTranslation *translation = new SoTranslation();
				
				xPosition = x *3.0;
				yPosition = y *3.0;
				zPosition = z *3.0;
				
				translation->translation = SbVec3f(xPosition, yPosition, zPosition);
				SoMaterial *material = new SoMaterial();
				material->set("diffuseColor 0.6 0.6 0.6");
				material->set("shininess 0.2");
				material->set("transparency 0.6");
						
				cubeInstance->addChild(translation);

				cubeInstance->addChild(material);
				
				cubeInstance->addChild(cubeTexture);
				
				cubeInstance->addChild(renderHints);
								
				cubeInstance->addChild(cubeShape);
				
				graph->addChild(cubeInstance);
				(renderarea->graphMatrix)[x][y][z] = cubeInstance;
			}
		}
	}
	
	return graph;
}

class Container : public QWidget {
public:
	Container(QWidget *parent) : QWidget(parent) {}
};

void EnterChat::handleReturn() {
	if (!text().isEmpty())
		emit newText(text());
	clear();
}

void TimerWidget::updateTimer() {
	display(--secs);
	if (secs <= 0)
		secs = 61;
}

int main(int argc, char **argv)
{
	// Initialize Qt and SoQt.
	QApplication app(argc, argv);
	qApp->setApplicationName("Prototipo v3.2");
	qApp->setOrganizationName("ITWorks");
	
	QMainWindow *mainwin = new QMainWindow();
	mainwin->setFixedSize(1000, 800);
	mainwin->setWindowTitle("Prototipo v3.2");
	SoQt::init(mainwin);

	QWidget *container = new Container(mainwin);
	container->resize(500, 500);
	mainwin->setCentralWidget(container);
	
	// ChatWidget
	QDockWidget *dock1 = new QDockWidget(mainwin);
	dock1->setWindowTitle("Chat");
	dock1->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
	dock1->setAllowedAreas(QFlag(Qt::AllDockWidgetAreas & ~Qt::TopDockWidgetArea));
	QWidget *chatwidget = new QWidget(dock1);
	QBoxLayout *LM = new QVBoxLayout(chatwidget);
	QTextEdit *chattext = new QTextEdit(chatwidget);
	chattext->setReadOnly(true);
	EnterChat *enterchat = new EnterChat(chatwidget);
	QObject::connect(enterchat, SIGNAL(returnPressed()), enterchat, SLOT(handleReturn()));
	QObject::connect(enterchat, SIGNAL(newText(const QString &)), chattext, SLOT(append(const QString &)));
	LM->addWidget(chattext);
	LM->addWidget(enterchat);
	chatwidget->setLayout(LM);
	dock1->setWidget(chatwidget);
	
	// TimerWidget
	QDockWidget *dock3 = new QDockWidget(mainwin);
	dock3->setWindowTitle("Timer");
	dock3->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
	dock3->setAllowedAreas(QFlag(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea));
	TimerWidget *timer = new TimerWidget(dock3);
	timer->display(60);
	QTimer *clock = new QTimer(dock3);
	QObject::connect(clock, SIGNAL(timeout()), timer, SLOT(updateTimer()));
	clock->start(1000);
	dock3->setWidget(timer);
	
	// GameInfo/History
	QDockWidget *dock2 = new QDockWidget(mainwin);
	dock2->setWindowTitle("Game Info");
	dock2->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
	dock2->setAllowedAreas(QFlag(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea));
	dock2->setWidget(new QTableView(dock2));
	
	mainwin->addDockWidget(Qt::RightDockWidgetArea, dock1);
	mainwin->addDockWidget(Qt::LeftDockWidgetArea, dock3);
	mainwin->addDockWidget(Qt::LeftDockWidgetArea, dock2);
	
	// Construct a (not-so-)simple scenegraph.
	SoSeparator *root = new SoSeparator();
	root->ref();
	
	SoDirectionalLight *light = new SoDirectionalLight();
	root->addChild(light);

	// Add the RenderArea.
	MyRenderArea *renderarea = new MyRenderArea(container);
	container->setFocusProxy(renderarea->getWidget());
	renderarea->setEventCallback(renderAreaCB, renderarea);
	renderarea->setBackgroundColor(SbColor(0.0, 0.1, 0.2));
	SoBoxHighlightRenderAction * highlater = new SoBoxHighlightRenderAction();
	highlater->setColor(SbColor(0.8, 0.8, 0.0));
	renderarea->setGLRenderAction(highlater);
	renderarea->setTransparencyType(SoGLRenderAction::BLEND);
	//renderarea->setAccumulationBuffer(true);
	renderarea->setAntialiasing(true, 1);
	
	root->addChild(renderarea->camera);
	
	SoSelection *selection = new SoSelection;
    selection->policy = SoSelection::SINGLE;
	
	selection->renderCaching = SoSeparator::ON;
	selection->boundingBoxCaching = SoSeparator::ON;
	selection->renderCulling = SoSeparator::ON;
	selection->pickCulling = SoSeparator::ON;

    selection->addChild(getGraph(renderarea));
	selection->addChild(renderarea->movedGraph);
	selection->addSelectionCallback(CubeSelectCB, renderarea);
	selection->addDeselectionCallback(CubeDeselectCB, renderarea);
	
	root->addChild(selection);
	renderarea->setSceneGraph(root);
	
	renderarea->camera->viewAll(root, renderarea->getViewportRegion());
	renderarea->camera->nearDistance.setValue(0.01f);
	mainwin->show();
	
	// Start event loop.
	int ret = app.exec();

	delete mainwin;
	root->unref();
	return ret;
}
